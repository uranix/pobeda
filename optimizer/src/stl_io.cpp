#include "stl_io.h"
#include "auxillary.h"

typedef std::vector<double> dvec;
typedef std::vector<float> fvec;
typedef std::vector<int> ivec;

fvec fnorm1(fvec & v1, fvec & v2, fvec & v3)
{
    float a1, a2, a3, b1, b2, b3;
    fvec nor(3);

    a1 = v2[0] - v1[0];
    a2 = v2[1] - v1[1];
    a3 = v2[2] - v1[2];

    b1 = v3[0] - v1[0];
    b2 = v3[1] - v1[1];
    b3 = v3[2] - v1[2];

    nor[0] =  a2*b3-a3*b2;
    nor[1] =  a3*b1-a1*b3;
    nor[2] =  a1*b2-a2*b1;

    double len = std::sqrt( nor[0]*nor[0] + nor[1]*nor[1] + nor[2]*nor[2] );

    nor[0] = nor[0] / len;
    nor[1] = nor[1] / len;
    nor[2] = nor[2] / len;

    return nor;
}


void write_stl1(std::string filename, dvec vert_1, dvec vert_2, dvec vert_3){

    //binary file
    std::string header_info = filename;
    char head[80];
    std::strncpy(head,header_info.c_str(),sizeof(head)-1);
    char attribute[2] = "0";
    unsigned long nTriLong = vert_1.size()/3 ;
    std::ofstream binfile;

    binfile.open((filename + ".stl").c_str(),  std::ios::out | std::ios::binary);

    binfile.write(head,sizeof(head));
    binfile.write((char*)&nTriLong,4);
    std::cout<<"saving configuration"<<std::endl;
        //write down every triangle
    for (int i = 0; i<vert_1.size()/3; i++){

        // compute normal
        fvec v1(3), v2(3), v3(3), n(3);
        for (int k = 0; k<3; k++)
        {
            v1[k] = (float) vert_1[i*3+k];
            v2[k] = (float) vert_2[i*3+k];
            v3[k] = (float) vert_3[i*3+k];
        }

        n = fnorm1(v1, v2, v3);

        //normal vector coordinates

        binfile.write((char*)&n[0], 4);
        binfile.write((char*)&n[1], 4);
        binfile.write((char*)&n[2], 4);

        //p1 coordinates
        binfile.write((char*)&v1[0], 4);
        binfile.write((char*)&v1[1], 4);
        binfile.write((char*)&v1[2], 4);

        //p2 coordinates
        binfile.write((char*)&v2[0], 4);
        binfile.write((char*)&v2[1], 4);
        binfile.write((char*)&v2[2], 4);

        //p3 coordinates
        binfile.write((char*)&v3[0], 4);
        binfile.write((char*)&v3[1], 4);
        binfile.write((char*)&v3[2], 4);

        binfile.write(attribute,2);

    }

    binfile.close();
}



void write_td2(std::string filename, dvec topo_derivs){

    //text file
    std::ofstream txtfile;
    txtfile.open((filename + ".txt").c_str());

    std::cout<<"saving topological derivatives"<<std::endl;
    for (int i = 0; i<topo_derivs.size(); i++){
        txtfile<<i<<" "<<topo_derivs[i]<<std::endl;
    }

    txtfile.close();
}



void write_td_slice(std::string filename, dvec topo_derivs, int M, int N, int K, int mode, int depth){

    // This procedure saves the flat slice of
    // the field of topological derivatives

    // mode = 1 - XY (mn)slice
    // mode = 2 - YZ (nk) slice
    // mode = 3 - XZ (mk) slice

    //binary file
    std::ofstream binfile;
    binfile.open((filename + ".txt").c_str());

    dvec td_slice(0);

    if (mode == 1)
    {
        if (depth > K-1) std::cout<<"TD slice is out of range!"<<std::endl;
        td_slice.resize(M*N);
        for (int n = 0; n<N; n++)
        {
            for (int m = 0; m<M; m++)
            {
                td_slice[m + M*n] = topo_derivs[iii(m,n,depth,M,N,K)];
            }
        }
    }

    if (mode == 2)
    {
        if (depth > M-1) std::cout<<"TD slice is out of range!"<<std::endl;

        td_slice.resize(N*K);
        for (int k = 0; k<K; k++)
        {
            for (int n = 0; n<N; n++)
            {
                td_slice[n + N*k] = topo_derivs[iii(depth,n,k,M,N,K)];
            }
        }
    }

    if (mode == 3)
    {
        if (depth > N-1) std::cout<<"TD slice is out of range!"<<std::endl;

        td_slice.resize(M*K);
        for (int k = 0; k<K; k++)
        {
            for (int m = 0; m<M; m++)
            {
                td_slice[m + M*k] = topo_derivs[iii(m,depth,k,M,N,K)];
            }
        }
    }




    std::cout<<"saving slice of topological derivatives"<<std::endl;
    for (int i = 0; i<td_slice.size(); i++){
        binfile<<i<<" "<<td_slice[i]<<std::endl;
    }

    binfile.close();
}
