#include "fmm_summation.h"
#include "elastic_kernels.h"

# define SRC_PER_TRG 64
# define DIM 3
# define SURF_DIM 6

extern double YOUNG;
extern double POISS;
extern double SHEAR;


//--------Global FMM stuff------------------


// pvfmm memory-manager
pvfmm::mem::MemoryManager matvec_mem_mgr(1000000);

// Matrices
pvfmm::PtFMM matvec_matrices(&matvec_mem_mgr);

// tree
pvfmm::PtFMM_Tree* matvec_tree;

//==========================================


// Naive black-box fmm
void calculate_displacements(
        vec & src_coord,
        vec & src_value,
        vec & surf_coord,
        vec & surf_value,
        vec & trg_coord,
        vec & trg_value,
        int mult_order,
        MPI_Comm comm){

  // Set kernel.
  const pvfmm::Kernel<double>& kernel_fn=ElasticKernel<double>::Disp();


  // Create memory-manager (optional)
  pvfmm::mem::MemoryManager mem_mgr(10000000);

  // Construct tree.
  int max_pts = 100;
  pvfmm::PtFMM_Tree* tree=PtFMM_CreateTree(src_coord, src_value, surf_coord, surf_value,
                                           trg_coord, comm, max_pts, pvfmm::FreeSpace);

  // Load matrices.
  pvfmm::PtFMM matrices(&mem_mgr);
  matrices.Initialize(mult_order, comm, &kernel_fn);

  // FMM Setup
  tree->SetupFMM(&matrices);

  // Run FMM
  size_t n_trg = trg_coord.size()/3;
  PtFMM_Evaluate(tree, trg_value, n_trg);

  // Free memory
  delete tree;
  return;
}



// Initial pass that creates fmm
// context for fast re-calculation

void init_fmm_pass(vec & src_coord,
        vec & src_value,
        vec & surf_coord,
        vec & surf_value,
        vec & trg_coord,
        vec & trg_value,
        int mult_order,
        MPI_Comm comm){

  std::cout<<"INIT FMM PASS START"<<std::endl;
  // Set kernel.
  const pvfmm::Kernel<double>& kernel_fn=ElasticKernel<double>::Disp();

  // Construct tree.
  int max_pts = 100;
  matvec_tree=PtFMM_CreateTree(src_coord, src_value, surf_coord, surf_value,
                                           trg_coord, comm, max_pts, pvfmm::FreeSpace);
  // Load matrices.
  //pvfmm::PtFMM matrices(&mem_mgr);
  matvec_matrices.Initialize(mult_order, comm, &kernel_fn);

  // FMM Setup
  matvec_tree->SetupFMM(&matvec_matrices);

  // Run FMM
  size_t n_trg = trg_coord.size()/3;
  pvfmm::PtFMM_Evaluate(matvec_tree, trg_value, n_trg);

  return;
}




void re_calculate_displacements(
        vec & src_value,
        vec & surf_value,
        vec & trg_value){



  // Clean FMM sources and targets
  matvec_tree->ClearFMMData();


  // Run FMM
  int n_trg = ( src_value.size() / DIM + surf_value.size() / SURF_DIM ) / SRC_PER_TRG;


  PtFMM_Evaluate(matvec_tree, trg_value, n_trg, &src_value, &surf_value);

  //matvec_tree->ClearFMMData();

  //std::cout<<"RE-CALC END"<<std::endl;

  return;
}



void direct_summation_displacement(vec & src_coord,
                                   vec & src_value,
                                   vec & surf_coord,
                                   vec & surf_value,
                                   vec & trg_coord,
                                   vec & trg_value)
{
    for (int i=0; i<trg_value.size();i++) {trg_value[i] = 0.0;}
    vec x(3), d(3), n(3), csi(3);
    for (int t = 0; t<trg_coord.size()/3;t++)
    {
        for (int i=0; i<3;i++) csi[i] = trg_coord[3*t+i];
        // SURF
        for (int s=0; s<surf_coord.size()/3;s++)
        {
            for (int i=0; i<3;i++){
                x[i] = surf_coord[s*3+i];
                d[i] = surf_value[s*6+i];
                n[i] = surf_value[s*6+3+i]; }

//        std::cout<<"x = "<<x[0]<<" "<<x[1]<<" "<<x[2]<<std::endl;
//        std::cout<<"d = "<<d[0]<<" "<<d[1]<<" "<<d[2]<<std::endl;
//        std::cout<<"n = "<<n[0]<<" "<<n[1]<<" "<<n[2]<<std::endl;


            for (int m = 0; m<3; m++){
            for (int k = 0; k<3; k++){

                trg_value[3*t+m] += P_ij( m+1, k+1, csi, x, n, POISS ) * d[k];
            }}
        }
        for (int s=0; s<src_coord.size()/3;s++)
        {
            for (int i=0; i<3;i++){
                x[i] = src_coord[s*3+i];
                d[i] = src_value[s*3+i];}
            for (int m = 0; m<3; m++){
            for (int k = 0; k<3; k++){

                trg_value[3*t+m] += U_ij( m+1, k+1, csi, x, SHEAR, POISS ) * d[k];
            }}
        }
    }
    return;
}

void calculate_stresses(
        vec & src_coord,
        vec & src_value,
        vec & surf_coord,
        vec & surf_value,
        vec & trg_coord,
        vec & trg_value,
        int mult_order,
        MPI_Comm comm){

    int n_trg = trg_coord.size()/3;

    const pvfmm::Kernel<double>& kernel_fn2=ElasticKernel<double>::Stress();

    // Create memory-manager (optional)
    pvfmm::mem::MemoryManager vol_mem_mgr(10000000);

    // Construct tree.
    size_t max_pts=600;
    pvfmm::PtFMM_Tree* vol_tree=PtFMM_CreateTree(src_coord, src_value, surf_coord, surf_value, trg_coord, comm, max_pts, pvfmm::FreeSpace);

    // Load matrices.
    pvfmm::PtFMM vol_matrices(&vol_mem_mgr);
    vol_matrices.Initialize(mult_order, comm, &kernel_fn2);

    // FMM Setup
    vol_tree->SetupFMM(&vol_matrices);

    // Run FMM
    PtFMM_Evaluate(vol_tree, trg_value, n_trg);

    delete vol_tree;
}



void direct_summation_stress(      vec & src_coord,
                                   vec & src_value,
                                   vec & surf_coord,
                                   vec & surf_value,
                                   vec & trg_coord,
                                   vec & trg_value)
{
    for (int i=0; i<trg_value.size();i++) {trg_value[i] = 0.0;}
    vec x(3), d(3), n(3), csi(3);
    for (int t = 0; t<trg_coord.size()/3;t++)
    {
        for (int i=0; i<3;i++) csi[i] = trg_coord[3*t+i];
        // SURF
        for (int s=0; s<surf_coord.size()/3;s++)
        {
            for (int i=0; i<3;i++){
                x[i] = surf_coord[s*3+i];
                d[i] = surf_value[s*6+i];
                n[i] = surf_value[s*6+3+i];
            }

            for (int k = 0; k<3; k++){

                trg_value[9*t+0] += P_ijk( 1, 1, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+1] += P_ijk( 1, 2, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+2] += P_ijk( 1, 3, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+3] += P_ijk( 2, 1, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+4] += P_ijk( 2, 2, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+5] += P_ijk( 2, 3, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+6] += P_ijk( 3, 1, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+7] += P_ijk( 3, 2, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+8] += P_ijk( 3, 3, k+1, csi, x, n, SHEAR, POISS ) * d[k];
            }
        }
        //SRC
        for (int s=0; s<src_coord.size()/3;s++)
        {
            for (int i=0; i<3;i++){
                x[i] = src_coord[s*3+i];
                d[i] = src_value[s*3+i];}

            for (int k = 0; k<3; k++){
                trg_value[9*t+0] += U_ijk( 1, 1, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+1] += U_ijk( 1, 2, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+2] += U_ijk( 1, 3, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+3] += U_ijk( 2, 1, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+4] += U_ijk( 2, 2, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+5] += U_ijk( 2, 3, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+6] += U_ijk( 3, 1, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+7] += U_ijk( 3, 2, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+8] += U_ijk( 3, 3, k+1, csi, x, POISS ) * d[k];
            }
        }
    }
    return;
}
