#ifndef MATVEC_H
#define MATVEC_H

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>
#include <omp.h>

typedef std::vector<double> vec;

// These functions should be mounted into Misha's class
void test_integral();
void test_integral2();


// Debug purposes only
void test_matvec(
               int N,
             vec & v,   // Input
             vec & Av,  // Output

              // Additional parameters

             // Triangles
             vec & vert_1,
             vec & vert_2,
             vec & vert_3,

             // Boundary conditions
             vec & bc_t,

            // FMM args
            int mult_order,
            MPI_Comm comm
        );



void matvec(
             vec & v,   // Input
             vec & Av,  // Output

              // Additional parameters

             // Triangles
             vec & vert_1,
             vec & vert_2,
             vec & vert_3,

             // Boundary conditions
             vec & bc_t,

            // FMM args
            int mult_order,
            MPI_Comm comm
        );

void rhs(
             vec & v,   // Input
             vec & Av,  // Output

             // Additional parameters

             // Triangles
             vec & vert_1,
             vec & vert_2,
             vec & vert_3,

             // Boundary conditions
             vec & bc_t,

             // FMM args
             int mult_order,
             MPI_Comm comm
        );

// This function subtracts inaccurate contributions
// from a current triangle, and then replaces them with
// exact analytical singular integral

void local_pass_u(

        vec & subtr,
        vec & add,

        vec & v_1,
        vec & v_2,
        vec & v_3,

        vec & val);



void local_pass(

        vec & subtr,
        vec & add,

        vec & v_1,
        vec & v_2,
        vec & v_3,

        int bc,
        vec & val);

// This function generates the set of sources on the triangle
void triangle2src(
        int mode, // 1 - generate src, 2 - generate surf
        vec & v_1,
        vec & v_2,
        vec & v_3,


        vec & src_coord,
        vec & src_value,
        vec & val
        );

#endif // MATVEC_H
