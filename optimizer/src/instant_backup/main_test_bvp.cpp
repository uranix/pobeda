// MAIN PROJECT FILE

//----------------------------
#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <ctime>
#include <pvfmm.hpp>
#include <utils.hpp>
#include "bvp.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"
#include "fmm_summation.h"
#include "matvec.h"

typedef std::vector<double> vec;

//-----------------------------



typedef std::vector<double> vec;


int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  MPI_Comm comm=MPI_COMM_WORLD;

  //--------Read command line options-----------------------------------------------------------------------------------------------------------------------
  commandline_option_start(argc, argv, "\
  This example demonstrates solving a particle N-body problem,\n\
  with Laplace Gradient kernel, using the PvFMM library.\n");
  commandline_option_start(argc, argv);
  omp_set_num_threads( atoi(commandline_option(argc, argv,  "-omp",     "1", false, "-omp  <int> = (1)    : Number of OpenMP threads."          )));
  size_t   N=(size_t)strtod(commandline_option(argc, argv,    "-N",     "1",  true, "-N    <int>          : Number of source and target points."),NULL);
  int      m=       strtoul(commandline_option(argc, argv,    "-m",    "10", false, "-m    <int> = (10)   : Multipole order (+ve even integer)."),NULL,10);
  commandline_option_end(argc, argv);
  pvfmm::Profile::Enable(true);
  //========================================================================================================================================================


  //-------Allocate memory for cube surface problem----------

  vec vert_1( N * N * 24 * 3);
  vec vert_2( N * N * 24 * 3);
  vec vert_3( N * N * 24 * 3);
  vec bc_type( N * N * 24 );
  vec tractions(N * N * 20 * 3);
  vec displacements(N * N * 4 * 3);
  vec tr_di(N * N * 24 * 3);

  //--------------------------------------------------------



//  vec csi(3); csi = {0.0, 0.0, 1.0};
//  vec n(3); n = {1.0, 1.0, 1.0};
//  for (int i = 0; i<3; i++){
//  for (int j = 0; j<3; j++){
//  vec x(3); x = {0.1*i, 0.1*j, 10.0};
//  std::cout<<P_ij( i+1, j+1, csi, x, n, 0.3 )<<std::endl;
//  std::cout<<P_ij1( i+1, j+1, csi, x, n, 0.3 )<<std::endl;
//  }}



  //------Set up BVP----------------------------------------
  set_surf_bvp(

          N,

          // General BVP parameters
          vert_1,
          vert_2,
          vert_3,
          bc_type,
          tractions,
          displacements,
          tr_di);


  //--------------------------------------------------------
  std::cout<<"BVP set"<<std::endl;
  //---------Compute matvec---------------------------------
  vec v( N * N * 24 * 3);
  vec Av( N * N * 24 * 3);

  for (int i = 0; i<v.size(); i++)
  {v[i] = 1;} //v[0] = 1.;
  //test_matvec( N, v,Av,vert_1,vert_2,vert_3,bc_type,m,comm);

   rhs( v,Av,vert_1,vert_2,vert_3,bc_type,m,comm);
  for (int i = 0; i<Av.size(); i++)
  {std::cout<<"Av["<<i<<"]="<<Av[i]<<std::endl;}

  //--------------------------------------------------------

//  clock_t begin = std::clock();
//  clock_t end = std::clock();
//  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
//  std::cout<<"Test took "<<elapsed_secs<<" sec"<<std::endl;




  // Shut down MPI
  MPI_Finalize();

  return 0;

}

