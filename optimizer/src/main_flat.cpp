// POBEDA - Parallel Optimization with
// Boundary Elements and Domain Adaptivity

//----------------------------
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <ctime>
#include <pvfmm.hpp>
#include <utils.hpp>
#include "bvp.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"
#include "fmm_summation.h"
#include "matvec.h"
#include "solver_ksp.h"
#include "topo_deriv.h"
#include "mesher.h"
#include "auxillary.h"
#include "stl_io.h"
#include <string>

//============================
typedef std::vector<double> dvec;
typedef std::vector<float> fvec;
typedef std::vector<int> ivec;
//============================

//-----Global variables---------------------------------
int N_iter;         // global number of GMRES iterations
int N_cycle;  // global number of the optimization cycle
int smooth_iters; // global number of geometry correction
                  // iterations


double YOUNG;     // Material's Youngs modulus
double POISS;     // material's Poisson's ratio

double FUNCTIONAL; // cost functional

bool SAVE_CONF; // Save stl with current configuration
bool SAVE_TD;  // Save topological derivatives
bool VOXEL_PP; // Voxel field post-processing (frames and isolted voxels)
bool PVFMM_PROFILING;
//=======================================================


int main(int argc, char **argv){

    //----run configuration--------

    SAVE_CONF = true;
    SAVE_TD = true;
    VOXEL_PP = false;
    PVFMM_PROFILING = true;

    YOUNG = 1.0;
    POISS = 0.3;

    smooth_iters = 20;

    int MAX_CYCLES = 10; // if we did not reach our goal in [MAX_CYCLES] iteratios - we are done
    double ratio = 0.5; // if ratio less than [ratio] - we are done

    // Good for 160x160x160 model
    //double level = 0.0002; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    //double dlevel = 0.0;

    // Good for 16x16x16 model
    double level = 0.03; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    double dlevel = 0.01;


    //=============================


  N_cycle = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm comm=MPI_COMM_WORLD;

  int n_proc; // Number of MPI threads
  int i_proc; // index of this MPI thread
  MPI_Comm_size(comm, &n_proc);
  MPI_Comm_rank(comm, &i_proc);

  //--------Read command line options-----------------------------------------------------------------------------------------------------------------------
  commandline_option_start(argc, argv, "\
    This example demonstrates solving a particle N-body problem,\n\
    with Laplace Gradient kernel, using the PvFMM library.\n");

  commandline_option_start(argc, argv);
  omp_set_num_threads( atoi(commandline_option(argc, argv,  "-omp",     "1", false, "-omp  <int> = (1)    : Number of OpenMP threads."          )));
  size_t   N=(size_t)strtod(commandline_option(argc, argv,    "-N",     "1",  true, "-N    <int>          : Number of source and target points."),NULL);
  int      m =      strtoul(commandline_option(argc, argv,    "-m",    "10", false, "-m    <int> = (10)   : Multipole order (+ve even integer)."),NULL,10);
  commandline_option_end(argc, argv);
  pvfmm::Profile::Enable(PVFMM_PROFILING);
  //========================================================================================================================================================

  int MM = N;
  int NN = 10;
  int KK = N;

  // Start time measurement
  double t1, t2;
  t1 = MPI_Wtime();

  // Surface solution
  // Set up test optimization problem

  //=========
  double c = 1. / ((double) N);
  ivec side_BC(6);
  for (int i = 0; i<6; i++) side_BC[i] = 1;
  side_BC[3] = 2;
  dvec force_on_voxel(MM*NN*KK*6*3);
  for (int i = 0; i < force_on_voxel.size(); i++)
  {force_on_voxel[i] = 0;}
  // apply some force on a single voxel

  for (int n = 0; n<NN; n++)
  force_on_voxel[iiiii(MM-1,n,KK-1,4,2,MM,NN,KK,6,3)] = -1.0;

  dvec vert_1(0);
  dvec vert_2(0);
  dvec vert_3(0);
  ivec bc_type(0);
  dvec tr_di(0);



      // Status - at the beginning all the voxels are present
      ivec status(MM*NN*KK);
      for (int i = 0; i < status.size(); i++)
      {status[i] = 1;}

      //-----BEGINNING OF THE OPTIMIZATION CYCLE
      for ( int opt_cycle = 0; opt_cycle < MAX_CYCLES; opt_cycle++) {
          N_iter = 0; // GMRES iteration counter
          if (!i_proc) std::cout<<std::endl;
          if (!i_proc) std::cout<<std::endl;
          if (!i_proc) std::cout<<"========="<<std::endl;
          if (!i_proc) std::cout<<"========="<<std::endl;
          if (!i_proc) std::cout<<" CYCLE # "<<opt_cycle<<std::endl;
          if (!i_proc) std::cout<<"========="<<std::endl;
          if (!i_proc) std::cout<<"========="<<std::endl;
          if (!i_proc) std::cout<<std::endl;
          if (!i_proc) std::cout<<std::endl;

      // At this point all the arrays should present gathered on all processes
      // Generate the first iteration BVP
      generate_BVP( MM,NN,KK,c,side_BC,force_on_voxel,status,
                    vert_1,vert_2,vert_3,tr_di,bc_type,comm);

      // Write2file
      if ((!i_proc)&&(SAVE_CONF)){
        write_stl1("./CONF/Conf_N_"+std::to_string(N)+"_m_"+std::to_string(m)+"_iter_" + std::to_string(opt_cycle), vert_1, vert_2, vert_3);
      }

      // Manual scatter
      int N_trg = vert_1.size()/3.; // note - N_trg is global for all proc
      int start, end, span, step;
      if (n_proc == 1) step = N_trg;
      if (n_proc > 1) step = (int) std::floor(N_trg / (double) (n_proc));
      start = step * i_proc;
      end   = step *(i_proc+1);
      if (i_proc == n_proc-1) end = N_trg;
      span = end - start;
      for (int i = 0; i<span; i++)
      {
          // single component vectors
          bc_type[i] = bc_type[start+i];
          // tree component vectors
          for (int k=0;k<3;k++)
          {
              vert_1[3*i+k] = vert_1[3*start+3*i+k];
              vert_2[3*i+k] = vert_2[3*start+3*i+k];
              vert_3[3*i+k] = vert_3[3*start+3*i+k];
              tr_di[3*i+k] = tr_di[3*start+3*i+k];
          }
      }
      bc_type.resize(span);
      vert_1.resize(3*span);
      vert_2.resize(3*span);
      vert_3.resize(3*span);
      tr_di.resize(3*span);

      // Cash vector with knowns
      dvec buff(tr_di.size());
      for (int i = 0; i<tr_di.size(); i++)
          buff[i] = tr_di[i];

      //---SOLVE THE SURFACE PROBLEM WITH PETSC GMRES SOLVER------
      SolverFmmGmres * slvr = new SolverFmmGmres();
      slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di);
      slvr->solve(m, comm, 500, 5e-3);
      delete slvr; slvr = nullptr;
      //==========================================================

      // Sort the obtained solution onto tractions and displacements
      dvec tractions(buff.size()), displacements(buff.size());

      for (int i = 0; i<bc_type.size();i++)
      {
          if (bc_type[i] == 1)
          {
              for (int k = 0; k<3; k++)
              {    tractions[3*i+k] =  buff[3*i+k];
               displacements[3*i+k] = tr_di[3*i+k];}
          }
          if (bc_type[i] == 2)
          {
              for (int k = 0; k<3; k++)
              {    tractions[3*i+k] = tr_di[3*i+k];
               displacements[3*i+k] =  buff[3*i+k];}
          }
      }



      // Volume matvec and topological derivatives
      int N_vol = MM*NN*KK;
      dvec coordinates(N_vol*3);

      int d = 0;
      for (int m = 0; m<MM; m++){
          for (int n = 0; n<NN; n++){
              for (int k = 0; k<KK; k++){
                  coordinates[3*d+0] = 1.0/MM*m+1.0/(2.0*MM);
                  coordinates[3*d+1] = 1.0/NN*n+1.0/(2.0*NN);
                  coordinates[3*d+2] = 1.0/KK*k+1.0/(2.0*KK);
                  d++;
              }
          }
      }

      // A) For now all the targets live in main process
      //if (i_proc) coordinates.resize(0);


      // B) Scatter targets over processes
      if (n_proc == 1) step = N_vol;
      if (n_proc > 1) step = (int) std::floor(N_vol / (double) (n_proc));
      start = step * i_proc;
      end   = step *(i_proc+1);
      if (i_proc == n_proc-1) end = N_vol;
      span = end - start;
      for (int i = 0; i<span; i++)
      {
          for (int k=0;k<3;k++)
          {coordinates[3*i+k] = coordinates[3*start+3*i+k];}
      }
      coordinates.resize(3*span);


      //solve for stresses and TDs
      dvec topo_derivs(coordinates.size()/3);
      dvec ener_density(coordinates.size()/3);
      dvec stresses(coordinates.size()*3);

      // Solve for stresses
      volume_matvec(vert_1,vert_2,vert_3,
                         coordinates,tractions,
                         displacements,stresses, m, comm);

      // Solve for TDs
      topo_deriv( stresses, topo_derivs, YOUNG, POISS);
      ener_dens( stresses, ener_density, c);


      // Gather all topological derivatives
      // and energy densities to ALL process
      int sendcount = topo_derivs.size();
      double sendarray[sendcount];
      double sendarray2[sendcount];

      for (int i = 0; i<sendcount; i++)
      {
          sendarray[i] = topo_derivs[i];
          sendarray2[i] = ener_density[i];
      }
      double rbuf[N_vol];
      double rbuf2[N_vol];

      int recvcounts[n_proc];
      int displs[n_proc];
      for (int i = 0; i<n_proc; i++)
      {
          int start, end, span, step;
          if (n_proc==1) step = N_vol;
          if (n_proc>1) step = (int) std::floor(N_vol / (double) (n_proc));
          start = step * i;
          end   = step *(i+1);
          if (i == n_proc-1) end = N_vol;
          recvcounts[i] = end - start;
          if (i == 0) displs[i] = 0;
          if (i > 0) displs[i] = displs[i-1] + recvcounts[i-1];
      }

      // Gather TDs and cost func to ALL the processes
      for (int i = 0; i<n_proc; i++){
      MPI_Gatherv(sendarray, sendcount, MPI_DOUBLE,rbuf, recvcounts, displs,MPI_DOUBLE, i, comm);
      MPI_Gatherv(sendarray2, sendcount, MPI_DOUBLE,rbuf2, recvcounts, displs,MPI_DOUBLE, i, comm);
      }

      topo_derivs.resize(N_vol); ener_density.resize(N_vol);
      for (int i = 0; i<N_vol; i++) {topo_derivs[i] = rbuf[i]; ener_density[i] = rbuf2[i];}


      // Compute functional
      FUNCTIONAL = 0;
      for (int i = 0; i<ener_density.size(); i++)
      FUNCTIONAL +=ener_density[i];

      std::cout<<"COST FUNCTIONAL (STRAIN ENERGY):"<<FUNCTIONAL<<std::endl;

      //Save TDs
      if ((!i_proc)&&(SAVE_TD)){

        // Save all TDs in volume
        //write_td2("td_" + std::to_string(opt_cycle), topo_derivs, N, N, N );

        // Save XY, YZ, XZ slices through the center of the domain
        write_td_slice("./TD/td_slice_xy_N_"+std::to_string(N)+"_m_"+std::to_string(m)+"_iter_" + std::to_string(opt_cycle), topo_derivs, N, N, N, 1, (int) floor(N/2.));
        write_td_slice("./TD/td_slice_yz_N_"+std::to_string(N)+"_m_"+std::to_string(m)+"_iter_" + std::to_string(opt_cycle), topo_derivs, N, N, N, 2, (int) floor(N/2.));
        write_td_slice("./TD/td_slice_xz_N_"+std::to_string(N)+"_m_"+std::to_string(m)+"_iter_" + std::to_string(opt_cycle), topo_derivs, N, N, N, 3, (int) floor(N/2.));
      }

      // Determine span of TDs and absolute cutoff level
      double TD_max = -10e10;
      double TD_min = 10e10;
      for (int i = 0; i<topo_derivs.size(); i++)
      {
          if (topo_derivs[i]>TD_max) TD_max = topo_derivs[i];
          if (topo_derivs[i]<TD_min) TD_min = topo_derivs[i];
      }
      double abs_level = TD_min + (TD_max - TD_min) * level;

      if (!i_proc){
      std::cout<<"TD_min = "<<TD_min<<std::endl;
      std::cout<<"TD_max = "<<TD_max<<std::endl;
      std::cout<<"abs_level = "<<abs_level<<std::endl;
      }

      // Mask previously removed voxels in a simple way
      for (int i = 0; i<topo_derivs.size(); i++)
      { if (status[i] == 0) topo_derivs[i] = 0.0;}

      // Cutting voxels
      for (int i = 0; i<topo_derivs.size(); i++)
      {if (topo_derivs[i]<abs_level) status[i] = 0;}


      //Cutting isolated voxels
      if (VOXEL_PP){
          int removed = 0;
          for (int si = 0; si<smooth_iters; si++)
          {
              for (int i = 0; i<status.size();i++)
              {
                  if (status[i]==1)
                  {
                      bool rem = false;
                      int neighbors = 0;
                      ivec mnks = mnk(i, N,N,N);
                      if ((mnks[0] < N-1)&&(status[iii(mnks[0]+1,mnks[1],mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0)&&(status[iii(mnks[0]-1,mnks[1],mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[1] < N-1)&&(status[iii(mnks[0],mnks[1]+1,mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[1] > 0)&&(status[iii(mnks[0],mnks[1]-1,mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[2] < N-1)&&(status[iii(mnks[0],mnks[1],mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[2] > 0)&&(status[iii(mnks[0],mnks[1],mnks[2]-1,N,N,N)]==1)) neighbors++;
                      if (neighbors<2) { status[i] = 0; removed ++; rem = true;}

                      // Remove "frame" voxels
                      if ((mnks[0] == N-1)&&(status[iii(mnks[0]-1,mnks[1],mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[0] == 0)&&(status[iii(mnks[0]+1,mnks[1],mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[1] == N-1)&&(status[iii(mnks[0],mnks[1]-1,mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[1] == 0)&&(status[iii(mnks[0],mnks[1]+1,mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[2] == N-1)&&(status[iii(mnks[0],mnks[1],mnks[2]-1,N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[2] == 0)&&(status[iii(mnks[0],mnks[1],mnks[2]+1,N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}

                  }

              }
          }
          if ((!i_proc)&&(removed > 0)) std::cout<<"Removed "<<removed<<"invalid voxels";
      }

      //=========================================================


    level +=dlevel;
  } // END OF OPTIMIZATION CYCLE

  t2 = MPI_Wtime();
  printf( "MPI time  %f\n", t2 - t1 );


  // Shut down MPI
  MPI_Finalize();


  return 0;

}
