// POBEDA - Parallel Optimization with
// Boundary Elements and Domain Adaptivity

//----------------------------
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <ctime>
#include <pvfmm.hpp>
#include <utils.hpp>
#include "bvp.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"
#include "fmm_summation.h"
#include "matvec.h"
#include "solver_ksp.h"
#include "topo_deriv.h"
#include "mesher.h"
#include "auxillary.h"
#include "stl_io.h"
#include <string>

//============================
typedef std::vector<double> dvec;
typedef std::vector<float> fvec;
typedef std::vector<int> ivec;
//============================

//-----Global variables---------------------------------
int N_iter;         // global number of GMRES iterations
int N_cycle;  // global number of the optimization cycle
int smooth_iters; // global number of geometry correction
                  // iterations


double YOUNG;      // Material's Youngs modulus
double POISS;      // material's Poisson's ratio
double SHEAR;      // material's shear modulus

double RATIO; // Current fraction to leave
double CRIT_RATIO; // Volume fraction to leave

double FUNCTIONAL; // cost functional

bool SAVE_CONF; // Save stl with current configuration
bool SAVE_TD;  // Save topological derivatives
bool VOXEL_PP; // Voxel field post-processing (frames and isolted voxels)
bool PVFMM_PROFILING;
bool TEST_OUTPUT; // Print out surface solution and topological derivatives
bool DIRECT_SUR;  // Direct summation when compute surface matvecs
bool DIRECT_VOL;  // Direct summation when compute volume matvecs
bool WRITE_ALL_TD; // Save all field of topological derivatives, not just slices.
                   // This option turns off automatically for large models
bool INTERP;       // Use interpolation for near field
//=======================================================


int main(int argc, char **argv){

    // Start time measurement
    double t1, t2;
    t1 = MPI_Wtime();

    //----run configuration--------

    SAVE_CONF = true;
    SAVE_TD = true;
    VOXEL_PP = true;
    PVFMM_PROFILING = false;
    TEST_OUTPUT = false;
    DIRECT_SUR = false;
    DIRECT_VOL = false;
    WRITE_ALL_TD = false;
    INTERP = false;

    RATIO = 1.0;       // Current ratio
    CRIT_RATIO = 0.5;  // Crit ratio

    SHEAR = 1.0;
    POISS = 0.3;

    YOUNG = SHEAR * (2 * (1. + POISS));

    smooth_iters = 100;

    int MAX_CYCLES = 10; // if we did not reach our goal in [MAX_CYCLES] iteratios - we are done
    double ratio = 0.5; // if ratio less than [ratio] - we are done

    // Good for 200x200x200 model
    //double level = 0.00003; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    //double dlevel = 0.000007;

    // Good for 100x100x100 model
    //double level = 0.0002; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    //double dlevel = 0.00005;

    // Good for 64x64x64 model
    //double level = 0.0004; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    //double dlevel = 0.0001;

    // Good for 20x20x20 model
    double level = 0.003; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    double dlevel = 0.0005;

    //double level = 0.0;
    //double dlevel = 0.0;

    //=============================


  N_cycle = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm comm=MPI_COMM_WORLD;

  int n_proc; // Number of MPI threads
  int i_proc; // index of this MPI thread
  MPI_Comm_size(comm, &n_proc);
  MPI_Comm_rank(comm, &i_proc);

  //--------Read command line options-----------------------------------------------------------------------------------------------------------------------
  commandline_option_start(argc, argv, "\
    This example demonstrates solving a particle N-body problem,\n\
    with Laplace Gradient kernel, using the PvFMM library.\n");

  commandline_option_start(argc, argv);
  omp_set_num_threads( atoi(commandline_option(argc, argv,  "-omp",     "1", false, "-omp  <int> = (1)    : Number of OpenMP threads."          )));
  size_t   N=(size_t)strtod(commandline_option(argc, argv,    "-N",     "1",  true, "-N    <int>          : Number of source and target points."),NULL);
  int      m =      strtoul(commandline_option(argc, argv,    "-m",    "10", false, "-m    <int> = (10)   : Multipole order (+ve even integer)."),NULL,10);
  commandline_option_end(argc, argv);
  pvfmm::Profile::Enable(PVFMM_PROFILING);
  //========================================================================================================================================================

  if ((N>20)&&(WRITE_ALL_TD))
  {
      std::cout<<"Too large model, skip writing full field of topological derivatives"<<std::endl;
      WRITE_ALL_TD = false;
  }



  // Surface solution
  // Set up test optimization problem

  //=========
  double c = 1. / ((double) N);
  ivec side_BC(6);
  for (int i = 0; i<6; i++) side_BC[i] = 1;
  side_BC[3] = 2;
  dvec force_on_voxel(N*N*N*6*3);
  for (int i = 0; i < force_on_voxel.size(); i++)
  {force_on_voxel[i] = 0;}
  // apply some force on a single voxel

  // Classic cantilever - line load
//  for (int n = 0; n<N; n++)
//  {
//    force_on_voxel[iiiii(N-1,n,N-1,4,2,N,N,N,6,3)] = -1.0;
//  }

  // "Interesting" cantilever - point load
  //  force_on_voxel[iiiii(N-1,N/2-1,N-1,4,2,N,N,N,6,3)] = -1.0*(N/2);
  //  force_on_voxel[iiiii(N-1,N/2  ,N-1,4,2,N,N,N,6,3)] = -1.0*(N/2);

  // Symmetric cantilever - line load
  for (int n = 0; n<N; n++)
  {
    force_on_voxel[iiiii(N-1,n,N/2-1,1,2,N,N,N,6,3)] = -0.5;
    force_on_voxel[iiiii(N-1,n,N/2  ,1,2,N,N,N,6,3)] = -0.5;
  }


  dvec vert_1(0);
  dvec vert_2(0);
  dvec vert_3(0);
  ivec bc_type(0);
  dvec tr_di(0);



  // Status - at the beginning all the voxels are present
  ivec status(N*N*N);
  ivec is_surf(N*N*N);

  for (int i = 0; i < status.size(); i++)
  {status[i] = 1; is_surf[i] = 0;}


  //----------------------------------------
  //----------------------------------------
  //-----BEGINNING OF THE OPTIMIZATION CYCLE
  //----------------------------------------
  //----------------------------------------

  for ( int opt_cycle = 0; opt_cycle < MAX_CYCLES; opt_cycle++) {


      double t_1, t_2; // In-cycle time measurements

      N_iter = 0; // GMRES iteration counter

      if (!i_proc) std::cout<<std::endl;
      if (!i_proc) std::cout<<std::endl;
      if (!i_proc) std::cout<<"========="<<std::endl;
      if (!i_proc) std::cout<<"========="<<std::endl;
      if (!i_proc) std::cout<<" CYCLE # "<<opt_cycle<<std::endl;
      if (!i_proc) std::cout<<"========="<<std::endl;
      if (!i_proc) std::cout<<"========="<<std::endl;
      if (!i_proc) std::cout<<std::endl;
      if (!i_proc) std::cout<<std::endl;



      ///----GENERATE BVP--------------------------------------------
      t_1 = MPI_Wtime();
      generate_BVP( N,N,N,c,side_BC,force_on_voxel,status,is_surf,
                    vert_1,vert_2,vert_3,tr_di,bc_type,comm);
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO GENERATE BVP: %f\n", t_2 - t_1 );
      ///============================================================





      ///----SAVE CONFIGURATION--------------------------------------
      t_1 = MPI_Wtime();
      if ((!i_proc)&&(SAVE_CONF)){
        write_stl1("./CONF/Conf_N_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), vert_1, vert_2, vert_3);
      }
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO SAVE CONF: %f\n", t_2 - t_1 );
      ///============================================================




      ///----SCATTER SURFACE PROBLEM OVER PROCESSES--------------------------
      t_1 = MPI_Wtime();
      int N_trg = vert_1.size()/3.; // note - N_trg is global for all proc
      int start, end, span, step;
      if (n_proc == 1) step = N_trg;
      if (n_proc > 1) step = (int) std::floor(N_trg / (double) (n_proc));
      start = step * i_proc;
      end   = step *(i_proc+1);
      if (i_proc == n_proc-1) end = N_trg;
      span = end - start;
      for (int i = 0; i<span; i++)
      {
          // single component vectors
          bc_type[i] = bc_type[start+i];
          // tree component vectors
          for (int k=0;k<3;k++)
          {
              vert_1[3*i+k] = vert_1[3*start+3*i+k];
              vert_2[3*i+k] = vert_2[3*start+3*i+k];
              vert_3[3*i+k] = vert_3[3*start+3*i+k];
              tr_di[3*i+k] = tr_di[3*start+3*i+k];
          }
      }
      bc_type.resize(span);
      vert_1.resize(3*span);
      vert_2.resize(3*span);
      vert_3.resize(3*span);
      tr_di.resize(3*span);
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO SCATTER SURF: %f\n", t_2 - t_1 );
      ///============================================================





      ///----SURFACE SOLUTION----------------------------------------
      t_1 = MPI_Wtime();
      // Cash vector with knowns
      dvec buff(tr_di.size());
      for (int i = 0; i<tr_di.size(); i++)
          buff[i] = tr_di[i];
      SolverFmmGmres * slvr = new SolverFmmGmres();
      slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di,m, comm);
      slvr->solve(m, comm, 1000, 1e-4);
      slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di,m, comm);
      delete slvr; slvr = nullptr;
      // Sort the obtained solution onto tractions and displacements
      dvec tractions(buff.size()), displacements(buff.size());
      for (int i = 0; i<bc_type.size();i++)
      {
          if (bc_type[i] == 1)
          {
              for (int k = 0; k<3; k++)
              {    tractions[3*i+k] =  buff[3*i+k];
               displacements[3*i+k] = tr_di[3*i+k];}
          }
          if (bc_type[i] == 2)
          {
              for (int k = 0; k<3; k++)
              {    tractions[3*i+k] = tr_di[3*i+k];
               displacements[3*i+k] =  buff[3*i+k];}
          }
      }
      if (TEST_OUTPUT) {
          for (int i = 0; i<tractions.size()/3; i++)
          {std::cout<<"I_PROC="<<i_proc<<" , tractions ["<<i<<"] = ("<<tractions[3*i+0]<<" , "<<tractions[3*i+1]<<" , "<<tractions[3*i+2]<<")"<<std::endl;}

          for (int i = 0; i<displacements.size()/3; i++)
          {std::cout<<"I_PROC="<<i_proc<<" , displacements ["<<i<<"] = ("<<displacements[3*i+0]<<" , "<<displacements[3*i+1]<<" , "<<displacements[3*i+2]<<")"<<std::endl;}
      }
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO SOLVE SURFACE BVP: %f\n", t_2 - t_1 );
      ///============================================================






      ///----SCATTER VOL TARGETS-----------------------------------------
      t_1 = MPI_Wtime();
      int N_vol = N*N*N;
      dvec coordinates(N_vol*3);
      int d = 0;
      for (int mm = 0; mm<N; mm++){
          for (int nn = 0; nn<N; nn++){
              for (int kk = 0; kk<N; kk++){
                  coordinates[3*d+0] = 1.0/N*mm+1.0/(2.0*N);
                  coordinates[3*d+1] = 1.0/N*nn+1.0/(2.0*N);
                  coordinates[3*d+2] = 1.0/N*kk+1.0/(2.0*N);
                  d++;
              }
          }
      }
      // Scatter targets over processes
      if (n_proc == 1) step = N_vol;
      if (n_proc > 1) step = (int) std::floor(N_vol / (double) (n_proc));
      start = step * i_proc;
      end   = step *(i_proc+1);
      if (i_proc == n_proc-1) end = N_vol;
      span = end - start;
      for (int i = 0; i<span; i++)
      {
          for (int k=0;k<3;k++)
          {coordinates[3*i+k] = coordinates[3*start+3*i+k];}
      }
      coordinates.resize(3*span);
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO SCATTER VOL: %f\n", t_2 - t_1 );
      ///============================================================





      ///-------VOLUME SOLUTION--------------------------------------
      t_1 = MPI_Wtime();
      dvec topo_derivs(coordinates.size()/3);
      dvec ener_density(coordinates.size()/3);
      dvec stresses(coordinates.size()*3);
      // Solve for stresses
      volume_matvec(1, vert_1,vert_2,vert_3,
                         coordinates,tractions,
                         displacements,stresses, m, comm);
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO VOLUME SOLUTION: %f\n", t_2 - t_1 );
      ///============================================================





      ///------COMPUTATION OF ENERGIES AND TOPOLOGICAL DERIVATIVES---
      t_1 = MPI_Wtime();
      topo_deriv( stresses, topo_derivs, SHEAR, POISS);
      ener_dens( stresses, ener_density, c, YOUNG, POISS);

      if (TEST_OUTPUT){
        if (!i_proc) std::cout<<"TOPOLOGICAL DERIVATIVES"<<std::endl;
        for (int i = 0; i<topo_derivs.size(); i++) std::cout<<"At ("<<coordinates[3*i]<<", "<<coordinates[3*i+1]<<", "<<coordinates[3*i+2]<<") - "<<topo_derivs[i]<<std::endl;
      }
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO FUNC AND TDs: %f\n", t_2 - t_1 );
      ///============================================================




      ///------GATHER------------------------------------------------
      t_1 = MPI_Wtime();
      int sendcount = topo_derivs.size();
      double sendarray[sendcount];
      double sendarray2[sendcount];
      for (int i = 0; i<sendcount; i++)
      {
          sendarray[i] = topo_derivs[i];
          sendarray2[i] = ener_density[i];
      }
      double rbuf[N_vol];
      double rbuf2[N_vol];
      int recvcounts[n_proc];
      int displs[n_proc];
      for (int i = 0; i<n_proc; i++)
      {
          int start, end, span, step;
          if (n_proc==1) step = N_vol;
          if (n_proc>1) step = (int) std::floor(N_vol / (double) (n_proc));
          start = step * i;
          end   = step *(i+1);
          if (i == n_proc-1) end = N_vol;
          recvcounts[i] = end - start;
          if (i == 0) displs[i] = 0;
          if (i > 0) displs[i] = displs[i-1] + recvcounts[i-1];
      }
      // Gather TDs and cost func to ALL the processes
      for (int i = 0; i<n_proc; i++){
      MPI_Gatherv(sendarray, sendcount, MPI_DOUBLE,rbuf, recvcounts, displs,MPI_DOUBLE, i, comm);
      MPI_Gatherv(sendarray2, sendcount, MPI_DOUBLE,rbuf2, recvcounts, displs,MPI_DOUBLE, i, comm);
      }
      topo_derivs.resize(N_vol); ener_density.resize(N_vol);
      for (int i = 0; i<N_vol; i++) {topo_derivs[i] = rbuf[i]; ener_density[i] = rbuf2[i];}
      // Compute functional
      FUNCTIONAL = 0;
      for (int i = 0; i<ener_density.size(); i++)
      FUNCTIONAL +=ener_density[i];
      std::cout<<"COST FUNCTIONAL (STRAIN ENERGY):"<<FUNCTIONAL<<std::endl;
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO GATHER: %f\n", t_2 - t_1 );
      ///============================================================

      if (INTERP) {
      ///------POST-PROCESS TDs--------------------------------------
      // Step one - interpolate values of neighbour voxels
      for (int i = 0; i<is_surf.size(); i++)
      {
          if (is_surf[i]==2) // 2nd neighbourhood of the border
          {
              int mm = mnk(i,N,N,N)[0];
              int nn = mnk(i,N,N,N)[1];
              int kk = mnk(i,N,N,N)[2];
              double numerator = 0; double denominator = 0; // corrected td value is n/d
              int N_nei = 0;

              // Look at 6-neighborhood (sqrt(1))
              if ((mm>1) && (status[iii(mm-1,nn,kk,N,N,N)]==1) && (is_surf[iii(mm-1,nn,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn,kk,N,N,N)] * (1.0); denominator +=1.0;}
              if ((mm<N) && (status[iii(mm+1,nn,kk,N,N,N)]==1) && (is_surf[iii(mm+1,nn,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn,kk,N,N,N)] * (1.0); denominator +=1.0;}
              if ((nn>1) && (status[iii(mm,nn-1,kk,N,N,N)]==1) && (is_surf[iii(mm,nn+1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn-1,kk,N,N,N)] * (1.0); denominator +=1.0;}
              if ((nn<N) && (status[iii(mm,nn+1,kk,N,N,N)]==1) && (is_surf[iii(mm,nn+1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn+1,kk,N,N,N)] * (1.0); denominator +=1.0;}
              if ((kk>1) && (status[iii(mm,nn,kk-1,N,N,N)]==1) && (is_surf[iii(mm,nn,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn,kk-1,N,N,N)] * (1.0); denominator +=1.0;}
              if ((kk<N) && (status[iii(mm,nn,kk+1,N,N,N)]==1) && (is_surf[iii(mm,nn,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn,kk+1,N,N,N)] * (1.0); denominator +=1.0;}

              //Look at 12-neighbourhood (sqrt(2))
              if ((mm>1) && (nn>1) && (status[iii(mm-1,nn-1,kk,N,N,N)]==1) && (is_surf[iii(mm-1,nn-1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn-1,kk,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm>1) && (nn<N) && (status[iii(mm-1,nn+1,kk,N,N,N)]==1) && (is_surf[iii(mm-1,nn+1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn+1,kk,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm<N) && (nn>1) && (status[iii(mm+1,nn-1,kk,N,N,N)]==1) && (is_surf[iii(mm+1,nn-1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn-1,kk,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm<N) && (nn<N) && (status[iii(mm+1,nn+1,kk,N,N,N)]==1) && (is_surf[iii(mm+1,nn+1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn+1,kk,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm>1) && (kk>1) && (status[iii(mm-1,nn,kk-1,N,N,N)]==1) && (is_surf[iii(mm-1,nn,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn,kk-1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm>1) && (kk<N) && (status[iii(mm-1,nn,kk+1,N,N,N)]==1) && (is_surf[iii(mm-1,nn,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn,kk+1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm<N) && (kk>1) && (status[iii(mm+1,nn,kk-1,N,N,N)]==1) && (is_surf[iii(mm+1,nn,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn,kk-1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm<N) && (kk<N) && (status[iii(mm+1,nn,kk+1,N,N,N)]==1) && (is_surf[iii(mm+1,nn,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn,kk+1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((nn>1) && (kk>1) && (status[iii(mm,nn-1,kk-1,N,N,N)]==1) && (is_surf[iii(mm,nn-1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn-1,kk-1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((nn>1) && (kk<N) && (status[iii(mm,nn-1,kk+1,N,N,N)]==1) && (is_surf[iii(mm,nn-1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn-1,kk+1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((nn<N) && (kk>1) && (status[iii(mm,nn+1,kk-1,N,N,N)]==1) && (is_surf[iii(mm,nn+1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn+1,kk-1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((nn<N) && (kk<N) && (status[iii(mm,nn+1,kk+1,N,N,N)]==1) && (is_surf[iii(mm,nn+1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn+1,kk+1,N,N,N)] * (0.707); denominator +=0.707;}

              // Look at 8-neighborhood (sqrt(3))
              if ((mm>1) && (nn>1) && (kk>1) && (status[iii(mm-1,nn-1,kk-1,N,N,N)]==1) && (is_surf[iii(mm-1,nn-1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn-1,kk-1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm<N) && (nn>1) && (kk>1) && (status[iii(mm+1,nn-1,kk-1,N,N,N)]==1) && (is_surf[iii(mm+1,nn-1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn-1,kk-1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm>1) && (nn<N) && (kk>1) && (status[iii(mm-1,nn+1,kk-1,N,N,N)]==1) && (is_surf[iii(mm-1,nn+1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn+1,kk-1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm>1) && (nn>1) && (kk<N) && (status[iii(mm-1,nn-1,kk+1,N,N,N)]==1) && (is_surf[iii(mm-1,nn-1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn-1,kk+1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm<N) && (nn<N) && (kk>1) && (status[iii(mm+1,nn+1,kk-1,N,N,N)]==1) && (is_surf[iii(mm+1,nn+1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn+1,kk-1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm<N) && (nn>1) && (kk<N) && (status[iii(mm+1,nn-1,kk+1,N,N,N)]==1) && (is_surf[iii(mm+1,nn-1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn-1,kk+1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm>1) && (nn<N) && (kk<N) && (status[iii(mm-1,nn+1,kk+1,N,N,N)]==1) && (is_surf[iii(mm-1,nn+1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn+1,kk+1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm<N) && (nn<N) && (kk<N) && (status[iii(mm+1,nn+1,kk+1,N,N,N)]==1) && (is_surf[iii(mm+1,nn+1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn+1,kk+1,N,N,N)] * (0.577); denominator +=0.577;}


              if (N_nei>0) // if we managed to fix it
              {
                 topo_derivs[i] = numerator / denominator;
                 is_surf[i] = 0;
              }
              else
              {
                  std::cout<<"Could not fix TD!"<<std::endl;
              }
          }
      }


      // Step two - interpolate values of surface voxels
      for (int i = 0; i<is_surf.size(); i++)
      {
          if (is_surf[i]==1) // 1st neighbourhood of the border
          {
              int mm = mnk(i,N,N,N)[0];
              int nn = mnk(i,N,N,N)[1];
              int kk = mnk(i,N,N,N)[2];
              double numerator = 0; double denominator = 0; // corrected td value is n/d
              int N_nei = 0;

              // Look at 6-neighborhood (sqrt(1))
              if ((mm>1) && (status[iii(mm-1,nn,kk,N,N,N)]==1) && (is_surf[iii(mm-1,nn,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn,kk,N,N,N)] * (1.0); denominator +=1.0;}
              if ((mm<N) && (status[iii(mm+1,nn,kk,N,N,N)]==1) && (is_surf[iii(mm+1,nn,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn,kk,N,N,N)] * (1.0); denominator +=1.0;}
              if ((nn>1) && (status[iii(mm,nn-1,kk,N,N,N)]==1) && (is_surf[iii(mm,nn+1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn-1,kk,N,N,N)] * (1.0); denominator +=1.0;}
              if ((nn<N) && (status[iii(mm,nn+1,kk,N,N,N)]==1) && (is_surf[iii(mm,nn+1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn+1,kk,N,N,N)] * (1.0); denominator +=1.0;}
              if ((kk>1) && (status[iii(mm,nn,kk-1,N,N,N)]==1) && (is_surf[iii(mm,nn,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn,kk-1,N,N,N)] * (1.0); denominator +=1.0;}
              if ((kk<N) && (status[iii(mm,nn,kk+1,N,N,N)]==1) && (is_surf[iii(mm,nn,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn,kk+1,N,N,N)] * (1.0); denominator +=1.0;}

              //Look at 12-neighbourhood (sqrt(2))
              if ((mm>1) && (nn>1) && (status[iii(mm-1,nn-1,kk,N,N,N)]==1) && (is_surf[iii(mm-1,nn-1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn-1,kk,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm>1) && (nn<N) && (status[iii(mm-1,nn+1,kk,N,N,N)]==1) && (is_surf[iii(mm-1,nn+1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn+1,kk,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm<N) && (nn>1) && (status[iii(mm+1,nn-1,kk,N,N,N)]==1) && (is_surf[iii(mm+1,nn-1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn-1,kk,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm<N) && (nn<N) && (status[iii(mm+1,nn+1,kk,N,N,N)]==1) && (is_surf[iii(mm+1,nn+1,kk,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn+1,kk,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm>1) && (kk>1) && (status[iii(mm-1,nn,kk-1,N,N,N)]==1) && (is_surf[iii(mm-1,nn,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn,kk-1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm>1) && (kk<N) && (status[iii(mm-1,nn,kk+1,N,N,N)]==1) && (is_surf[iii(mm-1,nn,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn,kk+1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm<N) && (kk>1) && (status[iii(mm+1,nn,kk-1,N,N,N)]==1) && (is_surf[iii(mm+1,nn,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn,kk-1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((mm<N) && (kk<N) && (status[iii(mm+1,nn,kk+1,N,N,N)]==1) && (is_surf[iii(mm+1,nn,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn,kk+1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((nn>1) && (kk>1) && (status[iii(mm,nn-1,kk-1,N,N,N)]==1) && (is_surf[iii(mm,nn-1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn-1,kk-1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((nn>1) && (kk<N) && (status[iii(mm,nn-1,kk+1,N,N,N)]==1) && (is_surf[iii(mm,nn-1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn-1,kk+1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((nn<N) && (kk>1) && (status[iii(mm,nn+1,kk-1,N,N,N)]==1) && (is_surf[iii(mm,nn+1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn+1,kk-1,N,N,N)] * (0.707); denominator +=0.707;}
              if ((nn<N) && (kk<N) && (status[iii(mm,nn+1,kk+1,N,N,N)]==1) && (is_surf[iii(mm,nn+1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm,nn+1,kk+1,N,N,N)] * (0.707); denominator +=0.707;}

              // Look at 8-neighborhood (sqrt(3))
              if ((mm>1) && (nn>1) && (kk>1) && (status[iii(mm-1,nn-1,kk-1,N,N,N)]==1) && (is_surf[iii(mm-1,nn-1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn-1,kk-1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm<N) && (nn>1) && (kk>1) && (status[iii(mm+1,nn-1,kk-1,N,N,N)]==1) && (is_surf[iii(mm+1,nn-1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn-1,kk-1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm>1) && (nn<N) && (kk>1) && (status[iii(mm-1,nn+1,kk-1,N,N,N)]==1) && (is_surf[iii(mm-1,nn+1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn+1,kk-1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm>1) && (nn>1) && (kk<N) && (status[iii(mm-1,nn-1,kk+1,N,N,N)]==1) && (is_surf[iii(mm-1,nn-1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn-1,kk+1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm<N) && (nn<N) && (kk>1) && (status[iii(mm+1,nn+1,kk-1,N,N,N)]==1) && (is_surf[iii(mm+1,nn+1,kk-1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn+1,kk-1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm<N) && (nn>1) && (kk<N) && (status[iii(mm+1,nn-1,kk+1,N,N,N)]==1) && (is_surf[iii(mm+1,nn-1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn-1,kk+1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm>1) && (nn<N) && (kk<N) && (status[iii(mm-1,nn+1,kk+1,N,N,N)]==1) && (is_surf[iii(mm-1,nn+1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm-1,nn+1,kk+1,N,N,N)] * (0.577); denominator +=0.577;}
              if ((mm<N) && (nn<N) && (kk<N) && (status[iii(mm+1,nn+1,kk+1,N,N,N)]==1) && (is_surf[iii(mm+1,nn+1,kk+1,N,N,N)]==0))
              {N_nei++; numerator += topo_derivs[iii(mm+1,nn+1,kk+1,N,N,N)] * (0.577); denominator +=0.577;}


              if (N_nei>0) // if we managed to fix it
              {
                 topo_derivs[i] = numerator / denominator;
                 is_surf[i] = 0;
              }
              else
              {
                  std::cout<<"Could not fix TD!"<<std::endl;
              }
          }
      }
      }
      ///============================================================


      ///------SAVE TDs------------------------------------------------
      t_1 = MPI_Wtime();
      if ((!i_proc)&&(SAVE_TD)){

        // Save all TDs in volume
        if (WRITE_ALL_TD) write_td2("./TD/td_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), topo_derivs);

        // Save XY, YZ, XZ slices through the center of the domain
        write_td_slice("./TD/td_slice_xy_N_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), topo_derivs, N, N, N, 1, (int) floor(N/2.));
        write_td_slice("./TD/td_slice_yz_N_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), topo_derivs, N, N, N, 2, (int) floor(N/2.));
        write_td_slice("./TD/td_slice_xz_N_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), topo_derivs, N, N, N, 3, (int) floor(N/2.));
      }
      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO SAVE TDs: %f\n", t_2 - t_1 );
      ///============================================================




      ///------POST-PROCESSING----------------------------------------
      t_1 = MPI_Wtime();

      // Determine span of TDs and absolute cutoff level
      double TD_max = -10e10;
      double TD_min = 10e10;
      for (int i = 0; i<topo_derivs.size(); i++)
      {
          if (topo_derivs[i]>TD_max) TD_max = topo_derivs[i];
          if (topo_derivs[i]<TD_min) TD_min = topo_derivs[i];
      }
      double abs_level = TD_min + (TD_max - TD_min) * level;

      if (!i_proc){
      std::cout<<"TD_min = "<<TD_min<<std::endl;
      std::cout<<"TD_max = "<<TD_max<<std::endl;
      std::cout<<"abs_level = "<<abs_level<<std::endl;
      }

      // Mask previously removed voxels in a simple way
      for (int i = 0; i<topo_derivs.size(); i++)
      { if (status[i] == 0) topo_derivs[i] = 0.0;}

      // Cutting voxels
      for (int i = 0; i<topo_derivs.size(); i++)
      {if (topo_derivs[i]<level) status[i] = 0;}


      //Cutting isolated voxels
      if (VOXEL_PP){
          int removed = 0;
          for (int si = 0; si<smooth_iters; si++)
          {
              for (int i = 0; i<status.size();i++)
              {
                  if (status[i]==1)
                  {
                      bool rem = false;
                      int neighbors = 0;
                      ivec mnks = mnk(i, N,N,N);

                      // 6-neighborhood
                      if ((mnks[0] < N-1)&&(status[iii(mnks[0]+1,mnks[1],mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0)&&(status[iii(mnks[0]-1,mnks[1],mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[1] < N-1)&&(status[iii(mnks[0],mnks[1]+1,mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[1] > 0)&&(status[iii(mnks[0],mnks[1]-1,mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[2] < N-1)&&(status[iii(mnks[0],mnks[1],mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[2] > 0)&&(status[iii(mnks[0],mnks[1],mnks[2]-1,N,N,N)]==1)) neighbors++;

                      // 12-neighborhood
                      if ((mnks[0] < N-1)&&(mnks[1] < N-1)&&(status[iii(mnks[0]+1,mnks[1]+1,mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0  )&&(mnks[1] < N-1)&&(status[iii(mnks[0]-1,mnks[1]+1,mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[0] < N-1)&&(mnks[1] > 0  )&&(status[iii(mnks[0]+1,mnks[1]-1,mnks[2],N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0  )&&(mnks[1] > 0  )&&(status[iii(mnks[0]-1,mnks[1]-1,mnks[2],N,N,N)]==1)) neighbors++;

                      if ((mnks[0] < N-1)&&(mnks[2] < N-1)&&(status[iii(mnks[0]+1,mnks[1],mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0  )&&(mnks[2] < N-1)&&(status[iii(mnks[0]-1,mnks[1],mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] < N-1)&&(mnks[2] > 0  )&&(status[iii(mnks[0]+1,mnks[1],mnks[2]-1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0  )&&(mnks[2] > 0  )&&(status[iii(mnks[0]-1,mnks[1],mnks[2]-1,N,N,N)]==1)) neighbors++;

                      if ((mnks[1] < N-1)&&(mnks[2] < N-1)&&(status[iii(mnks[0],mnks[1]+1,mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[1] > 0  )&&(mnks[2] < N-1)&&(status[iii(mnks[0],mnks[1]-1,mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[1] < N-1)&&(mnks[2] > 0  )&&(status[iii(mnks[0],mnks[1]+1,mnks[2]-1,N,N,N)]==1)) neighbors++;
                      if ((mnks[1] > 0  )&&(mnks[2] > 0  )&&(status[iii(mnks[0],mnks[1]-1,mnks[2]-1,N,N,N)]==1)) neighbors++;

                      // 8-neighborhood
                      if ((mnks[0] < N-1 )&&(mnks[1] < N-1)&&(mnks[2] < N-1)&&(status[iii(mnks[0]+1,mnks[1]+1,mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] >  0  )&&(mnks[1] < N-1)&&(mnks[2] < N-1)&&(status[iii(mnks[0]-1,mnks[1]+1,mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] < N-1 )&&(mnks[1] > 0  )&&(mnks[2] < N-1)&&(status[iii(mnks[0]+1,mnks[1]-1,mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] < N-1 )&&(mnks[1] < N-1)&&(mnks[2] > 0  )&&(status[iii(mnks[0]+1,mnks[1]+1,mnks[2]-1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] < N-1 )&&(mnks[1] > 0  )&&(mnks[2] > 0  )&&(status[iii(mnks[0]+1,mnks[1]-1,mnks[2]-1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0   )&&(mnks[1] < N-1)&&(mnks[2] > 0  )&&(status[iii(mnks[0]-1,mnks[1]+1,mnks[2]-1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0   )&&(mnks[1] > 0  )&&(mnks[2] < N-1)&&(status[iii(mnks[0]-1,mnks[1]-1,mnks[2]+1,N,N,N)]==1)) neighbors++;
                      if ((mnks[0] > 0   )&&(mnks[1] > 0  )&&(mnks[2] > 0  )&&(status[iii(mnks[0]-1,mnks[1]-1,mnks[2]-1,N,N,N)]==1)) neighbors++;

                      if (neighbors<6) { status[i] = 0; removed ++; rem = true;}

                      // Remove "frame" voxels
                      if ((mnks[0] == N-1)&&(status[iii(mnks[0]-1,mnks[1],mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[0] == 0)&&(status[iii(mnks[0]+1,mnks[1],mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[1] == N-1)&&(status[iii(mnks[0],mnks[1]-1,mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[1] == 0)&&(status[iii(mnks[0],mnks[1]+1,mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[2] == N-1)&&(status[iii(mnks[0],mnks[1],mnks[2]-1,N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                      if ((mnks[2] == 0)&&(status[iii(mnks[0],mnks[1],mnks[2]+1,N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}

                  }

              }
          }
          if ((!i_proc)&&(removed > 0)) std::cout<<"Removed "<<removed<<"invalid voxels";
      }




      t_2 = MPI_Wtime();
      if (!i_proc) printf( "TIME TO POST-PROCESSING: %f\n", t_2 - t_1 );
      ///============================================================


      // Get ratio
      int vox = status.size();
      int one_vox = 0;
      for (int i = 0; i<status.size(); i++)
      {if (status[i] == 1) one_vox++; }
      RATIO = ((double) one_vox) / ((double) vox);

      std::cout<<"RATIO = "<<RATIO<<std::endl;

      if (RATIO < CRIT_RATIO)
      {
          std::cout<<"CRITICAL RATIO = "<<CRIT_RATIO<<" REACHED. QUIT OPTIMIZATION CYCLE"<<std::endl;
          break;
      }


    level +=dlevel;
  } // END OF OPTIMIZATION CYCLE

  t2 = MPI_Wtime();
  if (!i_proc) printf( "TOTAL COMPUTATION TIME  %f\n", t2 - t1 );


  // Shut down MPI
  MPI_Finalize();


  return 0;

}
