// POBEDA - Parallel Optimization with
// Boundary Elements and Domain Adaptivity

//----------------------------
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <ctime>
#include <pvfmm.hpp>
#include <utils.hpp>
#include "bvp.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"
#include "fmm_summation.h"
#include "matvec.h"
#include "solver_ksp.h"
#include "topo_deriv.h"
#include "point.h"
#include "mesher.h"
#include "auxillary.h"
#include "stl_io.h"
#include <string>

//============================
typedef std::vector<double> dvec;
typedef std::vector<float> fvec;
typedef std::vector<int> ivec;
//============================

//-----Global variables---------------------------------
int N_iter;         // global number of GMRES iterations
int N_cycle;  // global number of the optimization cycle
int smooth_iters; // global number of geometry correction
// iterations


double YOUNG;      // Material's Youngs modulus
double POISS;      // material's Poisson's ratio
double SHEAR;      // material's shear modulus

double RATIO; // Current fraction to leave
double CRIT_RATIO; // Volume fraction to leave

double FUNCTIONAL; // cost functional

bool SAVE_CONF; // Save stl with current configuration
bool SAVE_TD;  // Save topological derivatives
bool VOXEL_PP; // Voxel field post-processing (frames and isolted voxels)
bool PVFMM_PROFILING;
bool TEST_OUTPUT; // Print out surface solution and topological derivatives
bool DIRECT_SUR;  // Direct summation when compute surface matvecs
bool DIRECT_VOL;  // Direct summation when compute volume matvecs
bool WRITE_ALL_TD; // Save all field of topological derivatives, not just slices.
// This option turns off automatically for large models
bool INTERP;       // Use interpolation for near field
//=======================================================

static int Nforbc;

void boundary_conditions(double x, double y, double z, int face_number, int *bc_type, double *tr_di) {
    // Face numbering:
    // 0 - holes
    // 1 - x = 0
    // 2 - x = 1
    // 3 - y = 0
    // 4 - y = 1
    // 5 - z = 0
    // 6 - z = 1

    /*
    // Classic cantilever - line load
    //  for (int n = 0; n<N; n++)
    //  {
    //    force_on_voxel[iiiii(N-1,n,N-1,4,2,N,N,N,6,3)] = -1.0;
    //  }

    // "Interesting" cantilever - point load
    //  force_on_voxel[iiiii(N-1,N/2-1,N-1,4,2,N,N,N,6,3)] = -1.0*(N/2);
    //  force_on_voxel[iiiii(N-1,N/2  ,N-1,4,2,N,N,N,6,3)] = -1.0*(N/2);

    for (int n = 0; n<N; n++)
    {
    force_on_voxel[iiiii(N-1,n,N/2-1,1,2,N,N,N,6,3)] = -0.5;
    force_on_voxel[iiiii(N-1,n,N/2  ,1,2,N,N,N,6,3)] = -0.5;
    }
    */

    double h = 1. / Nforbc;

    // Symmetric cantilever - line load
    switch (face_number) {
        case 1: // X = 0, fixed displacements
            *bc_type = 2;
            tr_di[0] = tr_di[1] = tr_di[2] = 0;
            return;
        case 2: // X = 1, line load
            *bc_type = 1;
            tr_di[0] = tr_di[1] = tr_di[2] = 0;
            if (fabs(z - 0.5) < h)
                tr_di[2] = -0.5/h;
            return;
        default:
            *bc_type = 1;
            tr_di[0] = tr_di[1] = tr_di[2] = 0;
    }
}

int main(int argc, char **argv){

    // Start time measurement
    double t1, t2;

    //----run configuration--------

    SAVE_CONF = true;
    SAVE_TD = true;
    VOXEL_PP = true;
    PVFMM_PROFILING = false;
    TEST_OUTPUT = false;
    DIRECT_SUR = false;
    DIRECT_VOL = false;
    WRITE_ALL_TD = true;
    INTERP = true;

    RATIO = 1.0;       // Current ratio
    CRIT_RATIO = 0.3;  // Crit ratio

    SHEAR = 1.0;
    POISS = 0.3;

    YOUNG = SHEAR * (2 * (1. + POISS));

    smooth_iters = 100;

    int MAX_CYCLES = 10; // if we did not reach our goal in [MAX_CYCLES] iteratios - we are done

    // Good for 200x200x200 model
    //double level = 0.00003; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    //double dlevel = 0.000007;

    // Good for 100x100x100 model
    //double level = 0.0002; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    //double dlevel = 0.00005;

    // Good for 64x64x64 model
    //double level = 0.0004; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    //double dlevel = 0.0001;

    // Good for 20x20x20 model
    //double level = 0.003; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
    //double dlevel = 0.0005;

    double level  =  0.10;
    double dlevel = -0.01;

    //=============================

    N_cycle = 0;

    MPI_Init(&argc, &argv);
    t1 = MPI_Wtime();

    MPI_Comm comm=MPI_COMM_WORLD;

    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm, &n_proc);
    MPI_Comm_rank(comm, &i_proc);

    //--------Read command line options-----------------------------------------------------------------------------------------------------------------------
    commandline_option_start(argc, argv, "\
            This code optimizes topology according to topological derivative, using the PvFMM library.\n");

    commandline_option_start(argc, argv);
    omp_set_num_threads( atoi(commandline_option(argc, argv,  "-omp",     "1", false, "-omp  <int> = (1)    : Number of OpenMP threads."          )));
    size_t   N=(size_t)strtod(commandline_option(argc, argv,    "-N",     "1",  true, "-N    <int>          : Number of source and target points."),NULL);
    int      m =      strtoul(commandline_option(argc, argv,    "-m",    "10", false, "-m    <int> = (10)   : Multipole order (+ve even integer)."),NULL,10);
    commandline_option_end(argc, argv);
    pvfmm::Profile::Enable(PVFMM_PROFILING);
    //========================================================================================================================================================

    //=============================
    
    if (!i_proc) {
        if (sizeof(double) != 8) {
            std::cerr << "sizeof(double) = " << sizeof(double) << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        if (sizeof(float) != 4) {
            std::cerr << "sizeof(float) = " << sizeof(float) << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
    }

    if ((N>100)&&(WRITE_ALL_TD))
    {
        if (!i_proc) std::cout<<"Too large model, skip writing full field of topological derivatives"<<std::endl;
        WRITE_ALL_TD = false;
    }

    // Surface solution
    // Set up test optimization problem

    //=========
    double c = 1. / ((double) N);
    Nforbc = N;


    dvec vert_1(0);
    dvec vert_2(0);
    dvec vert_3(0);
    ivec bc_type(0);
    dvec tr_di(0);


    // At the beginning all the voxels are present
    dvec levelset(N*N*N);
    dvec Lphi(N*N*N);
    ivec is_surf(N*N*N);

    for (int i = 0; i < levelset.size(); i++) {
        levelset[i] = 2.0;
        is_surf[i] = 0;
    }


    //----------------------------------------
    //----------------------------------------
    //-----BEGINNING OF THE OPTIMIZATION CYCLE
    //----------------------------------------
    //----------------------------------------

    for ( int opt_cycle = 0; opt_cycle < MAX_CYCLES; opt_cycle++) {


        double t_1, t_2; // In-cycle time measurements

        N_iter = 0; // GMRES iteration counter

        if (!i_proc) std::cout<<std::endl;
        if (!i_proc) std::cout<<std::endl;
        if (!i_proc) std::cout<<"========="<<std::endl;
        if (!i_proc) std::cout<<"========="<<std::endl;
        if (!i_proc) std::cout<<" CYCLE # "<<opt_cycle<<std::endl;
        if (!i_proc) std::cout<<"========="<<std::endl;
        if (!i_proc) std::cout<<"========="<<std::endl;
        if (!i_proc) std::cout<<std::endl;
        if (!i_proc) std::cout<<std::endl;

        ///----GENERATE BVP--------------------------------------------
        t_1 = MPI_Wtime();
        generate_BVP( N,N,N,c,
                levelset, is_surf,
                vert_1,vert_2,vert_3,
                tr_di,bc_type,comm);
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO GENERATE BVP: %f\n", t_2 - t_1 );
        ///============================================================

        ///----SAVE CONFIGURATION--------------------------------------
        t_1 = MPI_Wtime();
        if ((!i_proc)&&(SAVE_CONF)){
            write_stl1("smooth/CONF/Conf_N_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), vert_1, vert_2, vert_3);
        }
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO SAVE CONF: %f\n", t_2 - t_1 );
        ///============================================================
        //
        // Get ratio
        int vox = levelset.size();
        int one_vox = 0;
        for (int i = 0; i< levelset.size(); i++)
            if (levelset[i] > 1)
                one_vox++;

        RATIO = ((double) one_vox) / ((double) vox);

        if (!i_proc) std::cout<<"VOLUME_RATIO = "<<RATIO<<std::endl;

        if (RATIO < CRIT_RATIO) { 
            if (!i_proc) std::cout<<"CRITICAL RATIO = "<<CRIT_RATIO<<" REACHED. QUIT OPTIMIZATION CYCLE"<<std::endl;
            break;
        }

        ///----SCATTER SURFACE PROBLEM OVER PROCESSES--------------------------
        t_1 = MPI_Wtime();
        int N_trg = vert_1.size()/3.; // note - N_trg is global for all proc
        int start, end, span, step;
        if (n_proc == 1) step = N_trg;
        if (n_proc > 1) step = (int) std::floor(N_trg / (double) (n_proc));
        start = step * i_proc;
        end   = step *(i_proc+1);
        if (i_proc == n_proc-1) end = N_trg;
        span = end - start;
        for (int i = 0; i<span; i++)
        {
            // single component vectors
            bc_type[i] = bc_type[start+i];
            // tree component vectors
            for (int k=0;k<3;k++)
            {
                vert_1[3*i+k] = vert_1[3*start+3*i+k];
                vert_2[3*i+k] = vert_2[3*start+3*i+k];
                vert_3[3*i+k] = vert_3[3*start+3*i+k];
                tr_di[3*i+k] = tr_di[3*start+3*i+k];
            }
        }
        bc_type.resize(span);
        vert_1.resize(3*span);
        vert_2.resize(3*span);
        vert_3.resize(3*span);
        tr_di.resize(3*span);
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO SCATTER SURF: %f\n", t_2 - t_1 );
        ///============================================================

        ///----SURFACE SOLUTION----------------------------------------
        t_1 = MPI_Wtime();
        // Cash vector with knowns
        dvec buff(tr_di.size());
        for (int i = 0; i<tr_di.size(); i++)
            buff[i] = tr_di[i];
        SolverFmmGmres * slvr = new SolverFmmGmres();
        slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di,m, comm);
        slvr->solve(m, comm, 1000, 1e-5);
        slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di,m, comm);
        delete slvr; slvr = nullptr;
        // Sort the obtained solution onto tractions and displacements
        dvec tractions(buff.size()), displacements(buff.size());
        for (int i = 0; i<bc_type.size();i++) {
            if (bc_type[i] == 1) {
                for (int k = 0; k<3; k++) { 
                    tractions[3*i+k] =  buff[3*i+k];
                    displacements[3*i+k] = tr_di[3*i+k];
                }
            }
            if (bc_type[i] == 2) {
                for (int k = 0; k<3; k++) {   
                    tractions[3*i+k] = tr_di[3*i+k];
                    displacements[3*i+k] = buff[3*i+k];
                }
            }
        }
        if (TEST_OUTPUT) {
            for (int i = 0; i<tractions.size()/3; i++)
                std::cout<<"I_PROC="<<i_proc<<" , tractions ["<<i<<"] = ("<<tractions[3*i+0]
                    <<" , "<<tractions[3*i+1]<<" , "<<tractions[3*i+2]<<")"<<std::endl;

            for (int i = 0; i<displacements.size()/3; i++)
                std::cout<<"I_PROC="<<i_proc<<" , displacements ["<<i<<"] = ("<<displacements[3*i+0]
                    <<" , "<<displacements[3*i+1]<<" , "<<displacements[3*i+2]<<")"<<std::endl;
        }
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO SOLVE SURFACE BVP: %f\n", t_2 - t_1 );
        ///============================================================
        //
        ///----SAVE SURFACE SOLUTION ----------------------------------
        t_1 = MPI_Wtime();
        if (SAVE_CONF) {
            write_surf_vtk("smooth/SURF/surf_proc" + std::to_string((long long)i_proc)+ 
                "_N_"+std::to_string((long long) N)+
                "_m_"+std::to_string((long long) m)+
                "_iter_" + std::to_string((long long)opt_cycle),
                vert_1, vert_2, vert_3, tractions, displacements);
        }
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO SAVE SURFACE SOLUTION: %f\n", t_2 - t_1 );

        ///----SCATTER VOL TARGETS-----------------------------------------
        t_1 = MPI_Wtime();
        int N_vol = N*N*N;

        double dx = 1. / N;

        struct simplex {
            point p[8];
            double h;
            simplex(double h, double scale = .7) : h(h) {
                for (int i = 0; i < 2; i++)
                    for (int j = 0; j < 2; j++)
                        for (int k = 0; k < 2; k++)
                            p[4*i+2*j+k] = point(i-.5,j-.5,k-.5)*scale*h;
            }
            const point center(int i, int j, int k) const {
                return point(.5+i, .5+j, .5+k) * h;
            }
            // Interpolate value at pr given values at vertices
            double operator()(const double v[8], const point &pr) const {
                double x = pr.x;
                double y = pr.y;
                double z = pr.z;
                double s = 0;
                for (int i = 0; i < 2; i++)
                    for (int j = 0; j < 2; j++)
                        for (int k = 0; k < 2; k++) {
                            const point &pi = p[4*i+2*j+k];
                            double xi = x / pi.x;
                            double eta = y / pi.y;
                            double zeta = z / pi.z;
                            s += v[4*i+2*j+k] * (1+xi)*(1+eta)*(1+zeta);
                        }
                return 0.125 * s;
            }
        };

        const simplex splx(c, .5);
        const int ptspervox = 8;
        const int coordpervox = ptspervox * 3; // coord per voxel

        dvec coordinates;
        for (int mm = 0; mm<N; mm++)
            for (int nn = 0; nn<N; nn++)
                for (int kk = 0; kk<N; kk++) {
                    const point pc(splx.center(mm,nn,kk)); // voxel center
                    for (int j = 0; j < ptspervox; j++) {
                        coordinates.push_back(splx.p[j].x + pc.x);
                        coordinates.push_back(splx.p[j].y + pc.y);
                        coordinates.push_back(splx.p[j].z + pc.z);
                    }
                }

        // Scatter targets over processes
        if (n_proc == 1) step = N_vol;
        if (n_proc > 1) step = (int) std::floor(N_vol / (double) (n_proc));
        start = step * i_proc;
        end   = step *(i_proc+1);
        if (i_proc == n_proc-1) end = N_vol;
        span = end - start;
        for (int i = 0; i<span; i++)
            for (int j = 0; j < coordpervox; j++)
                coordinates[coordpervox * i + j] = coordinates[coordpervox * (start+i) + j];
        coordinates.resize(coordpervox * span);
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO SCATTER VOL: %f\n", t_2 - t_1 );
        ///============================================================

        ///-------VOLUME SOLUTION--------------------------------------
        t_1 = MPI_Wtime();
        dvec topo_derivs(coordinates.size() / 3);
        dvec ener_density(coordinates.size() / 3);
        dvec stresses(coordinates.size() * 3);
        // Solve for stresses
        volume_matvec(1, vert_1,vert_2,vert_3,
                coordinates,tractions,
                displacements,stresses, m, comm);
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO VOLUME SOLUTION: %f\n", t_2 - t_1 );
        ///============================================================

        ///------COMPUTATION OF ENERGIES AND TOPOLOGICAL DERIVATIVES---
        t_1 = MPI_Wtime();
        topo_deriv( stresses, topo_derivs, SHEAR, POISS);
        ener_dens( stresses, ener_density, c, YOUNG, POISS);

        if (TEST_OUTPUT){
            std::cout<<"TOPOLOGICAL DERIVATIVES"<<std::endl;
            for (int i = 0; i<topo_derivs.size(); i++) {
                point p(0, 0, 0);
                for (int j = 0; j < ptspervox; j++)
                    p = p + point(
                        coordinates[coordpervox * i + 3 * j + 0],
                        coordinates[coordpervox * i + 3 * j + 1],
                        coordinates[coordpervox * i + 3 * j + 2]
                    );
                p = p * (1. / ptspervox);
                std::cout<<"At "<< p.str() << " - " << splx(&topo_derivs[ptspervox * i], point(0, 0, 0)) <<std::endl;
            }
        }
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO FUNC AND TDs: %f\n", t_2 - t_1 );
        ///============================================================

        ///------GATHER------------------------------------------------
        t_1 = MPI_Wtime();
        int sendcount = topo_derivs.size();
        dvec sendarray(sendcount);
        dvec sendarray2(sendcount);
        for (int i = 0; i<sendcount; i++)
        {
            sendarray[i] = topo_derivs[i];
            sendarray2[i] = ener_density[i];
        }
        dvec rbuf(ptspervox * N_vol);
        dvec rbuf2(ptspervox * N_vol);
        int recvcounts[n_proc];
        int displs[n_proc];
        for (int i = 0; i<n_proc; i++)
        {
            int start, end, span, step;
            if (n_proc==1) step = ptspervox * N_vol;
            if (n_proc>1) step = (int) std::floor(N_vol / (double) (n_proc));
            start = ptspervox * step * i;
            end   = ptspervox * step *(i+1);
            if (i == n_proc-1) end = ptspervox * N_vol;
            recvcounts[i] = end - start;
            if (i == 0) displs[i] = 0;
            if (i > 0) displs[i] = displs[i-1] + recvcounts[i-1];
        }
        // Gather TDs and cost func to ALL the processes
        for (int i = 0; i<n_proc; i++){
            MPI_Gatherv(sendarray.data(), sendcount, MPI_DOUBLE,rbuf.data(), recvcounts, displs,MPI_DOUBLE, i, comm);
            MPI_Gatherv(sendarray2.data(), sendcount, MPI_DOUBLE,rbuf2.data(), recvcounts, displs,MPI_DOUBLE, i, comm);
        }
        topo_derivs.resize(ptspervox * N_vol);
        ener_density.resize(ptspervox * N_vol);
        for (int i = 0; i<ptspervox * N_vol; i++) {
            topo_derivs[i] = rbuf[i];
            ener_density[i] = rbuf2[i];
        }
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO GATHER: %f\n", t_2 - t_1 );
        ///============================================================

        // INTERPOLATION
        
/*        {
            std::ofstream f("smooth/TD/ALL_TD." + std::to_string(static_cast<long long>(opt_cycle))+ ".csv");
            for (int i = 0; i < N_vol; i++)
                for (int j = 0; j < ptspervox; j++) {
                    const point p(
                        coordinates[coordpervox * i + 3 * j + 0],
                        coordinates[coordpervox * i + 3 * j + 1],
                        coordinates[coordpervox * i + 3 * j + 2]
                    );
                    f << p.x << "," << p.y << "," << p.z << "," << is_surf[i] << "," << topo_derivs[ptspervox * i + j] << "\n";
                }
        }*/

        const double inf = 1e20;
        FUNCTIONAL = 0;

        dvec new_TD(N_vol);
        for (int i = 0; i < N_vol; i++)
            if (levelset[i] <= 1)
                new_TD[i] = inf; // will be cut by min(levelset[i], TD[i] / level)
            else if (is_surf[i] == 0) {
                new_TD[i] = splx(&topo_derivs[ptspervox * i], point(0, 0, 0));
                FUNCTIONAL += splx(&ener_density[ptspervox * i], point(0, 0, 0));
            } else {// levelset[i] > 1 & surf = 1, Find closest and interpolate
                ivec mnks = mnk(i,N,N,N);
                int radius = 4;
                int m0 = mnks[0], n0 = mnks[1], k0 = mnks[2];
                double sum = 0;
                double sumfunc = 0;
                double sumw = 0;
                int cnt = 0;
                const point &trgt = splx.center(m0, n0, k0);
                for (int m = m0 - radius; m <= m0 + radius; m++) {
                    if (m < 0 || m >= N) continue;
                    for (int n = n0 - radius; n <= n0 + radius; n++) {
                        if (n < 0 || n >= N) continue;
                        for (int k = k0 - radius; k <= k0 + radius; k++) {
                            if (k < 0 || k >= N) continue;
                            int in = iii(m, n, k, N, N, N);
                            if (levelset[in] <= 1 || is_surf[in] == 1)
                                continue;
                            double dist2 = pow(m - m0, 2) + pow(n - n0, 2) + pow(k - k0, 2);
                            double w = pow(dist2, -1.5);
                            const point &src = splx.center(m, n, k);
                            double v = splx(&topo_derivs[ptspervox*in], trgt - src);
                            double func = splx(&ener_density[ptspervox*in], trgt - src);
                            sum += w * v;
                            sumfunc += w * func;
                            sumw += w;
                            cnt ++;
                        }
                    }
                }
                if (sumw == 0) {
                    if (!i_proc) std::cout << "No data to extrapolate TD into voxel ("
                        << m0 << ", " << n0 << ", " << k0 << "). Set TD=-inf" << std::endl;
                    sum = -inf;
                    sumfunc = 0;
                    sumw = 1;
                    cnt = 1;
                }
                double avg = sum / sumw;
                double avgfunc = sumfunc / sumw;
                new_TD[i] = avg;
                // XXX: multiply by cell volume fraction cut by surface
                FUNCTIONAL += avgfunc;
            }

        topo_derivs = new_TD;
        new_TD.clear();
        if (!i_proc) std::cout<<"COST FUNCTIONAL (STRAIN ENERGY):"<<FUNCTIONAL<<std::endl;
        ///============================================================

        ///------POST-PROCESSING----------------------------------------
        t_1 = MPI_Wtime();

        // Determine span of TDs and absolute cutoff level

        dvec tdsort;
        for (int i = 0; i<topo_derivs.size(); i++)
            if (topo_derivs[i] < inf)
                tdsort.push_back(topo_derivs[i]);

        std::sort(tdsort.begin(), tdsort.end());
        int lo = 0.8 * level * tdsort.size();
        int hi = 1.2 * level * tdsort.size();

        double TD_min = tdsort.front();
        double TD_max = tdsort.back();
        double abs_level;

        double gap = -1;
        for (int i = lo; i < hi; i++) {
            if (tdsort[i+1] - tdsort[i] > gap) {
                abs_level = 0.5*(tdsort[i] + tdsort[i+1]);
                gap = tdsort[i+1] - tdsort[i];
            }
            if (tdsort[i] > -inf && TD_min == -inf)
                TD_min = tdsort[i];
        }
        if (gap < 0)
            abs_level = tdsort[lo];

        if (abs_level == -inf)
            abs_level = -0.1 * inf;

        if (!i_proc){
            std::cout<<"TD_min = "<<TD_min<<std::endl;
            std::cout<<"TD_max = "<<TD_max<<std::endl;
            std::cout<<"abs_level = "<<abs_level<<std::endl;
        }

        // Cutting voxels
        for (int i = 0; i<topo_derivs.size(); i++) {
            double newval = (topo_derivs[i] - TD_min) / (abs_level - TD_min);
            if (newval < levelset[i])
                levelset[i] = newval > 0 ? newval : 0;
        }

        //Cutting isolated voxels
        if (VOXEL_PP){
            int removed = 0;
            for (int si = 0; si<smooth_iters; si++) {
                int lastrem = 0;
                for (int i = 0; i<levelset.size();i++) {
                    if (levelset[i] <= 1)
                        continue;
                    
                    int neighbors = 0;
                    ivec mnks = mnk(i,N,N,N);
                    int m0 = mnks[0];
                    int n0 = mnks[1];
                    int k0 = mnks[2];

                    int xnb = 0, ynb = 0, znb = 0;

                        // 6-neighborhood
                    if ((m0 < N-1)&&(levelset[iii(m0+1,n0,  k0  ,N,N,N)]>1)) xnb++;
                    if ((m0 > 0  )&&(levelset[iii(m0-1,n0,  k0  ,N,N,N)]>1)) xnb++;
                    if ((n0 < N-1)&&(levelset[iii(m0  ,n0+1,k0  ,N,N,N)]>1)) ynb++;
                    if ((n0 > 0  )&&(levelset[iii(m0  ,n0-1,k0  ,N,N,N)]>1)) ynb++;
                    if ((k0 < N-1)&&(levelset[iii(m0  ,n0,  k0+1,N,N,N)]>1)) znb++;
                    if ((k0 > 0  )&&(levelset[iii(m0,  n0,  k0-1,N,N,N)]>1)) znb++;

                    if ((xnb > 0) + (ynb > 0) + (znb > 0) < 2) {
                        levelset[i] = 0;
                        removed++;
                        lastrem++;
                    }
                }
                if (lastrem == 0)
                    break;
            }
            if ((!i_proc)&&(removed > 0)) std::cout<<"Removed "<<removed<<"invalid voxels"<<std::endl;
        }

        // Smoothing levelset
        for (int smooth_it = 0; smooth_it < 1; smooth_it++) {
            for (int i = 0; i < topo_derivs.size(); i++) {
                Lphi[i] = 0;

                ivec mnks = mnk(i,N,N,N);
                int m = mnks[0];
                int n = mnks[1];
                int k = mnks[2];
                if (m > 0)
                    Lphi[i] += levelset[iii(m-1,n,k,N,N,N)] - levelset[i];
                if (n > 0)
                    Lphi[i] += levelset[iii(m,n-1,k,N,N,N)] - levelset[i];
                if (k > 0)
                    Lphi[i] += levelset[iii(m,n,k-1,N,N,N)] - levelset[i];
                if (m < N-1)
                    Lphi[i] += levelset[iii(m+1,n,k,N,N,N)] - levelset[i];
                if (n < N-1)
                    Lphi[i] += levelset[iii(m,n+1,k,N,N,N)] - levelset[i];
                if (k < N-1)
                    Lphi[i] += levelset[iii(m,n,k+1,N,N,N)] - levelset[i];
            }

            const double cou = 1. / 6;

            for (int i = 0; i<topo_derivs.size(); i++)
                levelset[i] += cou * Lphi[i];
        }

        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO POST-PROCESSING: %f\n", t_2 - t_1 );
        ///============================================================

        ///------SAVE TDs------------------------------------------------
        t_1 = MPI_Wtime();
        if ((!i_proc)&&(SAVE_TD)){

            // Save all TDs in volume
            if (WRITE_ALL_TD) write_vtk("smooth/TD/td_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle) + ".vtk", N, topo_derivs, levelset, is_surf);

            // Save XY, YZ, XZ slices through the center of the domain
            write_td_slice("smooth/TD/td_slice_xy_N_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), topo_derivs, N, N, N, 1, (int) floor(N/2.));
            write_td_slice("smooth/TD/td_slice_yz_N_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), topo_derivs, N, N, N, 2, (int) floor(N/2.));
            write_td_slice("smooth/TD/td_slice_xz_N_"+std::to_string((long long) N)+"_m_"+std::to_string((long long) m)+"_iter_" + std::to_string((long long) opt_cycle), topo_derivs, N, N, N, 3, (int) floor(N/2.));
        }
        t_2 = MPI_Wtime();
        if (!i_proc) printf( "TIME TO SAVE TDs: %f\n", t_2 - t_1 );
        ///============================================================

        level += dlevel;
    } // END OF OPTIMIZATION CYCLE

    t2 = MPI_Wtime();
    if (!i_proc) printf( "TOTAL COMPUTATION TIME  %f\n", t2 - t1 );


    // Shut down MPI
    MPI_Finalize();

    return 0;

}
