#include <iostream>
#include <petscksp.h>
#include <petsctao.h>
#include "solver_ksp.h"
#include "bvp.h"
#include "mpi.h"
#include "auxillary.h"
#include "fmm_summation.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"

using  std::cout;
using  std::endl;

///////////////////////////////////////////////////////////////////////////////

SolverFmmGmres::SolverFmmGmres():
SRC_PER_TRG_(16),
DIM_(3),
SURF_DIM_(6),
N_vox_(-1),
vert_1_(nullptr),
vert_2_(nullptr),
vert_3_(nullptr),
bc_type_(nullptr),
tract_(nullptr),
displ_(nullptr),
tr_di_(nullptr),
max_pts_(100),
pvfmm_tree_(nullptr) {

//    cout << "Solver Fmm Gmres" << endl;
}

SolverFmmGmres::~SolverFmmGmres() {

    vert_1_ = nullptr;
    vert_2_ = nullptr;
    vert_3_ = nullptr;
    bc_type_ = nullptr;
    tract_ = nullptr;
    displ_ = nullptr;
    tr_di_ = nullptr;

    //  !!!
    // deallocate pvfmm tree.
}

bool SolverFmmGmres::init(const int Nvox_ext, const dvec & vert_1_ext, const dvec & vert_2_ext,
                          const dvec & vert_3_ext, const ivec & bc_type_ext,
                          const dvec & tract_ext, const dvec & displ_ext,
                          dvec & tr_di_ext) {
    N_vox_  = Nvox_ext;

    vert_1_ = &vert_1_ext;
    vert_2_ = &vert_2_ext;
    vert_3_ = &vert_3_ext;

    bc_type_ = &bc_type_ext;

    tract_ = &tract_ext;
    displ_ = &displ_ext;
    tr_di_ = &tr_di_ext;

    return true;
}

int SolverFmmGmres::solve(const int m_MultOrder, MPI_Comm comm, const int max_Iter, const double tol) {

    cout << "Surface solution Solver starts... " << endl;

    //pvfmm_tree_ = PtFMM_CreateTree(src_coord, src_value, surf_coord, surf_value, trg_coord, comm, max_pts, pvfmm::FreeSpace);


    dvec b_rhs(*tr_di_);
    // 1. Build rhs. (vector b)
    calc_rhs_b_(*tr_di_, b_rhs, m_MultOrder, comm);
    // now we have b_rhs for matvecs

    // 1.999  define matcvecs using petsc classes.
    // 2.     call GMRes


    // ..

    return 0;
}

void SolverFmmGmres::calc_rhs_b_(const dvec & v_inp, dvec & Av_out,
                                 const int mult_order, MPI_Comm comm) {

    // This function performs multiplication BB*bcs and generates rhs

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matdvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    // Temporary references.
    const dvec & vert_1 = *vert_1_;
    const dvec & vert_2 = *vert_2_;
    const dvec & vert_3 = *vert_3_;
    const ivec & bc_te = *bc_type_;

    ivec bc_t(bc_te.size());
    for (int i = 0; i<bc_t.size(); i++) bc_t[i] = 3 - bc_te[i]; // Only PP matrix

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt ++;
        if (bc_t[i]==2) src_cnt ++;
    }
    src_cnt *=SRC_PER_TRG_; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG_;
    dvec src_coord(DIM_ * src_cnt);
    dvec src_value(DIM_ * src_cnt);
    dvec surf_coord(DIM_ * surf_cnt);
    dvec surf_value(SURF_DIM_ * surf_cnt);
    dvec trg_coord(DIM_ * trg_cnt);
    dvec trg_value(DIM_ * trg_cnt);

    // Setting up FMM arrays

    for (int i = 0; i < DIM_ * trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM_ * SRC_PER_TRG_;
    int bv_size = SURF_DIM_ * SRC_PER_TRG_;
    dvec sbuff_coord(bc_size);
    dvec sbuff_src_v(bc_size);
    dvec sbuff_surf_v(bv_size);

    dvec v1(DIM_), v2(DIM_), v3(DIM_), val(DIM_);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM_; k++)
        {
            v1[k] = vert_1[i*DIM_+k];
            v2[k] = vert_2[i*DIM_+k];
            v3[k] = vert_3[i*DIM_+k];
            val[k] = v_inp[i*DIM_+k];
        }

        if (bc_t[i]==1)
        {
            //triangle2src(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            //triangle2src(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }

//    calculate_displacements(
//            src_coord,
//            src_value,
//            surf_coord,
//            surf_value,
//            trg_coord,
//            trg_value,
//            mult_order,
//            comm);


    for (int i = 0; i < DIM_ * trg_cnt; i++) Av_out[i] = trg_value[i];

    //=========================================================
    // 2) LOCAL pass
    //=========================================================

    // a) add (1/2)*delta_ij * u_j = 1/2 * u_i

    for (int i = 0; i < trg_cnt; i++) {
        if (bc_t[i]==1) {
            for (int k = 0; k<DIM_; k++) Av_out[DIM_ * i + k] += 0.5 * v_inp[DIM_ * i + k];
        }
    }

    // b) Subtract-add cycle
    dvec add(DIM_);
    dvec subtr(DIM_);
    for (int i = 0; i < trg_cnt; i++)
    {

        for (int k = 0; k<DIM_; k++)
        {
            v1[k] = vert_1[i*DIM_+k];
            v2[k] = vert_2[i*DIM_+k];
            v3[k] = vert_3[i*DIM_+k];
            val[k] = v_inp[i*DIM_+k];
        }
        //local_pass( subtr, add, v1, v2, v3, 3-bc_t[i], val);

        for (int k = 0; k < DIM_; k++)
        {
            if (bc_t[i]==2)
            { subtr[k] = - subtr[k]; add[k] = - add[k]; } // U matr comes with minus sign
            Av_out[DIM_*i+k] -= subtr[k];
            Av_out[DIM_*i+k] += add[k];
        }
    }
    for (int i = 0; i<Av_out.size(); i++){Av_out[i] = -Av_out[i];}
}

void SolverFmmGmres::local_pass_(dvec & sub, dvec & add, const dvec & v_1, const dvec & v_2,
                                 const dvec & v_3, const int bc, const dvec & val) {

    const double G  = 1.;
    const double nu = 0.3;

    dvec csi(DIM_);
    for (int i = 0; i < DIM_; i++) {
        csi[i] = (1./3.) * (v_1[i] + v_2[i] + v_3[i]);
    }
    //============================================
    //             Add value
    //============================================

    // (invert bc when using for RHS correction)
    for (int i = 0; i<DIM_; i++) add[i] = 0;
    if (bc==1)
    {
        for (int i = 0; i<DIM_; i++)
        {
            for (int j = 0; j<DIM_; j++)
            {
                add[i] += sint_u(i+1, j+1, v_1, v_2, v_3, csi, G, nu)* val[j];
            }
        }
    }
    if (bc==2)
    {
        for (int i = 0; i<DIM_; i++)
        {
            for (int j = 0; j<DIM_; j++)
            {
                add[i] += sint_p(i+1, j+1, v_1, v_2, v_3, csi, nu)* val[j];
            }
        }
    }

    //for (int i = 0; i<3; i++) {add[i] = 0;}

    //=========================================
    //             Sub value
    //=========================================

    for (int i = 0; i<DIM_; i++) sub[i] = 0;

    int bc_size = DIM_ * SRC_PER_TRG_; // 3 * (sources_per_triangle)
    int bv_size = SURF_DIM_ * SRC_PER_TRG_; // 6 * (sources_per_triangle)

    dvec src_coord(bc_size);
    dvec src_value(bc_size);

    dvec surf_coord(bc_size);
    dvec surf_value(bv_size);

    dvec x(DIM_), d(DIM_), n(DIM_);
    if (bc==1)
    {
        triangle2src_(1, v_1, v_2, v_3, src_coord, src_value, val);

        // Direct summation over src and surf
        for (int s=0; s< src_coord.size()/3; s++)
        {
            x[0] = src_coord[s*DIM_];
            x[1] = src_coord[s*DIM_+1];
            x[2] = src_coord[s*DIM_+2];

            d[0] = src_value[s*DIM_];
            d[1] = src_value[s*DIM_+1];
            d[2] = src_value[s*DIM_+2];

            for (int i = 0; i<DIM_; i++){
                for (int j = 0; j<DIM_; j++){
                    sub[i] += U_ij( i+1, j+1, csi, x, G, nu ) * d[j];
                }
            }
        }
    }

    if (bc==2)
    {
        triangle2src_(2, v_1, v_2, v_3, surf_coord, surf_value, val);

        // Direct summation over src and surf
        for (int s=0; s< src_coord.size()/3; s++)
        {
            x[0] = surf_coord[s*DIM_];
            x[1] = surf_coord[s*DIM_+1];
            x[2] = surf_coord[s*DIM_+2];

            d[0] = surf_value[s*SURF_DIM_];
            d[1] = surf_value[s*SURF_DIM_+1];
            d[2] = surf_value[s*SURF_DIM_+2];

            n[0] = surf_value[s*SURF_DIM_+3];
            n[1] = surf_value[s*SURF_DIM_+4];
            n[2] = surf_value[s*SURF_DIM_+5];

            for (int i = 0; i<DIM_; i++){
                for (int j = 0; j<DIM_; j++){
                    sub[i] += P_ij( i+1, j+1, csi, x, n, nu ) * d[j];
                }
            }
        }
    }
    //for (int i = 0; i<3; i++) {sub[i] = 0;}
}

void SolverFmmGmres::triangle2src_(int mode, const dvec & v_1, const dvec & v_2, const dvec & v_3,
                                   dvec & src_coord, dvec & src_value, dvec & val) {

    // Normal components by
    // Cubic Gauss quadrature points and weights
    dvec eta_1(4);
    eta_1 = { 1./3., 3./5., 1./5., 1./5. };
    dvec eta_2(4);
    eta_2 = { 1./3., 1./5., 3./5., 1./5. };
    dvec eta_3(4);
    for (int i = 0; i < 4; i++) {
        eta_3[i] = 1. - eta_1[i] - eta_2[i];
    }
    dvec ww_i(4);
    ww_i =  {-9.0/32.0, 25.0/96.0, 25.0/96.0, 25.0/96.0};

    // Split triangle into 4 pieces
    dvec v_12(DIM_);
    for (int i = 0; i < DIM_; i++) {
        v_12[i] = (1./2.) * (v_1[i] + v_2[i]);
    }
    dvec v_13(DIM_);
    for (int i = 0; i < DIM_; i++) {
        v_13[i] = (1./2.) * (v_1[i] + v_3[i]);
    }
    dvec v_23(DIM_);
    for (int i = 0; i < DIM_; i++) {
        v_23[i] = (1./2.) * (v_2[i] + v_3[i]);
    }

    int i = 0; // Point index
    dvec x_1(DIM_);
    dvec x_2(DIM_);
    dvec x_3(DIM_);
    for (int t = 0; t < 4; ++t) // cycle over triangles
    {
        if (t==0) for (int v = 0; v < DIM_; v++) { x_1[v] = v_1[v];  x_2[v] = v_12[v]; x_3[v] = v_13[v];}
        if (t==1) for (int v = 0; v < DIM_; v++) { x_1[v] = v_2[v];  x_2[v] = v_23[v]; x_3[v] = v_12[v];}
        if (t==2) for (int v = 0; v < DIM_; v++) { x_1[v] = v_3[v];  x_2[v] = v_13[v]; x_3[v] = v_23[v];}
        if (t==3) for (int v = 0; v < DIM_; v++) { x_1[v] = v_12[v]; x_2[v] = v_23[v]; x_3[v] = v_13[v];}

        for (int c = 0; c<4; c++ ) // cycle over gauss points
        {
            for (int m = 0; m<DIM_; m++)
            {
                if (mode == 1){
                    src_coord[DIM_*i + m] = x_1[m] * eta_1[c] + x_2[m] * eta_2[c] + x_3[m] * eta_3[c];
                    src_value[DIM_*i + m] = ww_i[c] * 2.0 * area(x_1, x_2, x_3) * val[m];
                }

                if (mode == 2){
                    src_coord[DIM_*i + m] = x_1[m] * eta_1[c] + x_2[m] * eta_2[c] + x_3[m] * eta_3[c];
                    src_value[SURF_DIM_*i + m] = ww_i[c] * 2.0 * area(x_1, x_2, x_3) * val[m];
                    src_value[SURF_DIM_*i + DIM_ + m] = norm(v_1, v_2, v_3)[m];
                }
            }
            i++;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////











////    bool  init(dvec & dv1, dvec & dv1, dvec & dv1);
////    bool  solve(const double tol, MPI_Comm comm);

//typedef struct {
//    PetscReal  param;      /* nonlinearity parameter */
//    PetscInt   mx, my;     /* discretization in x- and y-directions */
//    PetscInt   ndim;       /* problem dimension */
//    Vec        s, y, xvec; /* work space for computing Hessian */
//    PetscReal  hx, hy;     /* mesh spacing in x- and y-directions */
//} AppCtx;

//static  char help[] = "Demonstrates use of the TAO package to solve \n\
//unconstrained minimization problems on a single processor.  This example \n\
//is based on the Elastic-Plastic Torsion (dept) problem from the MINPACK-2 \n\
//test suite.\n\
//The command line options are:\n\
//  -mx <xg>, where <xg> = number of grid points in the 1st coordinate direction\n\
//  -my <yg>, where <yg> = number of grid points in the 2nd coordinate direction\n\
//  -par <param>, where <param> = angle of twist per unit length\n\n";


//PetscErrorCode HessianProductMat(Mat,Vec,Vec);
//PetscErrorCode HessianProduct(void*,Vec,Vec);

//bool  SolverFmmGmres::test_matrixfree_matvec() {

//    PetscErrorCode     ierr;                /* used to check for functions returning nonzeros */
//    PetscInt           mx=10;               /* discretization in x-direction */
//    PetscInt           my=10;               /* discretization in y-direction */
//    Vec                x;                   /* solution, gradient vectors */
//    PetscBool          flg;                 /* A return value when checking for use options */
//    Tao                tao;                 /* Tao solver context */
//    Mat                H;                   /* Hessian matrix */
//    TaoConvergedReason reason;
//    KSP                ksp;                 /* PETSc Krylov subspace solver */
//    AppCtx             user;                /* application context */
//    PetscMPIInt        size;                /* number of processes */
//    PetscReal          one=1.0;
//    int argc = 0;
//    char ** argv = nullptr;

//    /* Initialize TAO,PETSc */
//    PetscInitialize(&argc,&argv,(char *)0,help);

//    MPI_Comm_size(MPI_COMM_WORLD,&size);
//    if (size >1) SETERRQ(PETSC_COMM_SELF,1,"Incorrect number of processors");

//    /* Specify default parameters for the problem, check for command-line overrides */
//    user.param = 5.0;
//    PetscOptionsGetInt(NULL, NULL,"-my",&my,&flg);
//    PetscOptionsGetInt(NULL, NULL,"-mx",&mx,&flg);
//    PetscOptionsGetReal(NULL, NULL,"-par",&user.param,&flg);

//    PetscPrintf(PETSC_COMM_SELF,"\n---- Elastic-Plastic Torsion Problem -----\n");
//    PetscPrintf(PETSC_COMM_SELF,"mx: %D     my: %D   \n\n",mx,my);

//    user.ndim = mx * my; user.mx = mx; user.my = my;
//    user.hx = one/(mx+1); user.hy = one/(my+1);

//    /* Allocate vectors */
//    VecCreateSeq(PETSC_COMM_SELF,user.ndim,&user.y);
//    VecDuplicate(user.y,&user.s);
//    VecDuplicate(user.y,&x);

//    /* Create TAO solver and set desired solution method */
//    TaoCreate(PETSC_COMM_SELF,&tao);
//    TaoSetType(tao,TAOLMVM);

//    /* Set solution vector with an initial guess */
////    FormInitialGuess(&user,x);
////    TaoSetInitialVector(tao,x);

//    /* Set routine for function and gradient evaluation */
////    TaoSetObjectiveAndGradientRoutine(tao,FormFunctionGradient,(void *)&user);

////           128:   /* From command line options, determine if using matrix-free hessian */
////    PetscOptionsHasName(NULL,"-my_tao_mf",&flg);
////           130:   if (flg) {
//        MatCreateShell(PETSC_COMM_SELF,user.ndim,user.ndim,user.ndim,user.ndim,(void*)&user,&H);
//        MatShellSetOperation(H,MATOP_MULT,(void(*)(void))HessianProductMat);
//          //SolverFmmGmres::apply_matvec(Mat mat,Vec svec,Vec y)
////        MatShellSetOperation(H,MATOP_MULT,(void(*)(void)) apply_matvec);


////           133:     MatSetOption(H,MAT_SYMMETRIC,PETSC_TRUE);

////           135:     TaoSetHessianRoutine(tao,H,H,MatrixFreeHessian,(void *)&user);

////           137:     /* Set null preconditioner.  Alternatively, set user-provided
////           138:        preconditioner or explicitly form preconditioning matrix */
////           139:     PetscOptionsSetValue("-pc_type","none");
////           140:   } else {
////           141:     MatCreateSeqAIJ(PETSC_COMM_SELF,user.ndim,user.ndim,5,NULL,&H);
////           142:     MatSetOption(H,MAT_SYMMETRIC,PETSC_TRUE);
////           143:     TaoSetHessianRoutine(tao,H,H,FormHessian,(void *)&user);
////           144:   }


////           147:   /* Check for any TAO command line options */
////           148:   TaoSetFromOptions(tao);

////           150:   /* SOLVE THE APPLICATION */
////           151:   TaoSolve(tao);
////           152:   TaoGetKSP(tao,&ksp);
////           153:   if (ksp) {
////           154:     KSPView(ksp,PETSC_VIEWER_STDOUT_SELF);
////           155:   }

////           157:   /*
////           158:      To View TAO solver information use
////           159:       TaoView(tao);
////           160:   */

////           162:   /* Get information on termination */
////           163:   TaoGetConvergedReason(tao,&reason);
////           164:   if (reason <= 0){
////           165:     PetscPrintf(PETSC_COMM_WORLD,"Try a different TAO method, adjust some parameters, or check the function evaluation routines\n");
////           166:   }

//    TaoDestroy(&tao);
//    VecDestroy(&user.s);
//    VecDestroy(&user.y);
//    VecDestroy(&x);
//    MatDestroy(&H);

//    PetscFinalize();

//    return true;
//}


//PetscErrorCode SolverFmmGmres::apply_matvec(Mat mat,Vec svec,Vec y) {




//    return 0;
//}

///*
//    HessianProductMat - Computes the matrix-vector product
//    y = mat*svec.

//    Input Parameters:
// .  mat  - input matrix
// .  svec - input vector

//    Output Parameters:
// .  y    - solution vector
// */
//PetscErrorCode HessianProductMat(Mat mat,Vec svec,Vec y) {
//   void           *ptr;

//   MatShellGetContext(mat,&ptr);
////   HessianProduct(ptr,svec,y);
//   return 0;
//}



















//517: /* ------------------------------------------------------------------- */
//520: /*
//521:    Hessian Product - Computes the matrix-vector product:
//522:    y = f''(x)*svec.

//524:    Input Parameters
//525: .  ptr  - pointer to the user-defined context
//526: .  svec - input vector

//528:    Output Parameters:
//529: .  y    - product vector
//530: */
//531: PetscErrorCode HessianProduct(void *ptr,Vec svec,Vec y)
//532: {
//533:   AppCtx         *user = (AppCtx *)ptr;
//534:   PetscReal      p5 = 0.5, zero = 0.0, one = 1.0, hx, hy, val, area, *x, *s;
//535:   PetscReal      v, vb, vl, vr, vt, hxhx, hyhy;
//537:   PetscInt       nx, ny, i, j, k, ind;

//539:   nx   = user->mx;
//540:   ny   = user->my;
//541:   hx   = user->hx;
//542:   hy   = user->hy;
//543:   hxhx = one/(hx*hx);
//544:   hyhy = one/(hy*hy);

//546:   /* Get pointers to vector data */
//547:   VecGetArray(user->xvec,&x);
//548:   VecGetArray(svec,&s);

//550:   /* Initialize product vector to zero */
//551:   VecSet(y, zero);

//553:   /* Compute f''(x)*s over the lower triangular elements */
//554:   for (j=-1; j<ny; j++) {
//555:     for (i=-1; i<nx; i++) {
//556:        k = nx*j + i;
//557:        v = zero;
//558:        vr = zero;
//559:        vt = zero;
//560:        if (i != -1 && j != -1) v = s[k];
//561:        if (i != nx-1 && j != -1) {
//562:          vr = s[k+1];
//563:          ind = k+1; val = hxhx*(vr-v);
//564:          VecSetValues(y,1,&ind,&val,ADD_VALUES);
//565:        }
//566:        if (i != -1 && j != ny-1) {
//567:          vt = s[k+nx];
//568:          ind = k+nx; val = hyhy*(vt-v);
//569:          VecSetValues(y,1,&ind,&val,ADD_VALUES);
//570:        }
//571:        if (i != -1 && j != -1) {
//572:          ind = k; val = hxhx*(v-vr) + hyhy*(v-vt);
//573:          VecSetValues(y,1,&ind,&val,ADD_VALUES);
//574:        }
//575:      }
//576:    }

//578:   /* Compute f''(x)*s over the upper triangular elements */
//579:   for (j=0; j<=ny; j++) {
//580:     for (i=0; i<=nx; i++) {
//581:        k = nx*j + i;
//582:        v = zero;
//583:        vl = zero;
//584:        vb = zero;
//585:        if (i != nx && j != ny) v = s[k];
//586:        if (i != nx && j != 0) {
//587:          vb = s[k-nx];
//588:          ind = k-nx; val = hyhy*(vb-v);
//589:          VecSetValues(y,1,&ind,&val,ADD_VALUES);
//590:        }
//591:        if (i != 0 && j != ny) {
//592:          vl = s[k-1];
//593:          ind = k-1; val = hxhx*(vl-v);
//594:          VecSetValues(y,1,&ind,&val,ADD_VALUES);
//595:        }
//596:        if (i != nx && j != ny) {
//597:          ind = k; val = hxhx*(v-vl) + hyhy*(v-vb);
//598:          VecSetValues(y,1,&ind,&val,ADD_VALUES);
//599:        }
//600:     }
//601:   }



//    int argc = 0;
//    char ** argv = nullptr;

//    Vec            x,b,u;
//    Mat            A;            /* linear system matrix */
//    KSP            ksp;         /* KSP context */
//    KSP            *subksp;     /* array of local KSP contexts on this processor */
//    PC             pc;           /* PC context */
//    PC             subpc;        /* PC context for subdomain */
//    PetscReal      norm;         /* norm of solution error */
//    PetscInt       i,j,Ii,J,*blks, m = 4, n;
//    PetscMPIInt    rank,size;
//    PetscInt       its,nlocal,first,Istart,Iend;
//    PetscScalar    v,one = 1.0,none = -1.0;
//    PetscBool      isbjacobi, setFlag;

//    PetscInitialize(&argc, &argv, (char*)0, help_1);
//    //PetscOptionsGetInt(NULL, "-m", &m, );
//    PetscOptionsGetInt(NULL, NULL, "-m", &m, &setFlag);
//    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//    MPI_Comm_size(PETSC_COMM_WORLD,&size);

//    n = m + 2;

//    /* -------------------------------------------------------------------
//        Compute the matrix and right-hand-side vector that define
//        the linear system, Ax = b.
//    ------------------------------------------------------------------- */

//    /* Create and assemble parallel matrix */
//    MatCreate(PETSC_COMM_WORLD,&A);
//    MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, m*n, m*n);
//    MatSetFromOptions(A);
//    MatMPIAIJSetPreallocation(A,5,NULL,5,NULL);
//    MatSeqAIJSetPreallocation(A,5,NULL);
//    MatGetOwnershipRange(A,&Istart,&Iend);
//    for (Ii=Istart; Ii<Iend; Ii++) {
//        v = -1.0; i = Ii/n; j = Ii - i*n;
//        if (i>0)   {J = Ii - n; MatSetValues(A,1,&Ii,1,&J,&v,ADD_VALUES);}
//        if (i<m-1) {J = Ii + n; MatSetValues(A,1,&Ii,1,&J,&v,ADD_VALUES);}
//        if (j>0)   {J = Ii - 1; MatSetValues(A,1,&Ii,1,&J,&v,ADD_VALUES);}
//        if (j<n-1) {J = Ii + 1; MatSetValues(A,1,&Ii,1,&J,&v,ADD_VALUES);}
//        v = 4.0; MatSetValues(A,1,&Ii,1,&Ii,&v,ADD_VALUES);
//    }
//    MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
//    MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);


//    /* Create parallel vectors */
//    VecCreate(PETSC_COMM_WORLD,&u);
//    VecSetSizes(u,PETSC_DECIDE,m*n);
//    VecSetFromOptions(u);
//    VecDuplicate(u,&b);
//    VecDuplicate(b,&x);

//    /* Set exact solution; then compute right-hand-side vector. */
//    VecSet(u,one);
//    MatMult(A,u,b);

//    /* Create linear solver context */
//    KSPCreate(PETSC_COMM_WORLD,&ksp);

//    /* Set operators. Here the matrix that defines the linear system
//       also serves as the preconditioning matrix.  */
//    KSPSetOperators(ksp,A,A);

//    /* Set default preconditioner for this program to be block Jacobi.
//       This choice can be overridden at runtime with the option
//       -pc_type <type> */
//    KSPGetPC(ksp,&pc);
//    PCSetType(pc,PCBJACOBI);


//    /* -------------------------------------------------------------------
//                        Define the problem decomposition
//       ------------------------------------------------------------------- */

//    /* Call PCBJacobiSetTotalBlocks() to set individually the size of
//       each block in the preconditioner.  This could also be done with
//       the runtime option -pc_bjacobi_blocks <blocks>
//       Also, see the command PCBJacobiSetLocalBlocks() to set the
//       local blocks.
//       Note: The default decomposition is 1 block per processor.
//    */
//    PetscMalloc1(m,&blks);
//    for (i=0; i<m; i++) blks[i] = n;
//    PCBJacobiSetTotalBlocks(pc,m,blks);
//    PetscFree(blks);

//    /* -------------------------------------------------------------------
//               Set the linear solvers for the subblocks
//       ------------------------------------------------------------------- */

//    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//           Basic method, should be sufficient for the needs of most users.
//       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//      By default, the block Jacobi method uses the same solver on each
//      block of the problem.  To set the same solver options on all blocks,
//      use the prefix -sub before the usual PC and KSP options, e.g.,
//           -sub_pc_type <pc> -sub_ksp_type <ksp> -sub_ksp_rtol 1.e-4
//   */

//   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//         Advanced method, setting different solvers for various blocks.
//      - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//      Note that each block's KSP context is completely independent of
//      the others, and the full range of uniprocessor KSP options is
//      available for each block. The following section of code is intended
//      to be a simple illustration of setting different linear solvers for
//      the individual blocks.  These choices are obviously not recommended
//      for solving this particular problem.
//   */
//    PetscObjectTypeCompare((PetscObject)pc,PCBJACOBI,&isbjacobi);
//    if (isbjacobi) {
//    /* Call KSPSetUp() to set the block Jacobi data structures (including
//       creation of an internal KSP context for each block).

//       Note: KSPSetUp() MUST be called before PCBJacobiGetSubKSP(). */
//    KSPSetUp(ksp);

//    /* Extract the array of KSP contexts for the local blocks */
//    PCBJacobiGetSubKSP(pc,&nlocal,&first,&subksp);

//    /* Loop over the local blocks, setting various KSP options
//       for each block.  */
//    for (i=0; i<nlocal; i++) {
//        KSPGetPC(subksp[i],&subpc);
//            if (!rank) {
//                if (i%2) {
//                    PCSetType(subpc,PCILU);
//                } else {
//                    PCSetType(subpc,PCNONE);
//                    KSPSetType(subksp[i],KSPBCGS);
//                    KSPSetTolerances(subksp[i],1.e-6,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
//                }
//            } else {
//                PCSetType(subpc,PCJACOBI);
//                KSPSetType(subksp[i],KSPGMRES);
//                KSPSetTolerances(subksp[i],1.e-6,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
//            }
//        }
//    }

//    /* -------------------------------------------------------------------
//                           Solve the linear system
//       ------------------------------------------------------------------- */

//    /* Set runtime options */
//    KSPSetFromOptions(ksp);

//    /* Solve the linear system */
//    KSPSolve(ksp,b,x);

//    /* -------------------------------------------------------------------
//                           Check solution and clean up
//       ------------------------------------------------------------------- */

//    /*  Check the error */
//    VecAXPY(x,none,u);
//    VecNorm(x,NORM_2,&norm);
//    KSPGetIterationNumber(ksp,&its);
//    PetscPrintf(PETSC_COMM_WORLD,"Norm of error %g iterations %D\n",(double)norm,its);

//    /*   Free work space.  All PETSc objects should be destroyed when they
//         are no longer needed. */
//    KSPDestroy(&ksp);
//    VecDestroy(&u);  VecDestroy(&x);
//    VecDestroy(&b);  MatDestroy(&A);
//    PetscFinalize();



///---private
//    /*! pvfmm is called from here */
//    bool apply_matvec();
//    /*! rhs - right hand sided  */
//    bool calc_rhs_b();
//    /*! Will be renamed when IE solver is finished. */
//    dvec * dv1_;
//    dvec * dv2_;

///////////////////////////////////////////////////////////////////////////////

// End of the file

