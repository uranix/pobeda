# include "matvec.h"
# include "auxillary.h"
# include "fmm_summation.h"
# include "singular_integrals.h"
# include "elastic_kernels.h"

# define SRC_PER_TRG 16
# define DIM 3
# define SURF_DIM 6

typedef std::vector<double> vec;


void test_matvec(
            int N,
             vec & v,   // Input
             vec & Av,  // Output

             // Additional parameters

             // Triangles
             vec & vert_1,
             vec & vert_2,
             vec & vert_3,

             // Boundary conditions
             vec & bc_t,

             // FMM args
             int mult_order,
             MPI_Comm comm
        )
{
/*
    //-------1) Matvec with UU matrix - works--------------

    int trg_cnt = bc_t.size();
    int src_cnt = trg_cnt;
    src_cnt *=SRC_PER_TRG; // 16 sources per triangle
    vec src_coord(DIM*src_cnt);
    vec src_value(DIM*src_cnt);
    vec surf_coord(0);
    vec surf_value(0);
    vec trg_coord(DIM * trg_cnt);
    vec trg_value(DIM * trg_cnt);
    for (int i = 0; i < DIM*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int bc_size = DIM*SRC_PER_TRG;
    int bv_size = SURF_DIM*SRC_PER_TRG;
    vec sbuff_coord(bc_size);
    vec sbuff_value(bc_size);
    vec v1(DIM), v2(DIM), v3(DIM), val(DIM);
    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }
        triangle2src(1, v1,v2,v3,sbuff_coord,sbuff_value, val);
        for (int m = 0; m < bc_size;m++)  src_coord[bc_size*i + m] = sbuff_coord[m];
        for (int m = 0; m < bc_size;m++)  src_value[bc_size*i + m] = sbuff_value[m];
    }
    //==============================================================================
    */


    //-------1) Matvec with PP matrix - works--------------

    int trg_cnt = bc_t.size();
    int surf_cnt = trg_cnt;
    surf_cnt *=SRC_PER_TRG; // 16 sources per triangle
    vec surf_coord(DIM*surf_cnt);
    vec surf_value(SURF_DIM*surf_cnt);
    vec src_coord(0);
    vec src_value(0);
    vec trg_coord(DIM * trg_cnt);
    vec trg_value(DIM * trg_cnt);
    for (int i = 0; i < DIM*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int bc_size = DIM*SRC_PER_TRG;
    int bv_size = SURF_DIM*SRC_PER_TRG;
    vec sbuff_coord(bc_size);
    vec sbuff_value(bv_size);
    vec v1(DIM), v2(DIM), v3(DIM), val(DIM);
    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }
        triangle2src(2, v1,v2,v3,sbuff_coord,sbuff_value, val);
        for (int m = 0; m < bc_size;m++)  surf_coord[bc_size*i + m] = sbuff_coord[m];
        for (int m = 0; m < bv_size;m++)  surf_value[bv_size*i + m] = sbuff_value[m];
    }
    //==============================================================================

//    std::cout<<"TEST MATVEC SURF COORD:"<<std::endl;
//    for (int i = 0; i<surf_coord.size(); i++) std::cout<<surf_coord[i]<<std::endl;

//    std::cout<<"TEST MATVEC SURF VALUE:"<<std::endl;
//    for (int i = 0; i<surf_value.size(); i++) std::cout<<surf_value[i]<<std::endl;



    direct_summation_displacement(src_coord,
                                  src_value,
                                  surf_coord,
                                  surf_value,
                                  trg_coord,
                                  trg_value);

    for (int i = 0; i < DIM * trg_cnt; i++) Av[i] = trg_value[i];


    /*
    //=========================================================
    // 2) LOCAL pass (UU matrix) works!
    //=========================================================
    // Subtract-add cycle
    vec add(DIM); vec subtr(DIM);
    for (int i = 0; i < trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }
        local_pass( subtr, add, v1, v2, v3, 1, val);
        for (int k = 0; k < DIM; k++)
        { Av[DIM*i+k] -= subtr[k]; Av[DIM*i+k] += add[k];}
    }
    */

    //=========================================================
    // 2) LOCAL pass (PP matrix)
    //=========================================================
    // Subtract-add cycle
    vec add(DIM); vec subtr(DIM);
    for (int i = 0; i < trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }
        local_pass( subtr, add, v1, v2, v3, 2, val);
        for (int k = 0; k < DIM; k++)
        { Av[DIM*i+k] -= subtr[k]; Av[DIM*i+k] += add[k];}
    }


}

/*! Proper "matvec" to be used in the solver. */
void matvec(
             vec & v,   // Input
             vec & Av,  // Output

             // Additional parameters

             // Triangles
             vec & vert_1,
             vec & vert_2,
             vec & vert_3,

             // Boundary conditions
             vec & bc_t,

             // FMM args
             int mult_order,
             MPI_Comm comm
        )
{
    // This function performs multiplication A*v for a given v

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    // for (int i = 0; i<bc_t.size(); i++) bc_t[i] = 2; // Only PP matrix

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt ++;
        if (bc_t[i]==2) src_cnt ++;
    }
    src_cnt *=SRC_PER_TRG; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG;
    // !!!  Bellow 6 vectors are of very long size ~ 10^6. Should be optimized later  !!!
    vec src_coord(DIM*src_cnt);
    vec src_value(DIM*src_cnt);
    vec surf_coord(DIM*surf_cnt);
    vec surf_value(SURF_DIM*surf_cnt);
    vec trg_coord(DIM * trg_cnt);
    vec trg_value(DIM * trg_cnt);

    // Setting up FMM arrays

    for (int i = 0; i < DIM*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM*SRC_PER_TRG;
    int bv_size = SURF_DIM*SRC_PER_TRG;
    vec sbuff_coord(bc_size);
    vec sbuff_src_v(bc_size);
    vec sbuff_surf_v(bv_size);

    vec v1(DIM), v2(DIM), v3(DIM), val(DIM);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }

        if (bc_t[i]==1)
        {
            triangle2src(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            triangle2src(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }
/*
    direct_summation_displacement(src_coord,
                                  src_value,
                                  surf_coord,
                                  surf_value,
                                  trg_coord,
                                  trg_value);
*/

    // Handmade PVFMM is called here. Is declared in fmm_summation where genuine pvfmm is called.
    calculate_displacements(
            src_coord,
            src_value,
            surf_coord,
            surf_value,
            trg_coord,
            trg_value,
            mult_order,
            comm);


    for (int i = 0; i < DIM * trg_cnt; i++) Av[i] = trg_value[i];

    //=========================================================
    // 2) LOCAL pass
    //=========================================================

    // a) add (1/2)*delta_ij * u_j = 1/2 * u_i

    for (int i = 0; i < trg_cnt; i++)
    { if (bc_t[i]==1) {for (int k = 0; k<DIM; k++) Av[DIM * i + k] += 0.5 * v[DIM * i + k];}}

    // b) Subtract-add cycle
    vec add(DIM); vec subtr(DIM);
    for (int i = 0; i < trg_cnt; i++)
    {

        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }
        local_pass( subtr, add, v1, v2, v3, 3-bc_t[i], val);
        for (int k = 0; k < DIM; k++)
        {
            if (bc_t[i]==2)
            { subtr[k] = - subtr[k]; add[k] = - add[k]; } // U matr comes with minus sign
            Av[DIM*i+k] -= subtr[k]; Av[DIM*i+k] += add[k];
        }
    }
    for (int i = 0; i<Av.size(); i++){Av[i] = -Av[i];}
}



















void rhs(
             vec & v,   // Input
             vec & Av,  // Output

             // Additional parameters

             // Triangles
             vec & vert_1,
             vec & vert_2,
             vec & vert_3,

             // Boundary conditions
             vec & bc_te,

             // FMM args
             int mult_order,
             MPI_Comm comm
        )
{
    // This function performs multiplication BB*bcs and generates rhs

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    vec bc_t(bc_te.size());
    for (int i = 0; i<bc_t.size(); i++) bc_t[i] = 3 - bc_te[i]; // Only PP matrix

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt ++;
        if (bc_t[i]==2) src_cnt ++;
    }
    src_cnt *=SRC_PER_TRG; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG;
    vec src_coord(DIM*src_cnt);
    vec src_value(DIM*src_cnt);
    vec surf_coord(DIM*surf_cnt);
    vec surf_value(SURF_DIM*surf_cnt);
    vec trg_coord(DIM * trg_cnt);
    vec trg_value(DIM * trg_cnt);

    // Setting up FMM arrays

    for (int i = 0; i < DIM*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM*SRC_PER_TRG;
    int bv_size = SURF_DIM*SRC_PER_TRG;
    vec sbuff_coord(bc_size);
    vec sbuff_src_v(bc_size);
    vec sbuff_surf_v(bv_size);

    vec v1(DIM), v2(DIM), v3(DIM), val(DIM);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }

        if (bc_t[i]==1)
        {
            triangle2src(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            triangle2src(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }

    /*
    direct_summation_displacement(src_coord,
                                  src_value,
                                  surf_coord,
                                  surf_value,
                                  trg_coord,
                                  trg_value);
*/
    calculate_displacements(
            src_coord,
            src_value,
            surf_coord,
            surf_value,
            trg_coord,
            trg_value,
            mult_order,
            comm);


    for (int i = 0; i < DIM * trg_cnt; i++) Av[i] = trg_value[i];

    //=========================================================
    // 2) LOCAL pass
    //=========================================================

    // a) add (1/2)*delta_ij * u_j = 1/2 * u_i

    for (int i = 0; i < trg_cnt; i++)
    { if (bc_t[i]==1) {for (int k = 0; k<DIM; k++) Av[DIM * i + k] += 0.5 * v[DIM * i + k];}}

    // b) Subtract-add cycle
    vec add(DIM); vec subtr(DIM);
    for (int i = 0; i < trg_cnt; i++)
    {

        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }
        local_pass( subtr, add, v1, v2, v3, 3-bc_t[i], val);
        for (int k = 0; k < DIM; k++)
        {
            if (bc_t[i]==2)
            { subtr[k] = - subtr[k]; add[k] = - add[k]; } // U matr comes with minus sign
            Av[DIM*i+k] -= subtr[k]; Av[DIM*i+k] += add[k];
        }
    }
    // for (int i = 0; i<Av.size(); i++){Av[i] = -Av[i];}
}

















void local_pass_u( vec & sub, vec & add,
        vec & v_1, vec & v_2, vec & v_3,
        vec & val)
{

    double G = 1., nu = 0.3; // Kto by somnevalsya :)
    vec csi(DIM); for (int i = 0; i<DIM; i++) csi[i] = (1./3.)*(v_1[i] + v_2[i] + v_3[i]);
    //============================================
    //             Add value
    //============================================
    for (int i = 0; i<DIM; i++) add[i] = 0;
    for (int i = 0; i<DIM; i++)
    {
        for (int j = 0; j<DIM; j++)
        {
            add[i] += sint_u(i+1, j+1, v_1, v_2, v_3, csi, G, nu)* val[j];
        }
    }
    //for (int i = 0; i<3; i++) {add[i] = 0;} //Turn of add

    //=========================================
    //             Sub value
    //=========================================
    for (int i = 0; i<DIM; i++) sub[i] = 0;

    int bc_size = DIM * SRC_PER_TRG; // 3 * (sources_per_triangle)

    vec src_coord(bc_size); vec src_value(bc_size);
    vec x(DIM), d(DIM), n(DIM);

    triangle2src(1, v_1, v_2, v_3, src_coord, src_value, val);

    // Direct summation over src and surf
    for (int s=0; s< src_coord.size()/3; s++)
    {
        x[0] = src_coord[s*DIM];
        x[1] = src_coord[s*DIM+1];
        x[2] = src_coord[s*DIM+2];

        d[0] = src_value[s*DIM];
        d[1] = src_value[s*DIM+1];
        d[2] = src_value[s*DIM+2];

        for (int i = 0; i<DIM; i++){
            for (int j = 0; j<DIM; j++){
                sub[i] += U_ij( i+1, j+1, csi, x, G, nu ) * d[j];
            }
        }
    }
    //for (int i = 0; i<3; i++) {sub[i] = 0;}
}




// This function subtracts inaccurate contributions
// from a current triangle, and then replaces them with
// exact analytical singular integral

void local_pass(

        vec & sub,
        vec & add,

        vec & v_1,
        vec & v_2,
        vec & v_3,

        int bc,
        vec & val)
{

    double G = 1., nu = 0.3; // Kto by somnevalsya :)


    vec csi(DIM); for (int i = 0; i<DIM; i++) csi[i] = (1./3.)*(v_1[i] + v_2[i] + v_3[i]);
    //============================================
    //             Add value
    //============================================

    // (invert bc when using for RHS correction)
    for (int i = 0; i<DIM; i++) add[i] = 0;
    if (bc==1)
    {
        for (int i = 0; i<DIM; i++)
        {
            for (int j = 0; j<DIM; j++)
            {
                add[i] += sint_u(i+1, j+1, v_1, v_2, v_3, csi, G, nu)* val[j];
            }
        }
    }
    if (bc==2)
    {
        for (int i = 0; i<DIM; i++)
        {
            for (int j = 0; j<DIM; j++)
            {
                add[i] += sint_p(i+1, j+1, v_1, v_2, v_3, csi, nu)* val[j];
            }
        }
    }

    //for (int i = 0; i<3; i++) {add[i] = 0;}

    //=========================================
    //             Sub value
    //=========================================

    for (int i = 0; i<DIM; i++) sub[i] = 0;

    int bc_size = DIM * SRC_PER_TRG; // 3 * (sources_per_triangle)
    int bv_size = SURF_DIM * SRC_PER_TRG; // 6 * (sources_per_triangle)

    vec src_coord(bc_size);
    vec src_value(bc_size);

    vec surf_coord(bc_size);
    vec surf_value(bv_size);

    vec x(DIM), d(DIM), n(DIM);
    if (bc==1)
    {
        triangle2src(1, v_1, v_2, v_3, src_coord, src_value, val);

        // Direct summation over src and surf
        for (int s=0; s< src_coord.size()/3; s++)
        {
            x[0] = src_coord[s*DIM];
            x[1] = src_coord[s*DIM+1];
            x[2] = src_coord[s*DIM+2];

            d[0] = src_value[s*DIM];
            d[1] = src_value[s*DIM+1];
            d[2] = src_value[s*DIM+2];

            for (int i = 0; i<DIM; i++){
                for (int j = 0; j<DIM; j++){
                    sub[i] += U_ij( i+1, j+1, csi, x, G, nu ) * d[j];
                }
            }
        }
    }

    if (bc==2)
    {
        triangle2src(2, v_1, v_2, v_3, surf_coord, surf_value, val);

        // Direct summation over src and surf
        for (int s=0; s< src_coord.size()/3; s++)
        {
            x[0] = surf_coord[s*DIM];
            x[1] = surf_coord[s*DIM+1];
            x[2] = surf_coord[s*DIM+2];

            d[0] = surf_value[s*SURF_DIM];
            d[1] = surf_value[s*SURF_DIM+1];
            d[2] = surf_value[s*SURF_DIM+2];

            n[0] = surf_value[s*SURF_DIM+3];
            n[1] = surf_value[s*SURF_DIM+4];
            n[2] = surf_value[s*SURF_DIM+5];

            for (int i = 0; i<DIM; i++){
                for (int j = 0; j<DIM; j++){
                    sub[i] += P_ij( i+1, j+1, csi, x, n, nu ) * d[j];
                }
            }
        }
    }
    //for (int i = 0; i<3; i++) {sub[i] = 0;}
}



// This function generates the set of sources on the triangle
//         src_coord - contains coordinates of 16 source points
//                     (len(src_coords) = 48)
//         src_value - contains quadrature weights
//                     NOT multiplied by the solution
//         AND normals (len(src_value) = 96)


void triangle2src(
        int mode, // 1 - generate src, 2 - generate surf
        vec & v_1,
        vec & v_2,
        vec & v_3,

        vec & src_coord,
        vec & src_value,

        vec & val
        )
{
    // Normal components by
    // Cubic Gauss quadrature points and weights
    vec eta_1(4); eta_1 = { 1./3., 3./5., 1./5., 1./5. };
    vec eta_2(4); eta_2 = { 1./3., 1./5., 3./5., 1./5. };
    vec eta_3(4); for (int i = 0; i < 4; i++)
    {  eta_3[i] = 1. - eta_1[i] - eta_2[i]; }
    vec ww_i(4);  ww_i =  {-9.0/32.0, 25.0/96.0, 25.0/96.0, 25.0/96.0};
    // Split triangle into 4 pieces
    vec v_12(DIM); for (int i = 0; i < DIM; i++) v_12[i] = (1./2.) * (v_1[i] + v_2[i]);
    vec v_13(DIM); for (int i = 0; i < DIM; i++) v_13[i] = (1./2.) * (v_1[i] + v_3[i]);
    vec v_23(DIM); for (int i = 0; i < DIM; i++) v_23[i] = (1./2.) * (v_2[i] + v_3[i]);

    int i = 0; // Point index
    vec x_1(DIM); vec x_2(DIM); vec x_3(DIM);
    for (int t = 0; t<4; t++ ) // cycle over triangles
    {
        if (t==0) for (int v = 0; v < DIM; v++) { x_1[v] = v_1[v];  x_2[v] = v_12[v]; x_3[v] = v_13[v];}
        if (t==1) for (int v = 0; v < DIM; v++) { x_1[v] = v_2[v];  x_2[v] = v_23[v]; x_3[v] = v_12[v];}
        if (t==2) for (int v = 0; v < DIM; v++) { x_1[v] = v_3[v];  x_2[v] = v_13[v]; x_3[v] = v_23[v];}
        if (t==3) for (int v = 0; v < DIM; v++) { x_1[v] = v_12[v]; x_2[v] = v_23[v]; x_3[v] = v_13[v];}

        for (int c = 0; c<4; c++ ) // cycle over gauss points
        {
            for (int m = 0; m<DIM; m++)
            {
                if (mode == 1){
                    src_coord[DIM*i + m] = x_1[m] * eta_1[c] + x_2[m] * eta_2[c] + x_3[m] * eta_3[c];
                    src_value[DIM*i + m] = ww_i[c] * 2.0 * area(x_1, x_2, x_3) * val[m];
                }

                if (mode == 2){
                    src_coord[DIM*i + m] = x_1[m] * eta_1[c] + x_2[m] * eta_2[c] + x_3[m] * eta_3[c];
                    src_value[SURF_DIM*i + m] = ww_i[c] * 2.0 * area(x_1, x_2, x_3) * val[m];
                    src_value[SURF_DIM*i + DIM + m] = norm(v_1, v_2, v_3)[m];
                }
            }
            i++;
        }
    }
}

void test_integral()
{

    vec x1(3); x1 = {0.0, 0.0, 0.0};
    vec x2(3); x2 = {1.0, 0.0, 0.0};
    vec x3(3); x3 = {0.0, 1.0, 0.0};
    vec cs(3); cs = {1.0, 1.0, 0.0};
    vec val(3); val = {1.,1.,1.};
    vec src_coord(48), src_value(48);
    double Integral = 0;
    triangle2src(1, x1,x2,x3, src_coord, src_value, val);
    vec x(3), d(3);
    for (int s=0; s<src_coord.size()/3;s++)
    {
        for (int i=0; i<3;i++){
            x[i] = src_coord[s*3+i];
            d[i] = src_value[s*3+i];

        }

        std::cout<<"x = "<<x[0]<<" "<<x[1]<<" "<<x[2]<<std::endl;
        std::cout<<"d = "<<d[0]<<" "<<d[1]<<" "<<d[2]<<std::endl;


        for (int j=0; j<3;j++)
        {
              Integral += U_ij( 1, j+1, cs, x, 1.0, 0.3) * d[j];
              std::cout<<"U_ij"<<U_ij( 1, j+1, cs, x, 1.0, 0.3)<<std::endl;
        }
    }

    std::cout<<" Test integral U ij = "<<Integral<<std::endl;
}


void test_integral2()
{

    vec x1(3); x1 = {0.5, 1.0, 0.5};
    vec x2(3); x2 = {0.0, 1.0, 1.0};
    vec x3(3); x3 = {1.0, 1.0, 1.0};
    vec cs(3); cs = {0.5, 1.0, 0.833333};
    vec val(3); val = {0.,1.,0.};
    vec src_coord(48), src_value(96);
    double Integral = 0;
    triangle2src(2, x1,x2,x3, src_coord, src_value, val);
    vec x(3), d(3), n(3);
    for (int s=0; s<src_coord.size()/3;s++)
    {
        for (int i=0; i<3;i++){
            x[i] = src_coord[s*3+i];
            d[i] = src_value[s*6+i];
            n[i] = src_value[s*6+3+i];

        }
        for (int j=0; j<3;j++)
        {
              Integral += P_ij( 1, j+1, cs, x, n, 0.3) * d[j];
        }
    }


    std::cout<<" Test integral P ij = "<<Integral<<std::endl;
}
