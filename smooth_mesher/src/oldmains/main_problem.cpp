// POBEDA - Parallel Optimization with
// Boundary Elements and Domain Adaptivity

//----------------------------
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <ctime>
#include <pvfmm.hpp>
#include <utils.hpp>
#include "bvp.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"
#include "fmm_summation.h"
#include "matvec.h"
#include "solver_ksp.h"
#include "topo_deriv.h"
#include "mesher.h"
#include "auxillary.h"
#include "stl_io.h"
#include <string>


//---THESE TYPES ARE USED THROUGHOUT THE CODE--
typedef std::vector<double> dvec;
typedef std::vector<float> fvec;
typedef std::vector<int> ivec;
//=============================================


//----------GLOBAL VARIABLES-----------------------------
int N_iter; // global number of GMRES iterations
int N_cycle; // global number of the optimization cycle
int smooth_iters; // global number of geometry correction iterations
double YOUNG;     // Young's modulus of the material
double POISS;     // Poisson's ratio of the material
//=======================================================

int main(int argc, char **argv){

    YOUNG = 1.;
    POISS = 0.3;

    smooth_iters = 5;
    int mm = 2; // 8 vox
    for (int i = 0; i<8;i++)
        std::cout<<mnk(i, mm,mm,mm)[0]<<" "<<mnk(i, mm,mm,mm)[1]<<" "<<mnk(i, mm,mm,mm)[2];



  N_cycle = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm comm=MPI_COMM_WORLD;

  int n_proc; // Number of MPI threads
  int i_proc; // index of this MPI thread
  MPI_Comm_size(comm, &n_proc);
  MPI_Comm_rank(comm, &i_proc);

  //--------Read command line options-----------------------------------------------------------------------------------------------------------------------
  commandline_option_start(argc, argv, "\
    This example demonstrates solving a particle N-body problem,\n\
    with Laplace Gradient kernel, using the PvFMM library.\n");

  commandline_option_start(argc, argv);
  omp_set_num_threads( atoi(commandline_option(argc, argv,  "-omp",     "1", false, "-omp  <int> = (1)    : Number of OpenMP threads."          )));
  size_t   N=(size_t)strtod(commandline_option(argc, argv,    "-N",     "1",  true, "-N    <int>          : Number of source and target points."),NULL);
  int      m =      strtoul(commandline_option(argc, argv,    "-m",    "10", false, "-m    <int> = (10)   : Multipole order (+ve even integer)."),NULL,10);
  commandline_option_end(argc, argv);
  pvfmm::Profile::Enable(false);
  //========================================================================================================================================================

  // Start time measurement
  double t1, t2;
  t1 = MPI_Wtime();

  // Surface solution
  // Set up test optimization problem

  //=========
  double c = 1. / ((double) N);
  ivec side_BC(6);
  for (int i = 0; i<6; i++) side_BC[i] = 1;
  side_BC[3] = 2;
  dvec force_on_voxel(N*N*N*6*3);
  for (int i = 0; i < force_on_voxel.size(); i++)
  {force_on_voxel[i] = 0;}
  // apply some force on a single voxel

  for (int n = 0; n<N; n++)
  force_on_voxel[iiiii(N-1,n,N-1,4,2,N,N,N,6,3)] = -1.0;

  dvec vert_1(0);
  dvec vert_2(0);
  dvec vert_3(0);
  ivec bc_type(0);
  dvec tr_di(0);


  int MAX_CYCLES = 10; // if we did not reach our goal in [MAX_CYCLES] iteratios - we are done
  double ratio = 0.5; // if ratio less than [ratio] - we are done
  double level = 0.03; // We cut voxels with TD_min + (TD_max - TD_min) * [level] %
  double dlevel = 0.01;
      // Status - at the beginning all the voxels are present
      ivec status(N*N*N);
      for (int i = 0; i < status.size(); i++)
      {status[i] = 1;}

      //-----BEGINNING OF THE OPTIMIZATION CYCLE
      for ( int opt_cycle = 0; opt_cycle < MAX_CYCLES; opt_cycle++) {
          N_iter = 0; // GMRES iteration counter
          if (!i_proc) std::cout<<std::endl;
          if (!i_proc) std::cout<<std::endl;
          if (!i_proc) std::cout<<"========="<<std::endl;
          if (!i_proc) std::cout<<"========="<<std::endl;
          if (!i_proc) std::cout<<" CYCLE # "<<opt_cycle<<std::endl;
          if (!i_proc) std::cout<<"========="<<std::endl;
          if (!i_proc) std::cout<<"========="<<std::endl;
          if (!i_proc) std::cout<<std::endl;
          if (!i_proc) std::cout<<std::endl;


      // Generate the first iteration BVP
      generate_BVP( N,N,N,c,side_BC,force_on_voxel,status,
                    vert_1,vert_2,vert_3,tr_di,bc_type,comm);

      // Write2file
      if (!i_proc){
        write_stl1("iter_100_m5_" + std::to_string(opt_cycle), vert_1, vert_2, vert_3);
      }

      // Manual scatter
      int N_trg = vert_1.size()/3.; // note - N_trg is global for all proc
      int start, end, span, step;
      if (n_proc == 1) step = N_trg;
      if (n_proc > 1) step = (int) std::floor(N_trg / (double) (n_proc));
      start = step * i_proc;
      end   = step *(i_proc+1);
      if (i_proc == n_proc-1) end = N_trg;
      span = end - start;
      for (int i = 0; i<span; i++)
      {
          // single component vectors
          bc_type[i] = bc_type[start+i];
          // tree component vectors
          for (int k=0;k<3;k++)
          {
              vert_1[3*i+k] = vert_1[3*start+3*i+k];
              vert_2[3*i+k] = vert_2[3*start+3*i+k];
              vert_3[3*i+k] = vert_3[3*start+3*i+k];
              tr_di[3*i+k] = tr_di[3*start+3*i+k];
          }
      }
      bc_type.resize(span);
      vert_1.resize(3*span);
      vert_2.resize(3*span);
      vert_3.resize(3*span);
      tr_di.resize(3*span);

      // Cash vector with knowns
      dvec buff(tr_di.size());
      for (int i = 0; i<tr_di.size(); i++)
          buff[i] = tr_di[i];

      //---SOLVE THE SURFACE PROBLEM WITH PETSC GMRES SOLVER------
      SolverFmmGmres * slvr = new SolverFmmGmres();
      slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di);
      slvr->solve(m, comm, 500, 5e-3);
      delete slvr; slvr = nullptr;
      //==========================================================

      // Sort the obtained solution onto tractions and displacements
      dvec tractions(buff.size()), displacements(buff.size());

      for (int i = 0; i<bc_type.size();i++)
      {
          if (bc_type[i] == 1)
          {
              for (int k = 0; k<3; k++)
              {    tractions[3*i+k] =  buff[3*i+k];
               displacements[3*i+k] = tr_di[3*i+k];}
          }
          if (bc_type[i] == 2)
          {
              for (int k = 0; k<3; k++)
              {    tractions[3*i+k] = tr_di[3*i+k];
               displacements[3*i+k] =  buff[3*i+k];}
          }
      }



      // Volume matvec and topological derivatives
      int N_vol = N*N*N;
      dvec coordinates(N_vol*3);

      int d = 0;
      for (int m = 0; m<N; m++){
          for (int n = 0; n<N; n++){
              for (int k = 0; k<N; k++){
                  coordinates[3*d+0] = 1.0/N*m+1.0/(2.0*N);
                  coordinates[3*d+1] = 1.0/N*n+1.0/(2.0*N);
                  coordinates[3*d+2] = 1.0/N*k+1.0/(2.0*N);
                  d++;
              }
          }
      }

      // A) For now all the targets live in main process
      //if (i_proc) coordinates.resize(0);


      // B) Scatter targets over processes
      if (n_proc == 1) step = N_vol;
      if (n_proc > 1) step = (int) std::floor(N_vol / (double) (n_proc));
      start = step * i_proc;
      end   = step *(i_proc+1);
      if (i_proc == n_proc-1) end = N_vol;
      span = end - start;
      for (int i = 0; i<span; i++)
      {
          for (int k=0;k<3;k++)
          {coordinates[3*i+k] = coordinates[3*start+3*i+k];}
      }
      coordinates.resize(3*span);


      //solve for stresses and TDs
      dvec topo_derivs(coordinates.size()/3);
      dvec stresses(coordinates.size()*3);

      // Solve for stresses
      volume_matvec(vert_1,vert_2,vert_3,
                         coordinates,tractions,
                         displacements,stresses, m, comm);

      // Solve for TDs
      topo_deriv( stresses, topo_derivs, YOUNG, POISS);



      // Gather all topological derivatives to root process
      int sendcount = topo_derivs.size();
      double sendarray[sendcount];
      for (int i = 0; i<sendcount; i++) {sendarray[i] = topo_derivs[i];}
      double rbuf[N_vol];
      int recvcounts[n_proc];
      int displs[n_proc];
      for (int i = 0; i<n_proc; i++)
      {
          int start, end, span, step;
          if (n_proc==1) step = N_vol;
          if (n_proc>1) step = (int) std::floor(N_vol / (double) (n_proc));
          start = step * i;
          end   = step *(i+1);
          if (i == n_proc-1) end = N_vol;
          recvcounts[i] = end - start;
          if (i == 0) displs[i] = 0;
          if (i > 0) displs[i] = displs[i-1] + recvcounts[i-1];
      }
      MPI_Gatherv(sendarray, sendcount, MPI_DOUBLE,rbuf, recvcounts, displs,MPI_DOUBLE, 0, comm);



      if (i_proc) topo_derivs.resize(0);
      if (!i_proc)
      {
          std::cout<<"buffer after gather"<<std::endl;
          for (int i = 0; i<N_vol; i++)
            std::cout<<"i_proc = "<<i_proc<<", rbuf["<<i<<"] = "<<rbuf[i]<<std::endl;


          topo_derivs.resize(N_vol);
          for (int i = 0; i<N_vol; i++) topo_derivs[i] = rbuf[i];

          std::cout<<"TDs after gather"<<std::endl;
          for (int i = 0; i<topo_derivs.size(); i++)
            std::cout<<"i_proc = "<<i_proc<<", topo_derivs["<<i<<"] = "<<topo_derivs[i]<<std::endl;

      }



      // Determine span of TDs and absolute cutoff level
      double TD_max = -10e10;
      double TD_min = 10e10;
      for (int i = 0; i<topo_derivs.size(); i++)
      {
          if (topo_derivs[i]>TD_max) TD_max = topo_derivs[i];
          if (topo_derivs[i]<TD_min) TD_min = topo_derivs[i];
      }
      double abs_level = TD_min + (TD_max - TD_min) * level;

      if (!i_proc){
      std::cout<<"TD_min = "<<TD_min<<std::endl;
      std::cout<<"TD_max = "<<TD_max<<std::endl;
      std::cout<<"abs_level = "<<abs_level<<std::endl;
      }
      // Mask previously removed voxels in a simple way
      for (int i = 0; i<topo_derivs.size(); i++)
      { if (status[i] == 0) topo_derivs[i] = 0.0;}

      // Cutting voxels
      for (int i = 0; i<topo_derivs.size(); i++)
      {if (topo_derivs[i]<abs_level) status[i] = 0;}


      //Cutting isolated voxels
      int removed = 0;
      for (int si = 0; si<smooth_iters; si++)
      {
          for (int i = 0; i<status.size();i++)
          {
              if (status[i]==1)
              {
                  bool rem = false;
                  int neighbors = 0;
                  ivec mnks = mnk(i, N,N,N);
                  if ((mnks[0] < N-1)&&(status[iii(mnks[0]+1,mnks[1],mnks[2],N,N,N)]==1)) neighbors++;
                  if ((mnks[0] > 0)&&(status[iii(mnks[0]-1,mnks[1],mnks[2],N,N,N)]==1)) neighbors++;
                  if ((mnks[1] < N-1)&&(status[iii(mnks[0],mnks[1]+1,mnks[2],N,N,N)]==1)) neighbors++;
                  if ((mnks[1] > 0)&&(status[iii(mnks[0],mnks[1]-1,mnks[2],N,N,N)]==1)) neighbors++;
                  if ((mnks[2] < N-1)&&(status[iii(mnks[0],mnks[1],mnks[2]+1,N,N,N)]==1)) neighbors++;
                  if ((mnks[2] > 0)&&(status[iii(mnks[0],mnks[1],mnks[2]-1,N,N,N)]==1)) neighbors++;
                  if (neighbors<2) { status[i] = 0; removed ++; rem = true;}

                  // Remove "frame" voxels
                  if ((mnks[0] == N-1)&&(status[iii(mnks[0]-1,mnks[1],mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                  if ((mnks[0] == 0)&&(status[iii(mnks[0]+1,mnks[1],mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                  if ((mnks[1] == N-1)&&(status[iii(mnks[0],mnks[1]-1,mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                  if ((mnks[1] == 0)&&(status[iii(mnks[0],mnks[1]+1,mnks[2],N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                  if ((mnks[2] == N-1)&&(status[iii(mnks[0],mnks[1],mnks[2]-1,N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}
                  if ((mnks[2] == 0)&&(status[iii(mnks[0],mnks[1],mnks[2]+1,N,N,N)]==0)&&(!rem)) { status[i] = 0; removed ++;}

              }

          }
      }
      if ((!i_proc)&&(removed > 0)) std::cout<<"Removed "<<removed<<"invalid voxels";


      //=========================================================


    level +=dlevel;
  } // END OF OPTIMIZATION CYCLE

  t2 = MPI_Wtime();
  printf( "MPI time  %f\n", t2 - t1 );


  // Shut down MPI
  MPI_Finalize();


  return 0;

}





// Simple mesh
/*


      // Gather TDs - we need to find largest and smallest one
      // MPI_Gatherv(sbuf, scount, MPI_DOUBLE, rbuf, const int *recvcounts, const int *displs,
      //                MPI_Datatype recvtype, int root, MPI_Comm comm);





//      int root=0;
//      double *rbuf; rbuf = (double *) malloc( N*N*N * sizeof(double));
//      double *tbuf = (double *) malloc( topo_derivs.size() * sizeof(double));

//      double * arr1 = new double[razmer :)]
//      memcpy(see cplusplus.com)
//      double * const arr1 = vector.data() - returns pointer to double


//      delete[] arr1;

    //  for (int i = 0; i<topo_derivs.size(); i++) tbuf

      //MPI_Gather( tbuf, topo_derivs.size(), MPI_DOUBLE, rbuf, topo_derivs.size(), MPI_DOUBLE, root, comm);




      // Manual scatter of volume coords
      int N_vol = coordinates.size()/3.;
      if (n_proc = 1) step = N_vol;
      if (n_proc > 1) step = (int) std::floor(N_vol / (double) (n_proc-1.));
      start = step * i_proc;
      end   = step *(i_proc+1);
      if (i_proc == n_proc-1) end = N_trg;
      span = end - start;
      for (int i = 0; i<span; i++)
      {for (int k=0;k<3;k++)coordinates[3*i+k] = coordinates[3*start+3*i+k];}
      coordinates.resize(3*span);










// Form the array of tears (used by MPI_gatherv)
      double *tears = new double[n_proc];
      for (int i = 0; i<n_proc; i++)
      {
          start = step * i;
          end   = step *(i+1);
          if (i == n_proc-1) end = N_trg;
          tears[i] = end - start;
      }
      std::cout<<"MPI tears array:"<<std::endl;
      for (int i = 0; i<n_proc; i++) std::cout<<"tear["<<i<<"]="<<tears[i]<<std::endl;


//      if (!i_proc) std::cout<<"TOPOLOGICAL DERIVATIVES"<<std::endl;
//      for (int i = 0; i<topo_derivs.size(); i++) std::cout<<topo_derivs[i]<<std::endl;


//      cout.precision(2);

      // Table of stresses
//      if (!i_proc) std::cout<<"             TABLE OF STRESSES               "<<std::endl;
//      if (!i_proc) std::cout<<"_______________________________________________________________________________________________________________________________________"<<std::endl;
//      if (!i_proc) std::cout<<"s11\t\t"<<"s12\t\t"<<"s13\t\t"<<"s21\t\t"<<"s22\t\t"<<"s23\t\t"<<"s31\t\t"<<"s32\t\t"<<"s33\t\t"<<std::endl;
//      if (!i_proc) std::cout<<"_______________________________________________________________________________________________________________________________________"<<std::endl;

//      for (int i = 0; i<stresses.size()/9; i++)
//      {
//          if (!i_proc) std::cout<<stresses[9*i+0]<<"\t\t"<<stresses[9*i+1]<<"\t\t"<<stresses[9*i+2]<<"\t\t"<<stresses[9*i+3]<<"\t\t"<<stresses[9*i+4]<<"\t\t"<<stresses[9*i+5]<<"\t\t"<<stresses[9*i+6]<<"\t\t"<<stresses[9*i+7]<<"\t\t"<<stresses[9*i+8]<<"\t\t"<<std::endl;
//      }



  // --------Set up serial mesh------------------------------------
  int N_trg = N * N * 24;
  dvec vert_1( N_trg * 3);
  dvec vert_2( N_trg * 3);
  dvec vert_3( N_trg * 3);
  dvec tr_di(N_trg * 3);
  ivec bc_type( N_trg );
  set_surf_bvp(N, vert_1, vert_2, vert_3, bc_type, tr_di, comm);
  //===============================================================


  //---------Distrubute mesh over processes---------------------
  int n_proc; // Number of MPI threads
  int i_proc; // index of this MPI thread
  MPI_Comm_size(comm, &n_proc);
  MPI_Comm_rank(comm, &i_proc);

  dvec test(N_trg); for (int i = 0; i<N_trg; i++) test[i] = i;
  dvec test3(3*N_trg); for (int i = 0; i<N_trg; i++) { test3[3*i] = i; test3[3*i+1] = i; test3[3*i+2] = i;}

  int start, end, span;
  int step = (int) std::floor(N_trg / (double) (n_proc-1.));
  start = step * i_proc;
  end   = step *(i_proc+1);
  if (i_proc == n_proc-1) end = N_trg;
  span = end - start;

  for (int i = 0; i<span; i++)
  {
      // single component vectors
      test[i] = test[start+i];
      bc_type[i] = bc_type[start+i];

      // tree component vectors
      for (int k=0;k<3;k++)
      {
          test3[3*i+k] = test3[3*start+3*i+k];
          vert_1[3*i+k] = vert_1[3*start+3*i+k];
          vert_2[3*i+k] = vert_2[3*start+3*i+k];
          vert_3[3*i+k] = vert_3[3*start+3*i+k];
          tr_di[3*i+k] = tr_di[3*start+3*i+k];
      }
  }
  test.resize(span);
  bc_type.resize(span);
  test3.resize(3*span);
  vert_1.resize(3*span);
  vert_2.resize(3*span);
  vert_3.resize(3*span);
  tr_di.resize(3*span);
  //==========================================================


  std::cout<<"  tr_di  "<<std::endl;
  for (int i = 0; i<tr_di.size();i++)
  {std::cout<<tr_di[i]<<std::endl;}

  // Cash vector with knowns
  dvec buff(tr_di.size());
  for (int i = 0; i<tr_di.size(); i++)
      buff[i] = tr_di[i];


  //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  // And here, instead of solving volume matvec, try to do our test problem
  N = 100;

  dvec trg_value(9);
  dvec trg_coord(3); trg_coord = {0.5, 0.5, 0.5};
  int n_trg = trg_coord.size()/3;

  dvec surf_coord(0);//generate_coords(N);
  int n_surf = surf_coord.size()/3;
  dvec surf_value(n_surf * 6);
  for( int i = 0; i < n_surf * 6; i++) { surf_value[i]=1.0; }

  dvec src_coord = generate_coords(N);
  int n_src = src_coord.size()/3;
  dvec src_value(n_src * 3);
  for( int i = 0; i < n_src * 3; i++) { src_value[i]=1.0; }


  direct_summation_stress(src_coord,
                          src_value,
                          surf_coord,
                          surf_value,
                          trg_coord,
                          trg_value);

  std::cout<<"DIRECT SUMMATION RESULT:"<<std::endl;

  std::cout<<"TRG_VALUE 0:"<<trg_value[0]<<std::endl;
  std::cout<<"TRG_VALUE 1:"<<trg_value[1]<<std::endl;
  std::cout<<"TRG_VALUE 2:"<<trg_value[2]<<std::endl;
  std::cout<<"TRG_VALUE 3:"<<trg_value[3]<<std::endl;
  std::cout<<"TRG_VALUE 4:"<<trg_value[4]<<std::endl;
  std::cout<<"TRG_VALUE 5:"<<trg_value[5]<<std::endl;
  std::cout<<"TRG_VALUE 6:"<<trg_value[6]<<std::endl;
  std::cout<<"TRG_VALUE 7:"<<trg_value[7]<<std::endl;
  std::cout<<"TRG_VALUE 8:"<<trg_value[8]<<std::endl;

  for (int i = 0; i<trg_value.size(); i++) trg_value[i] = 0;

//  calculate_stresses(
//         src_coord,
//         src_value,
//         surf_coord,
//         surf_value,
//         trg_coord,
//         trg_value,
//         m,
//          comm);


  const pvfmm::Kernel<double>& kernel_fn=ElasticKernel<double>::Stress();

  // Create memory-manager (optional)
  pvfmm::mem::MemoryManager mem_mgr(10000000);

  // Construct tree.
  size_t max_pts=600;
  pvfmm::PtFMM_Tree* tree=PtFMM_CreateTree(src_coord, src_value, surf_coord, surf_value, trg_coord, comm, max_pts, pvfmm::FreeSpace);

  // Load matrices.
  pvfmm::PtFMM matrices(&mem_mgr);
  matrices.Initialize(m, comm, &kernel_fn);

  // FMM Setup
  tree->SetupFMM(&matrices);

  // Run FMM
  PtFMM_Evaluate(tree, trg_value, n_trg);

  std::cout<<"FMM SUMMATION RESULT:"<<std::endl;

  std::cout<<"TRG_VALUE 0:"<<trg_value[0]<<std::endl;
  std::cout<<"TRG_VALUE 1:"<<trg_value[1]<<std::endl;
  std::cout<<"TRG_VALUE 2:"<<trg_value[2]<<std::endl;
  std::cout<<"TRG_VALUE 3:"<<trg_value[3]<<std::endl;
  std::cout<<"TRG_VALUE 4:"<<trg_value[4]<<std::endl;
  std::cout<<"TRG_VALUE 5:"<<trg_value[5]<<std::endl;
  std::cout<<"TRG_VALUE 6:"<<trg_value[6]<<std::endl;
  std::cout<<"TRG_VALUE 7:"<<trg_value[7]<<std::endl;
  std::cout<<"TRG_VALUE 8:"<<trg_value[8]<<std::endl;


  return 0;
  //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


  //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  // And here, instead of solving volume matvec, try to do our test problem
  N = 100;

  dvec trg_value(9);
  dvec trg_coord(3); trg_coord = {0.5, 0.5, 0.5};
  int n_trg = trg_coord.size()/3;

  dvec surf_coord(0);//generate_coords(N);
  int n_surf = surf_coord.size()/3;
  dvec surf_value(n_surf * 6);
  for( int i = 0; i < n_surf * 6; i++) { surf_value[i]=1.0; }

  dvec src_coord = generate_coords(N);
  int n_src = src_coord.size()/3;
  dvec src_value(n_src * 3);
  for( int i = 0; i < n_src * 3; i++) { src_value[i]=1.0; }


  direct_summation_stress(src_coord,
                          src_value,
                          surf_coord,
                          surf_value,
                          trg_coord,
                          trg_value);

  std::cout<<"DIRECT SUMMATION RESULT:"<<std::endl;

  std::cout<<"TRG_VALUE 0:"<<trg_value[0]<<std::endl;
  std::cout<<"TRG_VALUE 1:"<<trg_value[1]<<std::endl;
  std::cout<<"TRG_VALUE 2:"<<trg_value[2]<<std::endl;
  std::cout<<"TRG_VALUE 3:"<<trg_value[3]<<std::endl;
  std::cout<<"TRG_VALUE 4:"<<trg_value[4]<<std::endl;
  std::cout<<"TRG_VALUE 5:"<<trg_value[5]<<std::endl;
  std::cout<<"TRG_VALUE 6:"<<trg_value[6]<<std::endl;
  std::cout<<"TRG_VALUE 7:"<<trg_value[7]<<std::endl;
  std::cout<<"TRG_VALUE 8:"<<trg_value[8]<<std::endl;

  for (int i = 0; i<trg_value.size(); i++) trg_value[i] = 0;

  calculate_stresses(
         src_coord,
         src_value,
         surf_coord,
         surf_value,
         trg_coord,
         trg_value,
         m,
          comm);


//  const pvfmm::Kernel<double>& kernel_fn=ElasticKernel<double>::Stress();

//  // Create memory-manager (optional)
//  pvfmm::mem::MemoryManager mem_mgr(10000000);

//  // Construct tree.
//  size_t max_pts=600;
//  pvfmm::PtFMM_Tree* tree=PtFMM_CreateTree(src_coord, src_value, surf_coord, surf_value, trg_coord, comm, max_pts, pvfmm::FreeSpace);

//  // Load matrices.
//  pvfmm::PtFMM matrices(&mem_mgr);
//  matrices.Initialize(m, comm, &kernel_fn);

//  // FMM Setup
//  tree->SetupFMM(&matrices);

//  // Run FMM
//  PtFMM_Evaluate(tree, trg_value, n_trg);

  std::cout<<"FMM SUMMATION RESULT:"<<std::endl;

  std::cout<<"TRG_VALUE 0:"<<trg_value[0]<<std::endl;
  std::cout<<"TRG_VALUE 1:"<<trg_value[1]<<std::endl;
  std::cout<<"TRG_VALUE 2:"<<trg_value[2]<<std::endl;
  std::cout<<"TRG_VALUE 3:"<<trg_value[3]<<std::endl;
  std::cout<<"TRG_VALUE 4:"<<trg_value[4]<<std::endl;
  std::cout<<"TRG_VALUE 5:"<<trg_value[5]<<std::endl;
  std::cout<<"TRG_VALUE 6:"<<trg_value[6]<<std::endl;
  std::cout<<"TRG_VALUE 7:"<<trg_value[7]<<std::endl;
  std::cout<<"TRG_VALUE 8:"<<trg_value[8]<<std::endl;


  return 0;
  //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  ivec mnk_(3);
  // test of mnk function
  for (int i = 0; i<8; i++)
  {
      mnk_ = mnk(i, 2,2,2);

      std::cout<<"i = "
               <<i<<", m = "
               <<mnk_[0]<<", n = "
               <<mnk_[1]<<", k = "
               <<mnk_[2]<<std::endl;
  }


  return 0;


*/
