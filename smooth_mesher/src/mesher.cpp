#include "mesher.h"
#include "auxillary.h"
#include "mesh.h"

typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

extern double YOUNG;     // Young's modulus of the material
extern double POISS;     // Poisson's ratio of the material

int iii(const dim3 &v, const dim3 &s) {
    return iii(v.i, v.j, v.k, s.i, s.j, s.k);
}

void generate_BVP(

        // Input

        int M,
        int N,
        int K,
        double c,

        dvec & levelset,

        // Output
        ivec & is_surf,

        dvec & vert_1,    // first vertice of a triangular element
        dvec & vert_2,    // second vertice of a triangular element
        dvec & vert_3,    // third vertice of a triangular element
        //dvec & nor,      // element's outward normal
        dvec & tr_di,      // vector of tractions (known if type 1(Neumann) BC are specified)
        //dvec & colloc,    // array of collocation points

        ivec & bc_type,      // BC type: 1 - Neumann 2 - Dirichlet
        //dvec & are,      // element's area

        // MPI
        MPI_Comm comm
        )
{
    // Simple sequential mesher

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (!rank) std::cout<<"Generate new boundary..."<<std::endl;

    // 1) TDs field is gathered to a 0 process
    // 2) Sequential mesh generation
    // 3) Mesh is evenly scattered over processes

    int N_ele;  // total number of elements

    // Counting the number of elements

    dim3 MNK(M, N, K);

    VoxelMesh vm(MNK, point(0, 0, 0), point(c * M, c * N, c * K));

    vm.for_each_voxel([MNK, &levelset] (VoxelMesh &vm, const dim3 &vox) {
        vm[vox] = levelset[iii(vox, MNK)];
    });

    SurfaceMesh<triangle> sm(vm, 1);

    N_ele = sm.elements().size();

    if (!rank) std::cout << "Surface mesh has " << N_ele << " elements" << std::endl;

    vert_1.clear();
    vert_2.clear();
    vert_3.clear();
    tr_di.clear();
    bc_type.clear();

    for (size_t i = 0; i < sm.elements().size(); i++) {
        const triangle &tr = sm.elements()[i];
        const point p1(sm.points()[tr.p1]);
        const point p2(sm.points()[tr.p3]); // swapped p2 and p3
        const point p3(sm.points()[tr.p2]);
        vert_1.push_back(p1.x);
        vert_1.push_back(p1.y);
        vert_1.push_back(p1.z);
        vert_2.push_back(p2.x);
        vert_2.push_back(p2.y);
        vert_2.push_back(p2.z);
        vert_3.push_back(p3.x);
        vert_3.push_back(p3.y);
        vert_3.push_back(p3.z);

        const point center((p1 + p2 + p3) * (1. / 3));
        int bc;
        double bcvec[3];

        boundary_conditions(center.x, center.y, center.z, sm.types()[i], &bc, bcvec);
        bc_type.push_back(bc);
        tr_di.push_back(bcvec[0]);
        tr_di.push_back(bcvec[1]);
        tr_di.push_back(bcvec[2]);
    }

    vm.for_each_voxel([MNK, &is_surf] (VoxelMesh &vm, const dim3 &vox) {
            dim3 dv;
            for (dv.i = -1; dv.i <= 1; dv.i++)
                for (dv.j = -1; dv.j <= 1; dv.j++)
                    for (dv.k = -1; dv.k <= 1; dv.k++) {
                        dim3 nb(vox + dv);
                        if (nb.i < 0 || nb.j < 0 || nb.k < 0) continue;
                        if (nb.i >= MNK.i) continue;
                        if (nb.j >= MNK.j) continue;
                        if (nb.k >= MNK.k) continue;
                        if (vm.mark(nb) & 2)
                            is_surf[iii(vox, MNK)] = 1;
                    }
        });

    double margin = 10e-10;
    for (int i = 0; i<vert_1.size(); i++)
    {
        vert_1[i] = margin/2. + vert_1[i]*(1 - margin);
        vert_2[i] = margin/2. + vert_2[i]*(1 - margin);
        vert_3[i] = margin/2. + vert_3[i]*(1 - margin);
    }
    if (!rank) std::cout<<"Done boundary generation"<<std::endl;
}
