#!/bin/bash
#PBS -N optimizer_smooth_mesh
#PBS -l nodes=2:ppn=16
#!PBS -l pmem=1gb
#PBS -l walltime=20:00:00
#PBS -r n
#PBS -k oe

module load Compilers/Intel/psxe_2015.6 Math/petsc/3.7.2/i4/psxe2015.6 Math/pvfmm/1.0.0/psxe2015.6

date
echo Submitting job $PBS_JOBNAME on
hostname
echo "   "USER: $USER
echo "   "HOME: $HOME
echo "   "JobName: $PBS_JOBNAME
echo "   "JobID: $PBS_JOBID
echo "   "$PBS_NUM_NODES Nodes occupied
echo "   "Proc Per Node: $PBS_NUM_PPN

#mpirun -np 4  $PBS_O_WORKDIR/test_solver_ksp -omp 4 -N 60 -m 6
export NUM_THREADS=8
MPI_PN=$(($PBS_NUM_PPN/$NUM_THREADS))
MPI_PT=$((${PBS_NUM_NODES}*${MPI_PN}))

echo "NUM_THREADS=${NUM_THREADS}"
echo "MPI_PN=${MPI_PN}"
echo "MPI_PT=${MPI_PT}"
echo "NUM_CORES=${PBS_NP}"

JOBID=$(echo $PBS_JOBID | cut -f 1 -d .)

ln -sf $PBS_O_WORKDIR/optimizer_smooth_mesh.o$JOBID $PBS_O_WORKDIR/last_output
ln -sf $PBS_O_WORKDIR/optimizer_smooth_mesh.e$JOBID $PBS_O_WORKDIR/last_error

N=20

mpirun -genv I_MPI_JOB_RESPECT_PROCESS_PLACEMENT=disable -genv OMP_NUM_THREADS=${NUM_THREADS} -ppn ${MPI_PN} -np ${MPI_PT} $PBS_O_WORKDIR/optimizer -omp ${NUM_THREADS} -N $N -m 6


date


