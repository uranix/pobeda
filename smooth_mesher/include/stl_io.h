#ifndef STL_IO_H
#define STL_IO_H


//----------------------------
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <pvfmm.hpp>

//============================
typedef std::vector<double> dvec;
typedef std::vector<float> fvec;
typedef std::vector<int> ivec;

fvec fnorm1(fvec & v1, fvec & v2, fvec & v3);
void write_stl1(std::string filename, dvec vert_1, dvec vert_2, dvec vert_3);
void write_td2(std::string filename, dvec topo_derivs);
void write_vtk(const std::string &filename, int N, const dvec &topo_derivs, const dvec &levelset, const ivec &is_surf);
void write_surf_vtk(const std::string &filename, const dvec &vert_1, const dvec &vert_2, const dvec &vert_3, const dvec &tr, const dvec &di);
void write_td_slice(std::string filename, dvec topo_derivs, int M, int N, int K, int mode, int depth);


#endif // STL_IO_H
