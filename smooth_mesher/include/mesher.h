#ifndef MESHER_H
#define MESHER_H

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>
#include <omp.h>

typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

extern void boundary_conditions(double x, double y, double z, int face_number, int *bc_type, double *tr_di);

void generate_BVP(

        // Input

        int M,
        int N,
        int K,
        double c,
        dvec & levelset, // Levelset funtcion. Mesh is generated for level = 1, inner = level > 1
        ivec & is_surf,

        // Output

        dvec & vert_1,    // first vertice of a triangular element
        dvec & vert_2,    // second vertice of a triangular element
        dvec & vert_3,    // third vertice of a triangular element
        //dvec & norm,      // element's outward normal
        dvec & tr_di,      // vector of tractions (known if type 1(Neumann) BC are specified)
        //dvec & colloc,    // array of collocation points

        ivec & bc_t,      // BC type: 1 - Neumann 2 - Dirichlet
        //dvec & area,      // element's area

        // MPI
        MPI_Comm comm
        );

void gather_arrays();

void scatter_arrays();

#endif // MESHER_H


/*

def boundary_generation( M, N, K, c, side_BC, force_on_voxel, inner_points_3D_array, status ):
    print("Generate new boundary...")
    global vert_1 # first vertice of a triangular element 
    global vert_2 # second vertice of a triangular element 
    global vert_3 # third vertice of a triangular element
    global norm   # element's outward normal
    global trac   # vector of tractions (known if type 1(Neumann) BC are specified)
    global disp   # vector of displacements (known if type 2 (Dirishlet) BCs are specified)
    global bc_t   # BC type: 1 - Neumann 2 - Dirishlet
    global area   # element's area
    global colloc # array of collocation points 
    global N_ele  # total number of elements
    
    # Counting the number of elements
    
    N_ele = 0
    for m in range (0,M):
        for n in range (0,N):
            for k in range (0,K):
                if (status[m,n,k] == 1):
                    
                    if (m == 0) or (status[m-1,n,k]==0):
                        N_ele = N_ele + 4
                    if (m == M-1) or (status[m+1,n,k]==0):
                        N_ele = N_ele + 4
                    
                    if (n == 0) or (status[m,n-1,k]==0):
                        N_ele = N_ele + 4
                    if (n == N-1) or (status[m,n+1,k]==0):
                        N_ele = N_ele + 4
                    
                    if (k == 0) or (status[m,n,k-1]==0):
                        N_ele = N_ele + 4
                    if (k == K-1) or (status[m,n,k+1]==0):
                        N_ele = N_ele + 4
                        
                    
    print("I'm going to generate")
    print N_ele, " elements. Wait a bit ..."
    
    vert_1 = np.zeros([N_ele, 3]) # first vertice of a triangular element 
    vert_2 = np.zeros([N_ele, 3]) # second vertice of a triangular element 
    vert_3 = np.zeros([N_ele, 3]) # third vertice of a triangular element
    norm   = np.zeros([N_ele, 3]) # element's outward normal
    trac   = np.zeros([N_ele, 3]) # vector of tractions (known if type 1(Neumann) BC are specified)
    disp   = np.zeros([N_ele, 3]) # vector of displacements (known if type 2 (Dirishlet) BCs are specified)
    bc_t   = np.ones([N_ele])    # BC type: 1 - Neumann 2 - Dirishlet
    area   = np.zeros([N_ele])    # element's area
    colloc = np.zeros([N_ele, 3]) # array of collocation points 

    # Vertices, normals, BCs, tractions
    i = 0 # Element index
    for m in range (0,M):
        for n in range (0,N):
            for k in range (0,K):
                if (status[m,n,k] == 1):
                    # 1 Y+
                    if (n == N-1) or (status[m,n+1,k]==0): # Add element 1 and 2
                    
                        # Element 1 
                        vert_1[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_2[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i, 2] = inner_points_3D_array[m,n,k,2] 
                        
                        vert_3[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_3[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_3[i, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        

                        
                        trac[i, 0] = force_on_voxel[m,n,k,0, 0] 
                        trac[i, 1] = force_on_voxel[m,n,k,0, 1] 
                        trac[i, 2] = force_on_voxel[m,n,k,0, 2] 
                        
                        if (n==N-1) and (side_BC[0] == 2):
                            bc_t[i] = 2
                        
                        # Element 2
                        vert_1[i+1, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i+1, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i+1, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+1, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_2[i+1, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i+1, 2] = inner_points_3D_array[m,n,k,2] 
                        
                        vert_3[i+1, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_3[i+1, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_3[i+1, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        norm[i+1, 0] = 0.0
                        norm[i+1, 1] = 1.0
                        norm[i+1, 2] = 0.0
                        
                        trac[i+1, 0] = force_on_voxel[m,n,k,0, 0] 
                        trac[i+1, 1] = force_on_voxel[m,n,k,0, 1] 
                        trac[i+1, 2] = force_on_voxel[m,n,k,0, 2] 
                        
                        if (n==N-1) and (side_BC[0] == 2):
                            bc_t[i+1] = 2
                            
                        # Element 3
                        vert_1[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+2, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i+2, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+2, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_2[i+2, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i+2, 2] = inner_points_3D_array[m,n,k,2] 
                        
                        vert_3[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_3[i+2, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_3[i+2, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        

                        
                        trac[i+2, 0] = force_on_voxel[m,n,k,0, 0] 
                        trac[i+2, 1] = force_on_voxel[m,n,k,0, 1] 
                        trac[i+2, 2] = force_on_voxel[m,n,k,0, 2] 
                        
                        if (n==N-1) and (side_BC[0] == 2):
                            bc_t[i+2] = 2
                            
                        # Element 4
                        vert_1[i+3, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+3, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i+3, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i+3, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_2[i+3, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i+3, 2] = inner_points_3D_array[m,n,k,2] 
                        
                        vert_3[i+3, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_3[i+3, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_3[i+3, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        

                        
                        trac[i+3, 0] = force_on_voxel[m,n,k,0, 0] 
                        trac[i+3, 1] = force_on_voxel[m,n,k,0, 1] 
                        trac[i+3, 2] = force_on_voxel[m,n,k,0, 2] 
                        
                        if (n==N-1) and (side_BC[0] == 2):
                            bc_t[i+3] = 2    
                            
                            
                        i = i + 4
                    
                    # 2 X+
                    if (m == M-1) or (status[m+1,n,k]==0): # Add element 3 and 4 
                    
                        # Element 5 
                        vert_1[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_3[i, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i, 0] = force_on_voxel[m,n,k,1, 0] 
                        trac[i, 1] = force_on_voxel[m,n,k,1, 1] 
                        trac[i, 2] = force_on_voxel[m,n,k,1, 2] 
                        
                        if (m==M-1) and (side_BC[1] == 2):
                            bc_t[i] = 2
                        
                        # Element 6
                        vert_1[i+1, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i+1, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+1, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i+1, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i+1, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i+1, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+1, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_3[i+1, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+1, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i+1, 0] = force_on_voxel[m,n,k,1, 0] 
                        trac[i+1, 1] = force_on_voxel[m,n,k,1, 1] 
                        trac[i+1, 2] = force_on_voxel[m,n,k,1, 2] 
                        
                        if (m==M-1) and (side_BC[1] == 2):
                            bc_t[i+1] = 2
                        
                        # Element 7
                        vert_1[i+2, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+2, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+2, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+2, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+2, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_3[i+2, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+2, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i+2, 0] = force_on_voxel[m,n,k,1, 0] 
                        trac[i+2, 1] = force_on_voxel[m,n,k,1, 1] 
                        trac[i+2, 2] = force_on_voxel[m,n,k,1, 2] 
                        
                        if (m==M-1) and (side_BC[1] == 2):
                            bc_t[i+2] = 2
                        
                        
                        # Element 8
                        vert_1[i+3, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i+3, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i+3, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+3, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i+3, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+3, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i+3, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_3[i+3, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+3, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i+3, 0] = force_on_voxel[m,n,k,1, 0] 
                        trac[i+3, 1] = force_on_voxel[m,n,k,1, 1] 
                        trac[i+3, 2] = force_on_voxel[m,n,k,1, 2] 
                        
                        if (m==M-1) and (side_BC[1] == 2):
                            bc_t[i+3] = 2
                                   
                        i = i + 4
                        
                    # 3 Y-
                    if (n == 0) or (status[m,n-1,k]==0): # Add element 5 and 6 
                    
                        # Element 9 
                        vert_1[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_3[i, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i, 0] = force_on_voxel[m,n,k,2, 0] 
                        trac[i, 1] = force_on_voxel[m,n,k,2, 1] 
                        trac[i, 2] = force_on_voxel[m,n,k,2, 2] 
                        
                        if (n==0) and (side_BC[2] == 2):
                            bc_t[i] = 2
                        
                        # Element 10
                        vert_1[i+1, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i+1, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+1, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+1, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i+1, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+1, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i+1, 0] = inner_points_3D_array[m,n,k,0]
                        vert_3[i+1, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_3[i+1, 2] = inner_points_3D_array[m,n,k,2]
                        

                        
                        trac[i+1, 0] = force_on_voxel[m,n,k,2, 0] 
                        trac[i+1, 1] = force_on_voxel[m,n,k,2, 1] 
                        trac[i+1, 2] = force_on_voxel[m,n,k,2, 2] 
                        
                        if (n==0) and (side_BC[2] == 2):
                            bc_t[i+1] = 2
                        
                        # Element 11
                        vert_1[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+2, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+2, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+2, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_3[i+2, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i+2, 0] = force_on_voxel[m,n,k,2, 0] 
                        trac[i+2, 1] = force_on_voxel[m,n,k,2, 1] 
                        trac[i+2, 2] = force_on_voxel[m,n,k,2, 2] 
                        
                        if (n==0) and (side_BC[2] == 2):
                            bc_t[i+2] = 2
                        
                        # Element 12
                        vert_1[i+3, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+3, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+3, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i+3, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i+3, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+3, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+3, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i+3, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_3[i+3, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i+3, 0] = force_on_voxel[m,n,k,2, 0] 
                        trac[i+3, 1] = force_on_voxel[m,n,k,2, 1] 
                        trac[i+3, 2] = force_on_voxel[m,n,k,2, 2] 
                        
                        if (n==0) and (side_BC[2] == 2):
                            bc_t[i+3] = 2
                        
                        
                        
                        i = i + 4
                        
                    # 4 X-
                    if (m == 0) or (status[m-1,n,k]==0): # Add element 7 and 8 
                    
                        # Element 13 
                        vert_1[i, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_3[i, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i, 0] = force_on_voxel[m,n,k,3, 0] 
                        trac[i, 1] = force_on_voxel[m,n,k,3, 1] 
                        trac[i, 2] = force_on_voxel[m,n,k,3, 2] 
                        
                        if (m==0) and (side_BC[3] == 2):
                            bc_t[i] = 2
                        
                        
                        # Element 14
                        vert_1[i+1, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+1, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i+1, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+1, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i+1, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i+1, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+1, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_3[i+1, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+1, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i+1, 0] = force_on_voxel[m,n,k,3, 0] 
                        trac[i+1, 1] = force_on_voxel[m,n,k,3, 1] 
                        trac[i+1, 2] = force_on_voxel[m,n,k,3, 2] 
                        
                        if (m==0) and (side_BC[3] == 2):
                            bc_t[i+1] = 2
                        
                        # Element 15
                        vert_1[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+2, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i+2, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+2, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_3[i+2, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+2, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i+2, 0] = force_on_voxel[m,n,k,3, 0] 
                        trac[i+2, 1] = force_on_voxel[m,n,k,3, 1] 
                        trac[i+2, 2] = force_on_voxel[m,n,k,3, 2] 
                        
                        if (m==0) and (side_BC[3] == 2):
                            bc_t[i+2] = 2
                        
                        
                        # Element 16
                        vert_1[i+3, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+3, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+3, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i+3, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i+3, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+3, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i+3, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_3[i+3, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+3, 2] = inner_points_3D_array[m,n,k,2] 
                        

                        
                        trac[i+3, 0] = force_on_voxel[m,n,k,3, 0] 
                        trac[i+3, 1] = force_on_voxel[m,n,k,3, 1] 
                        trac[i+3, 2] = force_on_voxel[m,n,k,3, 2] 
                        
                        if (m==0) and (side_BC[3] == 2):
                            bc_t[i+3] = 2
                        
                        
                        
                        i = i + 4
                    
                    # 5 Z+
                    if (k == K-1) or (status[m,n,k+1]==0): # Add element 9 and 10 
                    
                        # Element 17 
                        vert_1[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        

                        
                        trac[i, 0] = force_on_voxel[m,n,k,4, 0] 
                        trac[i, 1] = force_on_voxel[m,n,k,4, 1] 
                        trac[i, 2] = force_on_voxel[m,n,k,4, 2] 
                        
                        if (k==K-1) and (side_BC[4] == 2):
                            bc_t[i] = 2
                        
                        
                        # Element 18
                        vert_1[i+1, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+1, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i+1, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+1, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i+1, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+1, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i+1, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i+1, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+1, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        

                        
                        trac[i+1, 0] = force_on_voxel[m,n,k,4, 0] 
                        trac[i+1, 1] = force_on_voxel[m,n,k,4, 1] 
                        trac[i+1, 2] = force_on_voxel[m,n,k,4, 2] 
                        
                        if (k==K-1) and (side_BC[4] == 2):
                            bc_t[i+1] = 2
                        
                        # Element 19
                        vert_1[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+2, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                    
                        vert_2[i+2, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+2, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i+2, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i+2, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+2, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        

                        
                        trac[i+2, 0] = force_on_voxel[m,n,k,4, 0] 
                        trac[i+2, 1] = force_on_voxel[m,n,k,4, 1] 
                        trac[i+2, 2] = force_on_voxel[m,n,k,4, 2] 
                        
                        if (k==K-1) and (side_BC[4] == 2):
                            bc_t[i+2] = 2
                        
                        # Element 20
                        vert_1[i+3, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i+3, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+3, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_2[i+3, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i+3, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i+3, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        
                        vert_3[i+3, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i+3, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+3, 2] = inner_points_3D_array[m,n,k,2] + c / 2.0
                        

                        
                        trac[i+3, 0] = force_on_voxel[m,n,k,4, 0] 
                        trac[i+3, 1] = force_on_voxel[m,n,k,4, 1] 
                        trac[i+3, 2] = force_on_voxel[m,n,k,4, 2] 
                        
                        if (k==K-1) and (side_BC[4] == 2):
                            bc_t[i+3] = 2
                        
                        
                        
                        i = i + 4
                    
                    
                    # 5 Z-
                    if (k == 0) or (status[m,n,k-1]==0): # Add element 11 and 12 
                    
                        # Element 21 
                        vert_1[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        

                        
                        trac[i, 0] = force_on_voxel[m,n,k,5, 0] 
                        trac[i, 1] = force_on_voxel[m,n,k,5, 1] 
                        trac[i, 2] = force_on_voxel[m,n,k,5, 2] 
                        
                        if (k==0) and (side_BC[5] == 2):
                            bc_t[i] = 2
                        
                        # Element 22
                        vert_1[i+1, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_1[i+1, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+1, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i+1, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i+1, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_2[i+1, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+1, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i+1, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+1, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        

                        
                        trac[i+1, 0] = force_on_voxel[m,n,k,5, 0] 
                        trac[i+1, 1] = force_on_voxel[m,n,k,5, 1] 
                        trac[i+1, 2] = force_on_voxel[m,n,k,5, 2] 
                        
                        
                        if (k==0) and (side_BC[5] == 2):
                            bc_t[i+1] = 2
                            
                        # Element 23
                        vert_1[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+2, 1] = inner_points_3D_array[m,n,k,1] - c / 2.0
                        vert_1[i+2, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i+2, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_2[i+2, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i+2, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+2, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i+2, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+2, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        

                        
                        trac[i+2, 0] = force_on_voxel[m,n,k,5, 0] 
                        trac[i+2, 1] = force_on_voxel[m,n,k,5, 1] 
                        trac[i+2, 2] = force_on_voxel[m,n,k,5, 2] 
                        
                        
                        if (k==0) and (side_BC[5] == 2):
                            bc_t[i+2] = 2
                        
                        # Element 24
                        vert_1[i+3, 0] = inner_points_3D_array[m,n,k,0] - c / 2.0
                        vert_1[i+3, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_1[i+3, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_2[i+3, 0] = inner_points_3D_array[m,n,k,0] + c / 2.0
                        vert_2[i+3, 1] = inner_points_3D_array[m,n,k,1] + c / 2.0
                        vert_2[i+3, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        
                        vert_3[i+3, 0] = inner_points_3D_array[m,n,k,0] 
                        vert_3[i+3, 1] = inner_points_3D_array[m,n,k,1] 
                        vert_3[i+3, 2] = inner_points_3D_array[m,n,k,2] - c / 2.0
                        

                        
                        trac[i+3, 0] = force_on_voxel[m,n,k,5, 0] 
                        trac[i+3, 1] = force_on_voxel[m,n,k,5, 1] 
                        trac[i+3, 2] = force_on_voxel[m,n,k,5, 2] 
                        
                        
                        if (k==0) and (side_BC[5] == 2):
                            bc_t[i+3] = 2
                        
                        i = i + 4
                    
                    
    x1 = np.zeros(3)
    x2 = np.zeros(3)
    x3 = np.zeros(3)

    # 2) ASSINING NORMALS AND BCs
    for i in range (0, N_ele):

        a1 = vert_2[i,0] - vert_1[i,0]
        a2 = vert_2[i,1] - vert_1[i,1]
        a3 = vert_2[i,2] - vert_1[i,2]

        b1 = vert_3[i,0] - vert_1[i,0]
        b2 = vert_3[i,1] - vert_1[i,1]
        b3 = vert_3[i,2] - vert_1[i,2]

        norm[i,0] =  a2*b3-a3*b2
        norm[i,1] =  a3*b1-a1*b3
        norm[i,2] =  a1*b2-a2*b1

        norm[i,0] = norm[i,0] / ( norm[i,0]*norm[i,0] + norm[i,1]*norm[i,1] + norm[i,2]*norm[i,2]   )**0.5
        norm[i,1] = norm[i,1] / ( norm[i,0]*norm[i,0] + norm[i,1]*norm[i,1] + norm[i,2]*norm[i,2]   )**0.5
        norm[i,2] = norm[i,2] / ( norm[i,0]*norm[i,0] + norm[i,1]*norm[i,1] + norm[i,2]*norm[i,2]   )**0.5


    # 2) AREAS OF ELEMENTS
    for k in range (0,N_ele):
        x1[0] = vert_1[k, 0]
        x1[1] = vert_1[k, 1]
        x1[2] = vert_1[k, 2]
        x2[0] = vert_2[k, 0]
        x2[1] = vert_2[k, 1]
        x2[2] = vert_2[k, 2]
        x3[0] = vert_3[k, 0]
        x3[1] = vert_3[k, 1]
        x3[2] = vert_3[k, 2]
        area[k] = 0.5 * np.linalg.norm((np.cross((x2-x1),(x3-x1))))
    
    # 3) COLLOCATION POINTS AT THE CENTROIDS OF ELEMENTS

    colloc = (1.0/3.0) * (vert_1 + vert_2 + vert_3)
    print("OK")


*/
