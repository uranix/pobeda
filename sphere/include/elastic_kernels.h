#ifndef ELASTIC_KERNELS_H
#define ELASTIC_KERNELS_H

// All KIFMM needs to know about elasticity

#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <ctime>
#include <pvfmm.hpp>

typedef std::vector<double> vec;



//======ELASTICITY FUNDAMENTAL SOLUTIONS========

const double one_over_4pi = 1.0 / ( 4.0 * pvfmm::const_pi<double>());
const double one_over_8pi = 1.0 / ( 8.0 * pvfmm::const_pi<double>());
const double one_over_16pi = 1.0 / ( 16.0 * pvfmm::const_pi<double>());

// Auxillary
inline double r_csi_x( const double * const csi, const double * const x )
{
        return sqrt((csi[0]-x[0])*(csi[0]-x[0]) + (csi[1]-x[1])*(csi[1]-x[1]) + (csi[2]-x[2])*(csi[2]-x[2]) );
}

inline double dr_dn( const double * const csi, const double * const x, const double * const n )
{
    return ((x[0]-csi[0])/ r_csi_x(csi,x)) * n[0] + ((x[1]-csi[1])/ r_csi_x(csi,x)) * n[1] + ((x[2]-csi[2])/ r_csi_x(csi,x)) * n[2];
}

inline double delta_ij( const int i, const int j )  {return i==j ? 1.0:0.0;}

//------Force[3] - displacement[3] fundamental solution----
inline double U_ij( const int i, const int j, const vec& v_csi, const vec& v_x,
             const double G, const double nu )
{
    const double * const csi = v_csi.data();
    const double * const x   =   v_x.data();

    if ((std::fabs(csi[0]-x[0])<10e-5)&&
        (std::fabs(csi[1]-x[1])<10e-5)&&
        (std::fabs(csi[2]-x[2])<10e-5)) return 0.;

    return (one_over_16pi/((1.0 - nu) * G * r_csi_x(csi,x)) *
            (delta_ij(i,j) * (3.0 - 4.0 * nu) +
            ((x[i-1] - csi[i-1]) * (x[j-1] - csi[j-1]))
            / (r_csi_x(csi,x) * r_csi_x(csi,x))));
}

//------Force[3] - traction[3] fundamental solution--------
inline double P_ij( const int i, const int j, const vec & v_csi, const vec & v_x,
             const vec & v_n, const double nu )
{

    const double * const csi = v_csi.data();
    const double * const x   =   v_x.data();
    const double * const n   =   v_n.data();
    if ((std::fabs(csi[0]-x[0])<10e-5)&&
        (std::fabs(csi[1]-x[1])<10e-5)&&
        (std::fabs(csi[2]-x[2])<10e-5)) return 0.;

    return -( one_over_8pi /( (1.0 - nu) * r_csi_x(csi,x)*r_csi_x(csi,x) ) )
            * ( ( delta_ij(i,j) * (1.0 - 2.0 * nu) +
                 ( 3.0 * (x[i-1]-csi[i-1]) * (x[j-1]-csi[j-1]) ) / ( r_csi_x(csi,x) * r_csi_x(csi,x) ) ) * dr_dn(csi, x, n)
            - (1.0 - 2.0 * nu) * ( ((x[i-1] - csi[i-1]) / r_csi_x(csi, x)) * n[j-1] -
                                   ((x[j-1] - csi[j-1]) / r_csi_x(csi, x)) * n[i-1] ));
}





//------Force[3] - traction[3] fundamental solution--------
inline double P_ij1( const int i, const int j, const vec & v_csi, const vec & v_x,
             const vec & v_n, const double nu )
{

    const double * const csi = v_csi.data();
    const double * const x   =   v_x.data();
    const double * const n   =   v_n.data();
    if ((std::fabs(csi[0]-x[0])<10e-5)&&
        (std::fabs(csi[1]-x[1])<10e-5)&&
        (std::fabs(csi[2]-x[2])<10e-5)) return 0.;

    return -( 1.0 / ( 8.0 * 3.14159265358 * (1.0 - nu) * (r_csi_x(csi,x)*r_csi_x(csi,x)) ) ) *
    ( ( delta_ij(i,j) * (1.0 - 2.0 * nu) + 3.0 * (x[i-1]-csi[i-1])*(x[j-1]-csi[j-1]) / (r_csi_x(csi,x)*r_csi_x(csi,x))  ) * dr_dn(csi, x, n)
     - (1.0 - 2.0 * nu) * ( ((x[i-1] - csi[i-1]) / r_csi_x(csi, x)) * n[j-1] - ((x[j-1] - csi[j-1]) / r_csi_x(csi, x)) * n[i-1] ));
}




//------Displacement[3] - stress[3x3] fundamental solution---
inline double U_ijk( const int i, const int j, const int k, const vec &v_csi, const vec& v_x, const double nu )
{
    const double * const csi = v_csi.data();
    const double * const x   =   v_x.data();

    return ((one_over_8pi /( (1.0 - nu) * r_csi_x(csi, x) * r_csi_x(csi, x) ))
    * (  ( 1.0 - 2.0 * nu) * (  (( x[i-1] - csi[i-1] )/ r_csi_x(csi, x)) * delta_ij(k,j)
                          + (( x[j-1] - csi[j-1] )/ r_csi_x(csi, x)) * delta_ij(k,i)
                          - (( x[k-1] - csi[k-1] )/ r_csi_x(csi, x)) * delta_ij(j,i))
        + 3.0          *  (( x[k-1] - csi[k-1] )/ r_csi_x(csi, x))
                       *  (( x[j-1] - csi[j-1] )/ r_csi_x(csi, x))
                       *  (( x[i-1] - csi[i-1] )/ r_csi_x(csi, x)) ));

}

//------Traction[3] - stress [3x3] fundamental solution (with MINUS)
inline double P_ijk(const int i, const int j, const int k, const vec& v_csi, const vec& v_x, const vec& v_n, const double G, const double nu )
{
    const double * const csi = v_csi.data();
    const double * const x   =   v_x.data();
    const double * const n   =   v_n.data();

    return ( - G * one_over_4pi /
             ( (1.0 - nu) * r_csi_x(csi, x) * r_csi_x(csi, x) * r_csi_x(csi, x))

            * (

            3 * dr_dn(csi, x, n) * ( (1.0 - 2.0 * nu) * delta_ij(i,j) * (( x[k-1] - csi[k-1] )/ r_csi_x(csi, x))
            + nu * ( delta_ij(i,k) *  (( x[j-1] - csi[j-1] )/ r_csi_x(csi, x))   +
                     delta_ij(j,k) *  (( x[i-1] - csi[i-1] )/ r_csi_x(csi, x)) ) -
                     5.0 * (( x[i-1] - csi[i-1] )/ r_csi_x(csi, x)) *
                           (( x[j-1] - csi[j-1] )/ r_csi_x(csi, x)) *
                           (( x[k-1] - csi[k-1] )/ r_csi_x(csi, x)) ) +

            3.0 * nu * (  n[i-1] * (( x[j-1] - csi[j-1] )/ r_csi_x(csi, x))
                     * (( x[k-1] - csi[k-1] )/ r_csi_x(csi, x))
                     + n[j-1] * (( x[i-1] - csi[i-1] )/ r_csi_x(csi, x))
                     * (( x[k-1] - csi[k-1] )/ r_csi_x(csi, x)) ) +

            (1.0 - 2.0 * nu) *
            ( 3.0 * n[k-1] * (( x[i-1] - csi[i-1] )/ r_csi_x(csi, x)) *
                     (( x[j-1] - csi[j-1] )/ r_csi_x(csi, x)) +
                     n[j-1] * delta_ij(i,k) + n[i-1] * delta_ij(j,k) ) -

            (1.0 - 4.0 * nu) * (n[k-1] * delta_ij(i,j)) ));

}

// My Customized PVFMM Kernels

template <class T>
void force_displacement(T* r_src, int src_cnt, T* v_src_, int dof,
                        T* r_trg, int trg_cnt, T* k_out,
                        pvfmm::mem::MemoryManager* mem_mgr){

//    template <class T>
//    void force_displacement(T* r_src, int src_cnt, T* v_src_, int dof,
//                            T* r_trg, int trg_cnt, T* k_out,
//                            pvfmm::mem::MemoryManager* mem_mgr,
//    const double nu = 0.3, const double G = 1.0){


#ifndef __MIC__
  pvfmm::Profile::Add_FLOP((long long)trg_cnt*(long long)src_cnt*(45));
#endif
  for(int t=0;t<trg_cnt;t++)
  {
      double nu = 0.3, G = 1;
      vec p(3); for (int kk=0; kk<3; kk++) p[kk]=0;
      vec v_src(3);
      for(int s=0;s<src_cnt;s++)
      {
         vec   x(3);   x = { r_src[3*s  ],  r_src[3*s+1] ,  r_src[3*s+2]};
         vec csi(3); csi = { r_trg[3*t  ],  r_trg[3*t+1] ,  r_trg[3*t+2]};

         // Source value stores displacement
         v_src[0] = v_src_[s*3  ];
         v_src[1] = v_src_[s*3+1];
         v_src[2] = v_src_[s*3+2];

         for(int i=0;i<3;i++)
         {
           p[0] += U_ij(1, 1+i, csi, x, G, nu ) * v_src[i];
           p[1] += U_ij(2, 1+i, csi, x, G, nu ) * v_src[i];
           p[2] += U_ij(3, 1+i, csi, x, G, nu ) * v_src[i];
         }
      }
      k_out[t*3+0] += p[0];
      k_out[t*3+1] += p[1];
      k_out[t*3+2] += p[2];
  }
}

template <class T>
void displacement_displacement(T* r_src, int src_cnt, T* v_src_, int dof, T* r_trg, int trg_cnt, T* k_out, pvfmm::mem::MemoryManager* mem_mgr){
#ifndef __MIC__
  pvfmm::Profile::Add_FLOP((long long)trg_cnt*(long long)src_cnt*(45));
#endif
  for(int t=0;t<trg_cnt;t++)
  {
      double nu = 0.3;
      vec p(3); for (int kk=0; kk<3; kk++) p[kk]=0;
      vec v_src(3), n(3);
      for(int s=0;s<src_cnt;s++)
      {
         vec   x(3);   x = { r_src[3*s  ],  r_src[3*s+1] ,  r_src[3*s+2]};
         vec csi(3); csi = { r_trg[3*t  ],  r_trg[3*t+1] ,  r_trg[3*t+2]};

         // Source value stores displacement
         v_src[0] = v_src_[s*6  ];
         v_src[1] = v_src_[s*6+1];
         v_src[2] = v_src_[s*6+2];

         // ..and normal
         n[0] = v_src_[s*6+3];
         n[1] = v_src_[s*6+4];
         n[2] = v_src_[s*6+5];

         for(int i=0;i<3;i++)
         {
           p[0] += P_ij(1, 1+i, csi, x, n, nu ) * v_src[i];
           p[1] += P_ij(2, 1+i, csi, x, n, nu ) * v_src[i];
           p[2] += P_ij(3, 1+i, csi, x, n, nu ) * v_src[i];
         }
      }
      k_out[t*3+0] += p[0];
      k_out[t*3+1] += p[1];
      k_out[t*3+2] += p[2];
  }
}

template <class T>
void force_stress(T* r_src, int src_cnt, T* v_src_, int dof, T* r_trg, int trg_cnt, T* k_out, pvfmm::mem::MemoryManager* mem_mgr){
#ifndef __MIC__
  pvfmm::Profile::Add_FLOP((long long)trg_cnt*(long long)src_cnt*(45));
#endif
  for(int t=0;t<trg_cnt;t++)
  {
      double nu = 0.3;
      vec p(9); for (int kk=0; kk<9; kk++) p[kk]=0;
      vec v_src(3);
      for(int s=0;s<src_cnt;s++)
      {
         vec   x(3);   x = { r_src[3*s  ],  r_src[3*s+1] ,  r_src[3*s+2]};
         vec csi(3); csi = { r_trg[3*t  ],  r_trg[3*t+1] ,  r_trg[3*t+2]};

         // Source value stores displacement
         v_src[0] = v_src_[s*3  ];
         v_src[1] = v_src_[s*3+1];
         v_src[2] = v_src_[s*3+2];

        for(int i=0;i<3;i++)
        {
          p[0] += U_ijk(1, 1, i+1, csi, x, nu ) * v_src[i];
          p[1] += U_ijk(1, 2, i+1, csi, x, nu ) * v_src[i];
          p[2] += U_ijk(1, 3, i+1, csi, x, nu ) * v_src[i];
          p[3] += U_ijk(2, 1, i+1, csi, x, nu ) * v_src[i];
          p[4] += U_ijk(2, 2, i+1, csi, x, nu ) * v_src[i];
          p[5] += U_ijk(2, 3, i+1, csi, x, nu ) * v_src[i];
          p[6] += U_ijk(3, 1, i+1, csi, x, nu ) * v_src[i];
          p[7] += U_ijk(3, 2, i+1, csi, x, nu ) * v_src[i];
          p[8] += U_ijk(3, 3, i+1, csi, x, nu ) * v_src[i];
        }
      }
      k_out[t*9+0] += p[0];
      k_out[t*9+1] += p[1];
      k_out[t*9+2] += p[2];
      k_out[t*9+3] += p[3];
      k_out[t*9+4] += p[4];
      k_out[t*9+5] += p[5];
      k_out[t*9+6] += p[6];
      k_out[t*9+7] += p[7];
      k_out[t*9+8] += p[8];
  }
}


template <class T>
void displacement_stress(T* r_src, int src_cnt, T* v_src_, int dof, T* r_trg, int trg_cnt, T* k_out, pvfmm::mem::MemoryManager* mem_mgr){
#ifndef __MIC__
  pvfmm::Profile::Add_FLOP((long long)trg_cnt*(long long)src_cnt*(45));
#endif
  for(int t=0;t<trg_cnt;t++)
  {
      double nu = 0.3, G = 1.0;
      vec p(9); for (int kk=0; kk<9; kk++) p[kk]=0;
      vec v_src(3), n(3);
      for(int s=0;s<src_cnt;s++)
      {
         vec   x(3);   x = { r_src[3*s  ],  r_src[3*s+1] ,  r_src[3*s+2]};
         vec csi(3); csi = { r_trg[3*t  ],  r_trg[3*t+1] ,  r_trg[3*t+2]};

         // Source value stores displacement
         v_src[0] = v_src_[s*6  ];
         v_src[1] = v_src_[s*6+1];
         v_src[2] = v_src_[s*6+2];

         // ..and normal
         n[0] = v_src_[s*6+3];
         n[1] = v_src_[s*6+4];
         n[2] = v_src_[s*6+5];

        for(int i=0;i<3;i++)
        {
          p[0] += P_ijk(1, 1, i+1, csi, x, n, G, nu ) * v_src[i];
          p[1] += P_ijk(1, 2, i+1, csi, x, n, G, nu ) * v_src[i];
          p[2] += P_ijk(1, 3, i+1, csi, x, n, G, nu ) * v_src[i];
          p[3] += P_ijk(2, 1, i+1, csi, x, n, G, nu ) * v_src[i];
          p[4] += P_ijk(2, 2, i+1, csi, x, n, G, nu ) * v_src[i];
          p[5] += P_ijk(2, 3, i+1, csi, x, n, G, nu ) * v_src[i];
          p[6] += P_ijk(3, 1, i+1, csi, x, n, G, nu ) * v_src[i];
          p[7] += P_ijk(3, 2, i+1, csi, x, n, G, nu ) * v_src[i];
          p[8] += P_ijk(3, 3, i+1, csi, x, n, G, nu ) * v_src[i];
        }
      }
      k_out[t*9+0] += p[0];
      k_out[t*9+1] += p[1];
      k_out[t*9+2] += p[2];
      k_out[t*9+3] += p[3];
      k_out[t*9+4] += p[4];
      k_out[t*9+5] += p[5];
      k_out[t*9+6] += p[6];
      k_out[t*9+7] += p[7];
      k_out[t*9+8] += p[8];
  }
}

template <class Real_t>
struct ElasticKernel{
  inline static const pvfmm::Kernel<Real_t>& Disp();
  inline static const pvfmm::Kernel<Real_t>& Stress();

};

template <class Real_t>
inline const pvfmm::Kernel<Real_t>& ElasticKernel<Real_t>::Disp(){
  static const pvfmm::Kernel<Real_t> ker=pvfmm::BuildKernel<Real_t,
          force_displacement<Real_t>,
          displacement_displacement<Real_t> >("Displaccement", 3, std::pair<int,int>(3,3));
  return ker;
}

template <class Real_t>
inline const pvfmm::Kernel<Real_t>& ElasticKernel<Real_t>::Stress(){
  static const pvfmm::Kernel<Real_t> ker=pvfmm::BuildKernel<Real_t, force_stress<Real_t>, displacement_stress<Real_t> >("Stress", 3, std::pair<int,int>(3,9));
  return ker;
}

///////////////////////////////////////////////////////////////////////////////






#endif // ELASTIC_KERNELS_H

