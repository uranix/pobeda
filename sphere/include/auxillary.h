#ifndef AUXILLARY_H
#define AUXILLARY_H

#include <iostream>
#include <vector>
#include <cmath>

typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

// Set of useful functions utilized around the project


// 1) vector reshape functions

// Column major vectorization

// 2D array
int ii(int m,int n,int M,int N);


// 3D array
int iii(int m,int n,int k,int M,int N,int K);

ivec mnk(int i, int M,int N,int K);

// 4D array
int iiii(int m,int n,int k,int l,int M,int N,int K,int L);


// 5D array
int iiiii(int m,int n,int k,int l,int p,int M,int N,int K,int L,int P);


// 2) Normal components by three points of triangle
dvec norm(dvec & v1, dvec & v2, dvec & v3);

// 3) Area of triangle by its three components
double area(dvec & v1, dvec & v2, dvec & v3);

#endif // AUXILLARY_H
