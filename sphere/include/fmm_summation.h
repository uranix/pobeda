#ifndef FMM_SUMMATION_H
#define FMM_SUMMATION_H

#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <ctime>
#include <pvfmm.hpp>

typedef std::vector<double> vec;

// This file contains the necessary set of fmm summation procedures


// Prototype fmm summation procedure
void calculate_displacements(
        vec & src_coord,
        vec & src_value,
        vec & surf_coord,
        vec & surf_value,
        vec & trg_coord,
        vec & trg_value,
        int mult_order,
        MPI_Comm comm);

// Prototype fmm summation procedure
void re_calculate_displacements(
        vec & src_value,
        vec & surf_value,
        vec & trg_value);

// Initial fmm setup
void init_fmm_pass(
        vec & src_coord,
        vec & src_value,
        vec & surf_coord,
        vec & surf_value,
        vec & trg_coord,
        vec & trg_value,
        int mult_order,
        MPI_Comm comm);






// Direct summation to check FMM
// Caution - this function is SERIAL
void direct_summation_displacement(vec & src_coord,
                                   vec & src_value,
                                   vec & surf_coord,
                                   vec & surf_value,
                                   vec & trg_coord,
                                   vec & trg_value);
// Domain fields summation
void calculate_stresses(
        vec & src_coord,
        vec & src_value,
        vec & surf_coord,
        vec & surf_value,
        vec & trg_coord,
        vec & trg_value,
        int mult_order,
        MPI_Comm comm);



#endif // FMM_SUMMATION_H
