#ifndef STL_IO_H
#define STL_IO_H


//----------------------------
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <pvfmm.hpp>

//============================
typedef std::vector<double> dvec;
typedef std::vector<float> fvec;
typedef std::vector<int> ivec;

fvec fnorm1(fvec & v1, fvec & v2, fvec & v3);
void write_stl1(std::string filename, dvec vert_1, dvec vert_2, dvec vert_3);
void  write_td2(std::string filename, dvec topo_derivs);
void write_td_slice(std::string filename, dvec topo_derivs, int M, int N, int K, int mode, int depth);


void write_anal_solution(double R_c, int N, double S, double nu);

#endif // STL_IO_H
