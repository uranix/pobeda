#include <iostream>
#include <iomanip>
#include "matvec.h"
#include "auxillary.h"
#include "fmm_summation.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"

# define SRC_PER_TRG 16
# define DIM 3
# define SURF_DIM 6

extern int N_iter;

typedef std::vector<double> dvec;
//using  ivec = std::vector<int>;

using std::endl;
using std::cout;
using std::setw;

/*! Proper "matvec" to be used in the solver. */
void matvec(
             const PetscScalar * v,   // Input
             PetscScalar * Av,  // Output
             const int v_size,
             // Additional parameters

             // Triangles
             const dvec & vert_1,
             const dvec & vert_2,
             const dvec & vert_3,

             // Boundary conditions
             const ivec & bc_t,

             // FMM args
             int mult_order,
             MPI_Comm comm
        )
{

    N_iter++;
    std::cout<<"ITERATION # "<<N_iter<<std::endl;

    int n_proc1; // Number of MPI threads
    int i_proc1; // index of this MPI thread
    MPI_Comm_size(comm, &n_proc1);
    MPI_Comm_rank(comm, &i_proc1);

    // This function performs multiplication A*v for a given v

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    // for (int i = 0; i<bc_t.size(); i++) bc_t[i] = 2; // Only PP matrix

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt++;
        if (bc_t[i]==2) src_cnt++;
    }
    src_cnt *=SRC_PER_TRG; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG;

    // !!!  Bellow 6 dvectors are of very long size ~ 10^6. Should be optimized later  !!!
    dvec src_coord(DIM*src_cnt);
    dvec src_value(DIM*src_cnt);
    dvec surf_coord(DIM*surf_cnt);
    dvec surf_value(SURF_DIM*surf_cnt);
    dvec trg_coord(DIM * trg_cnt);
    dvec trg_value(DIM * trg_cnt);

    // Setting up FMM arrays

    for (int i = 0; i < DIM*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM*SRC_PER_TRG;
    int bv_size = SURF_DIM*SRC_PER_TRG;
    dvec sbuff_coord(bc_size);
    dvec sbuff_src_v(bc_size);
    dvec sbuff_surf_v(bv_size);

    dvec v1(DIM), v2(DIM), v3(DIM), val(DIM);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }

        if (bc_t[i]==1)
        {
            triangle2src(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            triangle2src(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }

    // Handmade PVFMM is called here. Is declared in fmm_summation where genuine pvfmm is called.

//    calculate_displacements(
//            src_coord,
//            src_value,
//            surf_coord,
//            surf_value,
//            trg_coord,
//            trg_value,
//            mult_order,
//            comm);

    direct_summation_displacement(src_coord,
                                  src_value,
                                  surf_coord,
                                  surf_value,
                                  trg_coord,
                                  trg_value);

//    re_calculate_displacements(
//            src_value,
//            surf_value,
//            trg_value
//           );

    for (int i = 0; i < DIM * trg_cnt; i++) Av[i] = trg_value[i];

    //=========================================================
    // 2) LOCAL pass
    //=========================================================

    // a) add (1/2)*delta_ij * u_j = 1/2 * u_i

    for (int i = 0; i < trg_cnt; i++)
    { if (bc_t[i]==1) {for (int k = 0; k<DIM; k++) Av[DIM * i + k] += 0.5 * v[DIM * i + k];}}

    // b) Subtract-add cycle
    dvec add(DIM); dvec subtr(DIM);
    for (int i = 0; i < trg_cnt; i++)
    {

        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }
        local_pass( subtr, add, v1, v2, v3, 3-bc_t[i], val);
        for (int k = 0; k < DIM; k++)
        {
            if (bc_t[i]==2)
            { subtr[k] = - subtr[k]; add[k] = - add[k]; } // U matr comes with minus sign
            Av[DIM*i+k] -= subtr[k]; Av[DIM*i+k] += add[k];
        }
    }
    for (int i = 0; i<v_size; i++){Av[i] = -Av[i];}

}



void rhs(dvec & v,   // Input
         dvec & Av,  // Output

             // Additional parameters
             // Triangles
             const dvec & vert_1,
             const dvec & vert_2,
             const dvec & vert_3,

             // Boundary conditions
             const ivec & bc_te,

             // FMM args
             const int mult_order,
             MPI_Comm comm
        )
{
    // This function performs multiplication BB*bcs and generates rhs

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matdvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    ivec bc_t(bc_te.size());
    for (int i = 0; i<bc_t.size(); i++) bc_t[i] = 3 - bc_te[i]; // Only PP matrix

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt ++;
        if (bc_t[i]==2) src_cnt ++;
    }
    src_cnt *=SRC_PER_TRG; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG;
    dvec src_coord(DIM*src_cnt);
    dvec src_value(DIM*src_cnt);
    dvec surf_coord(DIM*surf_cnt);
    dvec surf_value(SURF_DIM*surf_cnt);
    dvec trg_coord(DIM * trg_cnt);
    dvec trg_value(DIM * trg_cnt);

    // Setting up FMM arrays

    for (int i = 0; i < DIM*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM*SRC_PER_TRG;
    int bv_size = SURF_DIM*SRC_PER_TRG;
    dvec sbuff_coord(bc_size);
    dvec sbuff_src_v(bc_size);
    dvec sbuff_surf_v(bv_size);

    dvec v1(DIM), v2(DIM), v3(DIM), val(DIM);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }

        if (bc_t[i]==1)
        {
            triangle2src(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            triangle2src(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }


    direct_summation_displacement(src_coord,
                                  src_value,
                                  surf_coord,
                                  surf_value,
                                  trg_coord,
                                  trg_value);

//    calculate_displacements(
//            src_coord,
//            src_value,
//            surf_coord,
//            surf_value,
//            trg_coord,
//            trg_value,
//            mult_order,
//            comm);


    for (int i = 0; i < DIM * trg_cnt; i++) Av[i] = trg_value[i];

    //=========================================================
    // 2) LOCAL pass
    //=========================================================

    // a) add (1/2)*delta_ij * u_j = 1/2 * u_i

    for (int i = 0; i < trg_cnt; i++)
    { if (bc_t[i]==1) {for (int k = 0; k<DIM; k++) Av[DIM * i + k] += 0.5 * v[DIM * i + k];}}

    // b) Subtract-add cycle
    dvec add(DIM); dvec subtr(DIM);
    for (int i = 0; i < trg_cnt; i++)
    {

        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = v[i*DIM+k];
        }
        local_pass( subtr, add, v1, v2, v3, 3-bc_t[i], val);
        for (int k = 0; k < DIM; k++)
        {
            if (bc_t[i]==2)
            { subtr[k] = - subtr[k]; add[k] = - add[k]; } // U matr comes with minus sign
            Av[DIM*i+k] -= subtr[k]; Av[DIM*i+k] += add[k];
        }
    }
}


// This function subtracts inaccurate contributions
// from a current triangle, and then replaces them with
// exact analytical singular integral

void local_pass(

        dvec & sub,
        dvec & add,

        dvec & v_1,
        dvec & v_2,
        dvec & v_3,

        int bc,
        dvec & val)
{

    double G = 1., nu = 0.3; // Kto by somnevalsya :)


    dvec csi(DIM); for (int i = 0; i<DIM; i++) csi[i] = (1./3.)*(v_1[i] + v_2[i] + v_3[i]);
    //============================================
    //             Add value
    //============================================

    // (invert bc when using for RHS correction)
    for (int i = 0; i<DIM; i++) add[i] = 0;
    if (bc==1)
    {
        for (int i = 0; i<DIM; i++)
        {
            for (int j = 0; j<DIM; j++)
            {
                add[i] += sint_u(i+1, j+1, v_1, v_2, v_3, csi, G, nu)* val[j];
            }
        }
    }
    if (bc==2)
    {
        for (int i = 0; i<DIM; i++)
        {
            for (int j = 0; j<DIM; j++)
            {
                add[i] += sint_p(i+1, j+1, v_1, v_2, v_3, csi, nu)* val[j];
            }
        }
    }

    //for (int i = 0; i<3; i++) {add[i] = 0;}

    //=========================================
    //             Sub value
    //=========================================

    for (int i = 0; i<DIM; i++) sub[i] = 0;

    int bc_size = DIM * SRC_PER_TRG; // 3 * (sources_per_triangle)
    int bv_size = SURF_DIM * SRC_PER_TRG; // 6 * (sources_per_triangle)

    dvec src_coord(bc_size);
    dvec src_value(bc_size);

    dvec surf_coord(bc_size);
    dvec surf_value(bv_size);

    dvec x(DIM), d(DIM), n(DIM);
    if (bc==1)
    {
        triangle2src(1, v_1, v_2, v_3, src_coord, src_value, val);

        // Direct summation over src and surf
        for (int s=0; s< src_coord.size()/3; s++)
        {
            x[0] = src_coord[s*DIM];
            x[1] = src_coord[s*DIM+1];
            x[2] = src_coord[s*DIM+2];

            d[0] = src_value[s*DIM];
            d[1] = src_value[s*DIM+1];
            d[2] = src_value[s*DIM+2];

            for (int i = 0; i<DIM; i++){
                for (int j = 0; j<DIM; j++){
                    sub[i] += U_ij( i+1, j+1, csi, x, G, nu ) * d[j];
                }
            }
        }
    }

    if (bc==2)
    {
        triangle2src(2, v_1, v_2, v_3, surf_coord, surf_value, val);

        // Direct summation over src and surf
        for (int s=0; s< src_coord.size()/3; s++)
        {
            x[0] = surf_coord[s*DIM];
            x[1] = surf_coord[s*DIM+1];
            x[2] = surf_coord[s*DIM+2];

            d[0] = surf_value[s*SURF_DIM];
            d[1] = surf_value[s*SURF_DIM+1];
            d[2] = surf_value[s*SURF_DIM+2];

            n[0] = surf_value[s*SURF_DIM+3];
            n[1] = surf_value[s*SURF_DIM+4];
            n[2] = surf_value[s*SURF_DIM+5];

            for (int i = 0; i<DIM; i++){
                for (int j = 0; j<DIM; j++){
                    sub[i] += P_ij( i+1, j+1, csi, x, n, nu ) * d[j];
                }
            }
        }
    }
    //for (int i = 0; i<3; i++) {sub[i] = 0;}
}



// This function generates the set of sources on the triangle
//         src_coord - contains coordinates of 16 source points
//                     (len(src_coords) = 48)
//         src_value - contains quadrature weights
//                     NOT multiplied by the solution
//         AND normals (len(src_value) = 96)


void triangle2src(
        int mode, // 1 - generate src, 2 - generate surf
        dvec & v_1,
        dvec & v_2,
        dvec & v_3,

        dvec & src_coord,
        dvec & src_value,

        dvec & val
        )
{
    // Normal components by
    // Cubic Gauss quadrature points and weights
    dvec eta_1(4); eta_1 = { 1./3., 3./5., 1./5., 1./5. };
    dvec eta_2(4); eta_2 = { 1./3., 1./5., 3./5., 1./5. };
    dvec eta_3(4); for (int i = 0; i < 4; i++)
    {  eta_3[i] = 1. - eta_1[i] - eta_2[i]; }
    dvec ww_i(4);  ww_i =  {-9.0/32.0, 25.0/96.0, 25.0/96.0, 25.0/96.0};
    // Split triangle into 4 pieces
    dvec v_12(DIM); for (int i = 0; i < DIM; i++) v_12[i] = (1./2.) * (v_1[i] + v_2[i]);
    dvec v_13(DIM); for (int i = 0; i < DIM; i++) v_13[i] = (1./2.) * (v_1[i] + v_3[i]);
    dvec v_23(DIM); for (int i = 0; i < DIM; i++) v_23[i] = (1./2.) * (v_2[i] + v_3[i]);

    int i = 0; // Point index
    dvec x_1(DIM); dvec x_2(DIM); dvec x_3(DIM);
    for (int t = 0; t<4; t++ ) // cycle over triangles
    {
        if (t==0) for (int v = 0; v < DIM; v++) { x_1[v] = v_1[v];  x_2[v] = v_12[v]; x_3[v] = v_13[v];}
        if (t==1) for (int v = 0; v < DIM; v++) { x_1[v] = v_2[v];  x_2[v] = v_23[v]; x_3[v] = v_12[v];}
        if (t==2) for (int v = 0; v < DIM; v++) { x_1[v] = v_3[v];  x_2[v] = v_13[v]; x_3[v] = v_23[v];}
        if (t==3) for (int v = 0; v < DIM; v++) { x_1[v] = v_12[v]; x_2[v] = v_23[v]; x_3[v] = v_13[v];}

        for (int c = 0; c<4; c++ ) // cycle over gauss points
        {
            for (int m = 0; m<DIM; m++)
            {
                if (mode == 1){
                    src_coord[DIM*i + m] = x_1[m] * eta_1[c] + x_2[m] * eta_2[c] + x_3[m] * eta_3[c];
                    src_value[DIM*i + m] = ww_i[c] * 2.0 * area(x_1, x_2, x_3) * val[m];
                }

                if (mode == 2){
                    src_coord[DIM*i + m] = x_1[m] * eta_1[c] + x_2[m] * eta_2[c] + x_3[m] * eta_3[c];
                    src_value[SURF_DIM*i + m] = ww_i[c] * 2.0 * area(x_1, x_2, x_3) * val[m];
                    src_value[SURF_DIM*i + DIM + m] = norm(v_1, v_2, v_3)[m];
                }
            }
            i++;
        }
    }
}


/*! Old style matvec */
void init_matvec(
             // Triangles
             const dvec & vert_1,
             const dvec & vert_2,
             const dvec & vert_3,

             // Boundary conditions
             const ivec & bc_t,

             // FMM args
             int mult_order,
             MPI_Comm comm
        )
{
    // This function initializes fmm

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    // for (int i = 0; i<bc_t.size(); i++) bc_t[i] = 2; // Only PP matrix

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt ++;
        if (bc_t[i]==2) src_cnt ++;
    }
    src_cnt *=SRC_PER_TRG; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG;
    // !!!  Bellow 6 vectors are of very long size ~ 10^6. Should be optimized later  !!!
    vec src_coord(DIM*src_cnt);
    vec src_value(DIM*src_cnt);
    vec surf_coord(DIM*surf_cnt);
    vec surf_value(SURF_DIM*surf_cnt);
    vec trg_coord(DIM * trg_cnt);
    vec trg_value(DIM * trg_cnt);

    // Setting up FMM arrays

    for (int i = 0; i < DIM*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM*SRC_PER_TRG;
    int bv_size = SURF_DIM*SRC_PER_TRG;
    vec sbuff_coord(bc_size);
    vec sbuff_src_v(bc_size);
    vec sbuff_surf_v(bv_size);

    vec v1(DIM), v2(DIM), v3(DIM), val(DIM);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val[k] = 1.0;
        }

        if (bc_t[i]==1)
        {
            triangle2src(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            triangle2src(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }

    // Handmade PVFMM is called here. Is declared in fmm_summation where genuine pvfmm is called.
    // Initial fmm setup
    init_fmm_pass(
            src_coord,
            src_value,
            surf_coord,
            surf_value,
            trg_coord,
            trg_value,
            mult_order,
            comm);

}


void volume_matvec(

            // Triangles
            const dvec & vert_1,
            const dvec & vert_2,
            const dvec & vert_3,

            // Volume points
            dvec & trg_coord,

            // Surface solution
            const dvec & tractions,
            const dvec & displacements,

            dvec & stresses,

            // FMM args
            int mult_order,
            MPI_Comm comm
        )
{

    // Generate set of sources by vert_1, vert_2, vert_3

    int tri_cnt = vert_1.size()/3; // triangle count
    int src_cnt = tri_cnt * SRC_PER_TRG; // 16 sources per triangle

    int trg_cnt = trg_coord.size()/3;

    dvec src_coord(DIM*src_cnt);
    dvec src_value(DIM*src_cnt);
    dvec surf_coord(DIM*src_cnt);
    dvec surf_value(SURF_DIM*src_cnt);

    // Setting up FMM arrays
    int bc_size = DIM*SRC_PER_TRG;
    int bv_size = SURF_DIM*SRC_PER_TRG;
    dvec sbuff_coord(bc_size);
    dvec sbuff_src_v(bc_size);
    dvec sbuff_surf_v(bv_size);

    dvec v1(DIM), v2(DIM), v3(DIM), val_d(DIM), val_t(DIM);

    for (int i = 0; i<tri_cnt; i++)
    {
        for (int k = 0; k<DIM; k++)
        {
            v1[k] = vert_1[i*DIM+k];
            v2[k] = vert_2[i*DIM+k];
            v3[k] = vert_3[i*DIM+k];
            val_d[k] = displacements[i*DIM+k];
            val_t[k] = tractions[i*DIM+k];

        }
        triangle2src(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val_d);
        for (int m = 0; m < bc_size;m++) surf_coord[bc_size*i + m] = sbuff_coord[m];
        for (int m = 0; m < bv_size;m++) surf_value[bv_size*i + m] = sbuff_surf_v[m];


        triangle2src(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val_t);
        for (int m = 0; m < bc_size;m++)  src_coord[bc_size*i + m] = sbuff_coord[m];
        for (int m = 0; m < bc_size;m++)  src_value[bc_size*i + m] = sbuff_src_v[m];

    }

    // FMM summation
    calculate_stresses(
            src_coord,
            src_value,
            surf_coord,
            surf_value,
            trg_coord,
            stresses,
            mult_order,
            comm);
}


void direct_summation_stress(      vec & src_coord,
                                   vec & src_value,
                                   vec & surf_coord,
                                   vec & surf_value,
                                   vec & trg_coord,
                                   vec & trg_value,
                                   double SHEAR,
                                   double POISS)
{
    for (int i=0; i<trg_value.size();i++) {trg_value[i] = 0.0;}
    vec x(3), d(3), n(3), csi(3);
    for (int t = 0; t<trg_coord.size()/3;t++)
    {
        for (int i=0; i<3;i++) csi[i] = trg_coord[3*t+i];
        // SURF
        for (int s=0; s<surf_coord.size()/3;s++)
        {
            for (int i=0; i<3;i++){
                x[i] = surf_coord[s*3+i];
                d[i] = surf_value[s*6+i];
                n[i] = surf_value[s*6+3+i];
            }

            for (int k = 0; k<3; k++){

                trg_value[9*t+0] += P_ijk( 1, 1, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+1] += P_ijk( 1, 2, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+2] += P_ijk( 1, 3, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+3] += P_ijk( 2, 1, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+4] += P_ijk( 2, 2, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+5] += P_ijk( 2, 3, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+6] += P_ijk( 3, 1, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+7] += P_ijk( 3, 2, k+1, csi, x, n, SHEAR, POISS ) * d[k];
                trg_value[9*t+8] += P_ijk( 3, 3, k+1, csi, x, n, SHEAR, POISS ) * d[k];
            }
        }
        //SRC
        for (int s=0; s<src_coord.size()/3;s++)
        {
            for (int i=0; i<3;i++){
                x[i] = src_coord[s*3+i];
                d[i] = src_value[s*3+i];}

            for (int k = 0; k<3; k++){
                trg_value[9*t+0] += U_ijk( 1, 1, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+1] += U_ijk( 1, 2, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+2] += U_ijk( 1, 3, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+3] += U_ijk( 2, 1, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+4] += U_ijk( 2, 2, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+5] += U_ijk( 2, 3, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+6] += U_ijk( 3, 1, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+7] += U_ijk( 3, 2, k+1, csi, x, POISS ) * d[k];
                trg_value[9*t+8] += U_ijk( 3, 3, k+1, csi, x, POISS ) * d[k];
            }
        }
    }
    return;
}


void test_integral_u()
{
    double nu = 0.3;
    double G = 1.0;
    dvec val = {1.,  1.,  1.};
    std::cout<<"Test integral with U_ijk:"<<std::endl;
    dvec v_1 = {0.,  0.,  0.};
    dvec v_2 = {1.,  0.,  0.};
    dvec v_3 = {0.,  0.,  1.};

    //dvec nor = {0.,  -1.,  0.};

    dvec src_coord(DIM * SRC_PER_TRG);
    dvec src_value(DIM * SRC_PER_TRG);

    dvec surf_coord(0);
    dvec surf_value(0);

    triangle2src( 1, v_1, v_2, v_3, src_coord, src_value, val);

    dvec trg_coord = { 20.,  0.,   2.};
    dvec trg_value(9);

    direct_summation_stress( src_coord,
                             src_value,
                             surf_coord,
                             surf_value,
                             trg_coord,
                             trg_value,
                                 G,
                                 nu);
    cout.precision(9);
    for (int i = 0; i< 9; i++) std::cout<<trg_value[i]<<" "<<std::endl;
}

void test_integral_p()
{
    double nu = 0.3;
    double G = 1.0;

    dvec val = {1.,  1.,  1.};
    std::cout<<"Test integral with P_ijk:"<<std::endl;
    dvec v_1 = {0.,  0.,  0.};
    dvec v_2 = {1.,  0.,  0.};
    dvec v_3 = {0.,  0.,  1.};

    //dvec nor = {0.,  -1.,  0.};

    dvec surf_coord(DIM * SRC_PER_TRG);
    dvec surf_value(SURF_DIM * SRC_PER_TRG);

    dvec src_coord(0);
    dvec src_value(0);

    triangle2src( 2, v_1, v_2, v_3, surf_coord, surf_value, val);

    dvec trg_coord = { 20.,  0.,   2.};
    dvec trg_value(9);

    direct_summation_stress( src_coord,
                             src_value,
                             surf_coord,
                             surf_value,
                             trg_coord,
                             trg_value,
                                 G,
                                 nu);
    cout.precision(9);
    for (int i = 0; i< 9; i++) std::cout<<trg_value[i]<<" "<<std::endl;
}


// Add homogeneous strain field to the RHS
void add_hsf (dvec & rhs,

              // Additional parameters
              // Triangles
              const dvec & vert_1,
              const dvec & vert_2,
              const dvec & vert_3,

              // Strain tensor

              const dvec & eps

              )
{
    // Determine coordinates of collocation points
    dvec csi(vert_1.size()); for (int i = 0; i<vert_1.size(); i++) csi[i] = (1./3.)*(vert_1[i] + vert_2[i] + vert_3[i]);

    // Determine the addition to the RHS due to homogeneous strain field
    dvec uo(csi.size());

    double x,y,z;
    for (int i = 0; i<csi.size()/3.; i++)
    {
        uo[3*i+0] = eps[0] * csi[3*i+0] + eps[3] * csi[3*i+1] + eps[4] * csi[3*i+2];
        uo[3*i+1] = eps[3] * csi[3*i+0] + eps[1] * csi[3*i+1] + eps[5] * csi[3*i+2];
        uo[3*i+2] = eps[4] * csi[3*i+0] + eps[5] * csi[3*i+1] + eps[2] * csi[3*i+2];

    }

    // Add it to the RHS
    for (int i = 0; i<rhs.size(); i++) rhs[i] = rhs[i] + uo[i];

}

