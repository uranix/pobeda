#include "auxillary.h"


typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

// Column major vectorization

// 2D array
int ii(int m,int n,int M,int N)
{
    if ((m<0)||(n<0)) return -1;
    if ((m>M-1)||(n>N-1)) return -2;
    return n + N * m;
}

// 3D array
int iii(int m,int n,int k,int M,int N,int K)
{
    if ((m<0)||(n<0)||(k<0)) return -1;
    if ((m>M-1)||(n>N-1)||(k>K-1)) return -2;
    return k + K * n + K * N * m;
}

ivec mnk(int i, int M,int N,int K)
{
    ivec mnk(3);
    mnk[0] = int ( std::floor( i / (K * N) ) );
    mnk[1] = int ( std::floor( (i - mnk[0] * K * N ) / K ) );
    mnk[2] = i - mnk[0] * K * N - mnk[1] * K;
    return mnk;
}

// 4D array
int iiii(int m,int n,int k,int l,int M,int N,int K,int L)
{
    if ((m<0)||(n<0)||(k<0)||(l<0)) return -1;
    if ((m>M-1)||(n>N-1)||(k>K-1)||(l>L-1)) return -2;
    return l + L * k + L * K * n + L * K * N * m;
}

// 5D array
int iiiii(int m,int n,int k,int l,int p,int M,int N,int K,int L,int P)
{
    if ((m<0  )||(n<0  )||(k<0  )||(l<0  )||(p<0  )) {return -1; std::cout<<"error -1"<<std::endl;}
    if ((m>M-1)||(n>N-1)||(k>K-1)||(l>L-1)||(p>P-1)) {return -2; std::cout<<"error -2"<<std::endl;}
    return p + P * l + P * L * k + P * L * K * n + P * L * K * N * m;
}

// Normal components by three points of triangle
dvec norm(dvec & v1, dvec & v2, dvec & v3)
{
    double a1, a2, a3, b1, b2, b3;
    dvec norm(3);

    a1 = v2[0] - v1[0];
    a2 = v2[1] - v1[1];
    a3 = v2[2] - v1[2];

    b1 = v3[0] - v1[0];
    b2 = v3[1] - v1[1];
    b3 = v3[2] - v1[2];

    norm[0] =  a2*b3-a3*b2;
    norm[1] =  a3*b1-a1*b3;
    norm[2] =  a1*b2-a2*b1;

    double len = std::sqrt( norm[0]*norm[0] + norm[1]*norm[1] + norm[2]*norm[2] );
    norm[0] = norm[0] / len;
    norm[1] = norm[1] / len;
    norm[2] = norm[2] / len;
    return norm;
}

double area(dvec & v1, dvec & v2, dvec & v3)
{
    double a1, a2, a3, b1, b2, b3, c1, c2, c3;
    double area(3);

    a1 = v2[0] - v1[0];
    a2 = v2[1] - v1[1];
    a3 = v2[2] - v1[2];

    b1 = v3[0] - v1[0];
    b2 = v3[1] - v1[1];
    b3 = v3[2] - v1[2];

    c1 =  a2*b3-a3*b2;
    c2 =  a3*b1-a1*b3;
    c3 =  a1*b2-a2*b1;

    area = std::sqrt(c1*c1 + c2*c2 + c3*c3) / 2.;

    return area;
}
