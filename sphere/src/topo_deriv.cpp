#include "topo_deriv.h"

void topo_deriv( vec & trg_value, vec & deriv, double G, double nu)
{
    //Calculation of the topological derivatives

    double s_dot, s_tr;
    double E; E= 2*G*(1+nu);

    for (int t = 0; t<trg_value.size()/9; t++)
    {
        s_dot = 0; for (int i = 0; i<9; i++) {s_dot += trg_value[9*t+i] * trg_value[9*t+i];}
        s_tr = trg_value[9*t+0] + trg_value[9*t+4] + trg_value[9*t+8];

        deriv[t] =  ( (3.0 * (1.0 - nu))/(4.0 * E * (7.0 - 5.0 * nu)) ) *
                ( 10.0 * (1.0 + nu) * s_dot - (1.0 + 5.0 * nu) * s_tr*s_tr ) ;
    }
}
