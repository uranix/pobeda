// Test of our BIE solver for external problems
// Spherical cavity in an infinite elastic medium

//----------------------------
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <ctime>
#include <pvfmm.hpp>
#include <utils.hpp>
#include "bvp.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"
#include "fmm_summation.h"
#include "matvec.h"
#include "solver_ksp.h"
#include "topo_deriv.h"
#include "stl_io.h"
//============================

int N_iter;

typedef std::vector<double> dvec;
typedef std::vector<int> ivec;


int main(int argc, char **argv){

  double hole_rad = 0.3;

  N_iter = 0; // GMRES iteration counter
  // Prescribe the homog strain tensor (Voight conv)
  // (e_xx, e_yy, e_zz, g_xy, g_yz, g_yz)
  dvec eps(6); for (int i=0; i<6; i++) eps[i] = 0;
  eps[0] = 0.1; // 10% strain e_xx

  // Prescribe the homog stress tensor (Voight conv)
  // (s_xx, s_yy, s_zz, t_xy t_yz, t_yz)
  dvec sig(6); for (int i=0; i<6; i++) sig[i] = 0;
  sig[0] = 0.1;


  MPI_Init(&argc, &argv);
  MPI_Comm comm=MPI_COMM_WORLD;

  //--------Read command line options-----------------------------------------------------------------------------------------------------------------------
  commandline_option_start(argc, argv, "\
    This example demonstrates solving a particle N-body problem,\n\
    with Laplace Gradient kernel, using the PvFMM library.\n");

  commandline_option_start(argc, argv);
  omp_set_num_threads( atoi(commandline_option(argc, argv,  "-omp",     "1", false, "-omp  <int> = (1)    : Number of OpenMP threads."          )));
  size_t   N=(size_t)strtod(commandline_option(argc, argv,    "-N",     "1",  true, "-N    <int>          : Number of source and target points."),NULL);
  int      m =      strtoul(commandline_option(argc, argv,    "-m",    "10", false, "-m    <int> = (10)   : Multipole order (+ve even integer)."),NULL,10);
  commandline_option_end(argc, argv);
  pvfmm::Profile::Enable(false);
  //========================================================================================================================================================


  // Save analytical solution
  write_anal_solution(hole_rad, N, 0.1, 0.3);
  //return 0;



  // --------Set up serial mesh------------------------------------
  int N_trg = N * N * 24;
  dvec vert_1( N_trg * 3);
  dvec vert_2( N_trg * 3);
  dvec vert_3( N_trg * 3);
  dvec tr_di(  N_trg * 3);
  ivec bc_type( N_trg );
  set_sph_bvp(N, vert_1, vert_2, vert_3, bc_type, tr_di, hole_rad, 0.1);
  //===============================================================


  ///----SAVE CONFIGURATION--------------------------------------
  write_stl1("./CONF/Sphere", vert_1, vert_2, vert_3);
  ///============================================================
  //return 0;



  //---------Distrubute mesh over processes---------------------
  int n_proc; // Number of MPI threads
  int i_proc; // index of this MPI thread
  MPI_Comm_size(comm, &n_proc);
  MPI_Comm_rank(comm, &i_proc);



  dvec test(N_trg); for (int i = 0; i<N_trg; i++) test[i] = i;
  dvec test3(3*N_trg); for (int i = 0; i<N_trg; i++) { test3[3*i] = i; test3[3*i+1] = i; test3[3*i+2] = i;}

  int start, end, span;
  int step;
  if (n_proc == 1) step = N_trg;
  if (n_proc > 1) step = (int) std::floor(N_trg / (double) (n_proc-1.));


  start = step * i_proc;
  end   = step *(i_proc+1);
  if (i_proc == n_proc-1) end = N_trg;
  span = end - start;

  for (int i = 0; i<span; i++)
  {
      // single component vectors
      test[i] = test[start+i];
      bc_type[i] = bc_type[start+i];

      // tree component vectors
      for (int k=0;k<3;k++)
      {
          test3[3*i+k] = test3[3*start+3*i+k];
          vert_1[3*i+k] = vert_1[3*start+3*i+k];
          vert_2[3*i+k] = vert_2[3*start+3*i+k];
          vert_3[3*i+k] = vert_3[3*start+3*i+k];
          tr_di[3*i+k] = tr_di[3*start+3*i+k];
      }
  }
  test.resize(span);
  bc_type.resize(span);
  test3.resize(3*span);
  vert_1.resize(3*span);
  vert_2.resize(3*span);
  vert_3.resize(3*span);
  tr_di.resize(3*span);
  //==========================================================




  double t1, t2;
  t1 = MPI_Wtime();

  std::cout<<"  tr_di  "<<std::endl;
  for (int i = 0; i<tr_di.size();i++)
  {std::cout<<tr_di[i]<<std::endl;}

  // Cash vector with knowns
  dvec buff(tr_di.size());
  for (int i = 0; i<tr_di.size(); i++)
      buff[i] = tr_di[i];

  cout.precision(9);

  //---SOLVE THE SURFACE PROBLEM WITH PETSC GMRES SOLVER------
  SolverFmmGmres * slvr = new SolverFmmGmres();
  slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di, eps);
  slvr->solve(m, comm, 1000, 1e-3);
  delete slvr; slvr = nullptr;
  //==========================================================

  // Sort the obtained solution onto tractions and displacements
  dvec tractions(buff.size()), displacements(buff.size());

  for (int i = 0; i<bc_type.size();i++)
  {
      if (bc_type[i] == 1)
      {
          for (int k = 0; k<3; k++)
          {    tractions[3*i+k] =  buff[3*i+k];
           displacements[3*i+k] = tr_di[3*i+k];}
      }
      if (bc_type[i] == 2)
      {
          for (int k = 0; k<3; k++)
          {    tractions[3*i+k] = tr_di[3*i+k];
           displacements[3*i+k] =  buff[3*i+k];}
      }
  }

  for (int i = 0; i<tractions.size()/3; i++)
  {std::cout<<"I_PROC="<<i_proc<<" , tractions ["<<i<<"] = ("<<tractions[3*i+0]<<" , "<<tractions[3*i+1]<<" , "<<tractions[3*i+2]<<")"<<std::endl;}

  for (int i = 0; i<displacements.size()/3; i++)
  {std::cout<<"I_PROC="<<i_proc<<" , displacements ["<<i<<"] = ("<<displacements[3*i+0]<<" , "<<displacements[3*i+1]<<" , "<<displacements[3*i+2]<<")"<<std::endl;}

  // return 0;
  // This guy must be created
  // and distributed along processes
  dvec coordinates(N*N*N*3);

  int d = 0;
  for (int m = 0; m<N; m++){
      for (int n = 0; n<N; n++){
          for (int k = 0; k<N; k++){
              coordinates[3*d+0] = 1.0/N*m+1.0/(2.0*N);
              coordinates[3*d+1] = 1.0/N*n+1.0/(2.0*N);
              coordinates[3*d+2] = 1.0/N*k+1.0/(2.0*N);
              d++;
          }
      }
  }

  for (int i = 0; i<coordinates.size()/3; i++)
  {std::cout<<" coordinates ["<<i<<"] = ("<<coordinates[3*i+0]<<" , "<<coordinates[3*i+1]<<" , "<<coordinates[3*i+2]<<")"<<std::endl;}


  dvec topo_derivs(coordinates.size()/3);
  dvec stresses(coordinates.size()*3);


  if (i_proc) coordinates.resize(0);

  // Solve for stresses
  volume_matvec(vert_1,vert_2,vert_3,
                     coordinates,tractions,
                     displacements,stresses, m, comm);




  cout.precision(2);
  // Table of stresses
  if (!i_proc) std::cout<<"             TABLE OF STRESSES               "<<std::endl;
  if (!i_proc) std::cout<<"_______________________________________________________________________________________________________________________________________"<<std::endl;
  if (!i_proc) std::cout<<"s11\t\t"<<"s12\t\t"<<"s13\t\t"<<"s21\t\t"<<"s22\t\t"<<"s23\t\t"<<"s31\t\t"<<"s32\t\t"<<"s33\t\t"<<std::endl;
  if (!i_proc) std::cout<<"_______________________________________________________________________________________________________________________________________"<<std::endl;

  for (int i = 0; i<stresses.size()/9; i++)
  {
      if (!i_proc) std::cout<<stresses[9*i+0]<<"\t\t"<<stresses[9*i+1]<<"\t\t"<<stresses[9*i+2]<<"\t\t"<<stresses[9*i+3]<<"\t\t"<<stresses[9*i+4]<<"\t\t"<<stresses[9*i+5]<<"\t\t"<<stresses[9*i+6]<<"\t\t"<<stresses[9*i+7]<<"\t\t"<<stresses[9*i+8]<<"\t\t"<<std::endl;
  }
  cout.precision(8);

  // Form and save the array of s_zz stresses
  dvec stress_zz(stresses.size()/9);
  for (int i = 0; i<stress_zz.size(); i++)
      stress_zz[i] = - stresses[9*i+8]+sig[0];

  // Cut out stresses inside the void
  double X, Y, Z, Rad;
  for (int i = 0; i<coordinates.size()/3; i++)
  {
      X = coordinates[3*i+0];
      Y = coordinates[3*i+1];
      Z = coordinates[3*i+2];
      Rad = std::sqrt( (X-0.5)*(X-0.5) + (Y-0.5)*(Y-0.5) + (Z-0.5)*(Z-0.5));
      if (Rad < hole_rad) stress_zz[i] = 0.;
  }


  // Save slice of stress components

  write_td_slice("TD/stress_zz", stress_zz, N, N, N, 1, (int) floor(N/2.));

  // Solve for TDs
  //topo_deriv( stresses, topo_derivs, 1.0, 0.3);
  //if (!i_proc) std::cout<<"TOPOLOGICAL DERIVATIVES"<<std::endl;
  //for (int i = 0; i<topo_derivs.size(); i++) std::cout<<"At ("<<coordinates[3*i]<<", "<<coordinates[3*i+1]<<", "<<coordinates[3*i+2]<<") - "<<topo_derivs[i]<<std::endl;

  t2 = MPI_Wtime();
  printf( "MPI time  %f\n", t2 - t1 );

  // Shut down MPI
  MPI_Finalize();


  return 0;

}
