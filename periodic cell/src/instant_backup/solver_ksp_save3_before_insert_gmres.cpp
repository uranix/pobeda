#include <iostream>
#include <petscksp.h>
#include <petsctao.h>
#include "solver_ksp.h"
#include "bvp.h"
#include "matvec.h"

#include "mpi.h"
#include "auxillary.h"
#include "singular_integrals.h"
#include "elastic_kernels.h"
//#include "fmm_summation.h"


using ivec = std::vector<int>;
using dvec = std::vector<double>;

using  std::cout;
using  std::endl;

///////////////////////////////////////////////////////////////////////////////

PetscErrorCode apply_Op(Mat opA, Vec vx_in, Vec Ax_out) {

    void * ctx = 0;
    MatShellGetContext(opA, &ctx);
    MatrInfo * matr_ctx = (MatrInfo *) ctx;

    cout << "context goes...  " << endl; // << matr_ctx->DIM << endl;

    PetscScalar * Av_arr;
    VecGetArray(Ax_out, &Av_arr);

    const PetscScalar * v_arr_xread;
    VecGetArrayRead(vx_in, &v_arr_xread);

    PetscInt x_sz;
    VecGetLocalSize(vx_in, &x_sz);

    matvec(v_arr_xread, Av_arr, x_sz,
           *matr_ctx->vert_1_, *matr_ctx->vert_2_, *matr_ctx->vert_3_,
           *matr_ctx->bc_type_, matr_ctx->mult_order, matr_ctx->comm);


    // Backward setup of the original matvec into the Av_out
    const int N_trg = 24 * matr_ctx->N_vox * matr_ctx->N_vox;
    const int span = local_span(N_trg, matr_ctx->comm);
    int loc_ind[3 * span];
    setup_local_indxs(loc_ind,matr_ctx->N_vox, matr_ctx->comm);
    VecSetValues(Ax_out, x_sz, loc_ind, Av_arr, INSERT_VALUES);
    VecAssemblyBegin(Ax_out);
    VecAssemblyEnd(Ax_out);

    return 0;
}

///////////////////////////////////////////////////////////////////////////////

void setup_local_indxs(PetscInt * loc_ind_out, const int Nvox, MPI_Comm comm_mpi) {


    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm_mpi, &n_proc);
    MPI_Comm_rank(comm_mpi, &i_proc);

    //---Very weird way to init petsc vector - but it works -------
    int N_trg = 24 * Nvox * Nvox;
    int step = (int) std::floor(N_trg / (double) (n_proc - 1.));
    const int start = step *  i_proc;
    int end         = step * (i_proc + 1);
    if (i_proc == n_proc-1) end = N_trg;
    const int span = end - start;

    for (int i = 0; i < span; i++) {
        for (int k = 0; k < 3; k++) {
            loc_ind_out[3 * i + k] = 3 * (start + i) + k;
        }
    }
}

int  local_span(const int N_trg, MPI_Comm comm_mpi) {

    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm_mpi, &n_proc);
    MPI_Comm_rank(comm_mpi, &i_proc);

    //---Very weird way to init petsc vector - but it works -------

    int step = (int) std::floor(N_trg / (double) (n_proc - 1.));
    const int start = step *  i_proc;
    int end         = step * (i_proc + 1);
    if (i_proc == n_proc-1) end = N_trg;
    return (end - start);
}

///////////////////////////////////////////////////////////////////////////////

SolverFmmGmres::SolverFmmGmres():
matr_ctx_(),
SRC_PER_TRG_(16),
DIM_(3),
SURF_DIM_(6),
N_vox_(-1),
vert_1_(nullptr),
vert_2_(nullptr),
vert_3_(nullptr),
bc_type_(nullptr),
tract_(nullptr),
displ_(nullptr),
tr_di_(nullptr),
max_pts_(100),
pvfmm_tree_(nullptr) {

//    cout << "Solver Fmm Gmres" << endl;
}

SolverFmmGmres::~SolverFmmGmres() {

    vert_1_ = nullptr;
    vert_2_ = nullptr;
    vert_3_ = nullptr;
    bc_type_ = nullptr;
    tract_ = nullptr;
    displ_ = nullptr;
    tr_di_ = nullptr;

    //  !!!
    // deallocate pvfmm tree.
    //if (pvfmm_tree_) delete pvfmm_tree_;
}

bool SolverFmmGmres::init(const int Nvox_ext, const dvec & vert_1_ext, const dvec & vert_2_ext,
                          const dvec & vert_3_ext, const ivec & bc_type_ext,
                          dvec & tr_di_ext, const int m_MultOrder, MPI_Comm comm_mpi) {
    N_vox_  = Nvox_ext;
    vert_1_ = &vert_1_ext;
    vert_2_ = &vert_2_ext;
    vert_3_ = &vert_3_ext;
    bc_type_ = &bc_type_ext;
    tr_di_ = &tr_di_ext;

//    init_matvec(
//                 *vert_1_,
//                 *vert_2_,
//                 *vert_3_,
//                 *bc_type_,
//                 m_MultOrder,
//                 comm_mpi);

    return true;
}

int SolverFmmGmres::solve(const int m_MultOrder, MPI_Comm comm_mpi, const int max_Iter, const double tol) {

    cout << "Surface solution Solver starts... " << endl;

    //pvfmm_tree_ = PtFMM_CreateTree(src_coord, src_value, surf_coord, surf_value, trg_coord, comm, max_pts, pvfmm::FreeSpace);

    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm_mpi, &n_proc);
    MPI_Comm_rank(comm_mpi, &i_proc);


    // TO DO: refactorize the b_rhs as follows:
    //    dvec * const b_rhs = tr_di_;
    //    dvec tmp_rhs_b(tr_di_->size());

    //dvec b_rhs(*tr_di_);
    dvec b_rhs(tr_di_->size());

    // 1. Build rhs. (vector b)
    calc_rhs_b_(*tr_di_, b_rhs, m_MultOrder, comm_mpi);
//    first_matvec_(*tr_di_, b_rhs, m_MultOrder, comm_mpi);
    // now we have b_rhs for matvecs
//        for (int i = 0; i < b_rhs.size();i++) {
//            std::cout << "rhs[" << i << ", "<< i_proc << "]= "<<b_rhs[i]<<std::endl;
//        }

//    dvec Uno(tr_di_->size());
//    for (int i = 0; i < tr_di_->size(); ++i) {
//        Uno[i] = 1.0;
//    }

//    PetscScalar rzlt[tr_di_->size()];

//    matvec(Uno.data(), rzlt, tr_di_->size(),
//           *vert_1_, *vert_2_, *vert_3_,
//           *bc_type_, m_MultOrder, comm_mpi);

//    for (int i = 0; i < b_rhs.size();i++) {
//        std::cout << "rzlt[" << i << ", "<< i_proc << "]= "<< rzlt[i]<<std::endl;
//    }



    // 1.2 Initialize vectors
    Vec            x, b;      /* approx solution, RHS, exact solution */

    PetscInitialize(0, 0, (char*)0, 0);

    PetscErrorCode ierr;
    VecCreate(comm_mpi, &x);
    ierr = VecSetSizes(x, b_rhs.size(), PETSC_DECIDE);
    CHKERRQ(ierr);
    cout << "ierr = " << ierr << endl;

    VecSetFromOptions(x);

    //--- Init petsc vector ---
    const int N_trg = 24*N_vox_*N_vox_;
    const int span = local_span(N_trg, comm_mpi);
    int loc_ind[3 * span];
    setup_local_indxs(loc_ind, N_vox_, comm_mpi);

//    VecSetValues(x, b_rhs.size(), loc_ind, b_rhs.data(), INSERT_VALUES);
    VecSet(x, 1.0);
    VecAssemblyBegin(x);
    VecAssemblyEnd(x);
    //===========================================================

//    VecView(x, PETSC_VIEWER_STDOUT_WORLD);

    // PETSc RHS
    ierr = VecDuplicate(x, &b);CHKERRQ(ierr);
    VecCopy(x,b);
//    VecView(b, PETSC_VIEWER_STDOUT_WORLD);

    // 2. Setup matrix (shell).
    Mat            A;            /* linear system matrix */

    // Pass the context to the matvec implementation
    matr_ctx_.vert_1_ = vert_1_;
    matr_ctx_.vert_2_ = vert_2_;
    matr_ctx_.vert_3_ = vert_3_;
    matr_ctx_.bc_type_ = bc_type_;
    matr_ctx_.tr_di_ = tr_di_;
    matr_ctx_.comm = comm_mpi;
    matr_ctx_.mult_order = m_MultOrder;
    matr_ctx_.N_vox = N_vox_;

    MatCreateShell(comm_mpi, b_rhs.size(), b_rhs.size(), PETSC_DETERMINE, PETSC_DETERMINE, &matr_ctx_, &A);
    MatShellSetOperation(A, MATOP_MULT, (void(*)(void)) apply_Op);
    MatMult(A,x,b);
    VecView(x, PETSC_VIEWER_STDOUT_WORLD);
    cout << "=====" << endl;
    VecView(b, PETSC_VIEWER_STDOUT_WORLD);


    cout << "start gmres..." << endl;
    // 2.     call GMRes
    KSP    ksp_solver;    /* linear solver context */
    PC     prec;     /* preconditioner context */

    KSPCreate(comm_mpi, &ksp_solver);
    KSPSetOperators(ksp_solver, A,A);
    KSPSetType(ksp_solver, KSPGMRES);
    KSPSetInitialGuessNonzero(ksp_solver,PETSC_TRUE);

    KSPSetFromOptions(ksp_solver);
    KSPSetUp(ksp_solver);


    // View solver info;
    //KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);

    // b = right-hand side, x - solution
    KSPSolve(ksp_solver, b, x);

    PetscInt its;
    KSPConvergedReason reason;
    KSPGetConvergedReason(ksp_solver,&reason);
    if (reason==KSP_DIVERGED_INDEFINITE_PC) {
        PetscPrintf(comm_mpi,"\nDivergence because of indefinite preconditioner;\n");
        PetscPrintf(comm_mpi,"Run the executable again but with '-pc_factor_shift_type POSITIVE_DEFINITE' option.\n");
   } else if (reason < 0) {
        PetscPrintf(comm_mpi,"\nOther kind of divergence: this should not happen.\n");
   } else {
        KSPGetIterationNumber(ksp_solver,&its);
   }

    KSPView(ksp_solver,PETSC_VIEWER_STDOUT_WORLD);

    KSPDestroy(&ksp_solver);

    // Finalization: free all petsc data
    VecDestroy(&x);
    PetscFinalize();

    return 0;
}

//    first_matvec_(*tr_di_, b_rhs, m_MultOrder, comm);
//    matvec_(*tr_di_, b_rhs, m_MultOrder, comm);

//    for (int i = 0; i<b_rhs.size(); i++)
//    {std::cout<<i_proc<<" - "<<"rhs["<<i<<"]="<<b_rhs[i]<<std::endl;}




                                   //const PetscScalar * v_inp
void SolverFmmGmres::first_matvec_(const dvec & v_inp, dvec & Av_out,
                                 const int mult_order, MPI_Comm comm)
{

//    for (i...) {
//        aaa[i] += v_inp[i] - 224;
//    }


    // This function performs multiplication A*v for a given v

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    // Temporary references.
    const dvec & vert_1 = *vert_1_;
    const dvec & vert_2 = *vert_2_;
    const dvec & vert_3 = *vert_3_;
    const ivec & bc_t = *bc_type_;

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt ++;
        if (bc_t[i]==2) src_cnt ++;
    }
    src_cnt *=SRC_PER_TRG_; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG_;

    dvec src_coord(DIM_*src_cnt);
    dvec src_value(DIM_*src_cnt);
    dvec surf_coord(DIM_*surf_cnt);
    dvec surf_value(SURF_DIM_*surf_cnt);
    dvec trg_coord(DIM_ * trg_cnt);
    dvec trg_value(DIM_ * trg_cnt);

    // Setting up FMM arrays

    for (int i = 0; i < DIM_*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM_*SRC_PER_TRG_;
    int bv_size = SURF_DIM_*SRC_PER_TRG_;
    dvec sbuff_coord(bc_size);
    dvec sbuff_src_v(bc_size);
    dvec sbuff_surf_v(bv_size);

    dvec v1(DIM_), v2(DIM_), v3(DIM_), val(DIM_);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM_; k++)
        {
            v1[k] = vert_1[i*DIM_+k];
            v2[k] = vert_2[i*DIM_+k];
            v3[k] = vert_3[i*DIM_+k];
            val[k] = v_inp[i*DIM_+k];
        }

        if (bc_t[i]==1)
        {
            triangle2src_(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            triangle2src_(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }

//    int n_proc; // Number of MPI threads
//    int i_proc; // index of this MPI thread
//    MPI_Comm_size(comm, &n_proc);
//    MPI_Comm_rank(comm, &i_proc);

//    for (int i = 0; i<src_coord.size(); i++)
//        std::cout<<i_proc<<" - "<<src_coord[i]<<std::endl;



//    direct_summation_displacement(src_coord,
//                                  src_value,
//                                  surf_coord,
//                                  surf_value,
//                                  trg_coord,
//                                  trg_value);

    // Handmade PVFMM is called here. Is declared in fmm_summation where genuine pvfmm is called.
    calculate_displacements_(
            src_coord,
            src_value,
            surf_coord,
            surf_value,
            trg_coord,
            trg_value,
            mult_order,
            comm);

    for (int i = 0; i < DIM_ * trg_cnt; i++) Av_out[i] = trg_value[i];

    //=========================================================
    // 2) LOCAL pass
    //=========================================================

    // a) add (1/2)*delta_ij * u_j = 1/2 * u_i

    for (int i = 0; i < trg_cnt; i++)
    { if (bc_t[i]==1) {for (int k = 0; k<DIM_; k++) Av_out[DIM_ * i + k] += 0.5 * v_inp[DIM_ * i + k];}}

    // b) Subtract-add cycle
    dvec add(DIM_); dvec subtr(DIM_);
    for (int i = 0; i < trg_cnt; i++)
    {

        for (int k = 0; k<DIM_; k++)
        {
            v1[k] = vert_1[i*DIM_+k];
            v2[k] = vert_2[i*DIM_+k];
            v3[k] = vert_3[i*DIM_+k];
            val[k] = v_inp[i*DIM_+k];
        }
        local_pass_( subtr, add, v1, v2, v3, 3-bc_t[i], val);
        for (int k = 0; k < DIM_; k++)
        {
            if (bc_t[i]==2)
            { subtr[k] = - subtr[k]; add[k] = - add[k]; } // U matr comes with minus sign
            Av_out[DIM_*i+k] -= subtr[k]; Av_out[DIM_*i+k] += add[k];
        }
    }
    for (int i = 0; i<Av_out.size(); i++){Av_out[i] = -Av_out[i];}
}

void SolverFmmGmres::matvec_(const dvec & v_inp, dvec & Av_out,
                                 const int mult_order, MPI_Comm comm)
{
    // This function performs multiplication A*v for a given v

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    // Temporary references.
    const dvec & vert_1 = *vert_1_;
    const dvec & vert_2 = *vert_2_;
    const dvec & vert_3 = *vert_3_;
    const ivec & bc_t = *bc_type_;

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt ++;
        if (bc_t[i]==2) src_cnt ++;
    }
    src_cnt *=SRC_PER_TRG_; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG_;

    dvec src_coord(DIM_*src_cnt);
    dvec src_value(DIM_*src_cnt);
    dvec surf_coord(DIM_*surf_cnt);
    dvec surf_value(SURF_DIM_*surf_cnt);
    dvec trg_coord(DIM_ * trg_cnt);
    dvec trg_value(DIM_ * trg_cnt);



    // Setting up FMM arrays

    for (int i = 0; i < DIM_*trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM_*SRC_PER_TRG_;
    int bv_size = SURF_DIM_*SRC_PER_TRG_;
    dvec sbuff_coord(bc_size);
    dvec sbuff_src_v(bc_size);
    dvec sbuff_surf_v(bv_size);

    dvec v1(DIM_), v2(DIM_), v3(DIM_), val(DIM_);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM_; k++)
        {
            v1[k] = vert_1[i*DIM_+k];
            v2[k] = vert_2[i*DIM_+k];
            v3[k] = vert_3[i*DIM_+k];
            val[k] = v_inp[i*DIM_+k];
        }

        if (bc_t[i]==1)
        {
            triangle2src_(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            triangle2src_(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }

//    int n_proc; // Number of MPI threads
//    int i_proc; // index of this MPI thread
//    MPI_Comm_size(comm, &n_proc);
//    MPI_Comm_rank(comm, &i_proc);

//    for (int i = 0; i<src_coord.size(); i++)
//        std::cout<<i_proc<<" - "<<src_coord[i]<<std::endl;



//    direct_summation_displacement(src_coord,
//                                  src_value,
//                                  surf_coord,
//                                  surf_value,
//                                  trg_coord,
//                                  trg_value);

    // Handmade PVFMM is called here. Is declared in fmm_summation where genuine pvfmm is called.

    re_calculate_displacements_(
    //calculate_displacements_(
            src_coord,
            src_value,
            surf_coord,
            surf_value,
            trg_coord,
            trg_value,
            mult_order,
            comm);

    for (int i = 0; i < DIM_ * trg_cnt; i++) Av_out[i] = trg_value[i];

    //=========================================================
    // 2) LOCAL pass
    //=========================================================

    // a) add (1/2)*delta_ij * u_j = 1/2 * u_i

    for (int i = 0; i < trg_cnt; i++)
    { if (bc_t[i]==1) {for (int k = 0; k<DIM_; k++) Av_out[DIM_ * i + k] += 0.5 * v_inp[DIM_ * i + k];}}

    // b) Subtract-add cycle
    dvec add(DIM_); dvec subtr(DIM_);
    for (int i = 0; i < trg_cnt; i++)
    {

        for (int k = 0; k<DIM_; k++)
        {
            v1[k] = vert_1[i*DIM_+k];
            v2[k] = vert_2[i*DIM_+k];
            v3[k] = vert_3[i*DIM_+k];
            val[k] = v_inp[i*DIM_+k];
        }
        local_pass_( subtr, add, v1, v2, v3, 3-bc_t[i], val);
        for (int k = 0; k < DIM_; k++)
        {
            if (bc_t[i]==2)
            { subtr[k] = - subtr[k]; add[k] = - add[k]; } // U matr comes with minus sign
            Av_out[DIM_*i+k] -= subtr[k]; Av_out[DIM_*i+k] += add[k];
        }
    }
    for (int i = 0; i<Av_out.size(); i++){Av_out[i] = -Av_out[i];}
}


void SolverFmmGmres::calc_rhs_b_(const dvec & v_inp, dvec & Av_out,
                                 const int mult_order, MPI_Comm comm) {

    // This function performs multiplication BB*bc-s and generates rhs

    //=========================================================
    // 1) FMM pass
    //=========================================================

    // For matvec:
    // bc_t = 1 (tractions are given) means: x = displacements (surf_value)
    // bc_t = 2 (displacem are given) means: x = tractions (src_value)

    // Temporary references.
    const dvec & vert_1 = *vert_1_;
    const dvec & vert_2 = *vert_2_;
    const dvec & vert_3 = *vert_3_;
    const ivec & bc_te = *bc_type_;

    ivec bc_t(bc_te.size());
    for (int i = 0; i<bc_t.size(); i++) bc_t[i] = 3 - bc_te[i]; // FMM pass for RHS requires inversion of BCs

    int trg_cnt = bc_t.size();
    int src_cnt=0, surf_cnt=0;

    for (int i = 0; i < trg_cnt; i++)
    {
        if (bc_t[i]==1) surf_cnt ++;
        if (bc_t[i]==2) src_cnt ++;
    }
    src_cnt *=SRC_PER_TRG_; // 16 sources per triangle
    surf_cnt *=SRC_PER_TRG_;
    dvec src_coord(DIM_ * src_cnt);
    dvec src_value(DIM_ * src_cnt);
    dvec surf_coord(DIM_ * surf_cnt);
    dvec surf_value(SURF_DIM_ * surf_cnt);
    dvec trg_coord(DIM_ * trg_cnt);
    dvec trg_value(DIM_ * trg_cnt);

    // Setting up FMM arrays

    for (int i = 0; i < DIM_ * trg_cnt; i++) trg_coord[i] = (1./3.) * (vert_1[i] + vert_2[i] + vert_3[i]);
    int s = 0, su = 0; // Numbers of src and surf sources
    int bc_size = DIM_ * SRC_PER_TRG_;
    int bv_size = SURF_DIM_ * SRC_PER_TRG_;
    dvec sbuff_coord(bc_size);
    dvec sbuff_src_v(bc_size);
    dvec sbuff_surf_v(bv_size);

    dvec v1(DIM_), v2(DIM_), v3(DIM_), val(DIM_);

    for (int i = 0; i<trg_cnt; i++)
    {
        for (int k = 0; k<DIM_; k++)
        {
            v1[k] = vert_1[i*DIM_+k];
            v2[k] = vert_2[i*DIM_+k];
            v3[k] = vert_3[i*DIM_+k];
            val[k] = v_inp[i*DIM_+k];
        }

        if (bc_t[i]==1)
        {
            triangle2src_(2, v1,v2,v3, sbuff_coord, sbuff_surf_v, val);
            for (int m = 0; m < bc_size;m++) surf_coord[bc_size*su + m] = sbuff_coord[m];
            for (int m = 0; m < bv_size;m++) surf_value[bv_size*su + m] = sbuff_surf_v[m];
            su ++;
        }
        if (bc_t[i]==2)
        {
            triangle2src_(1, v1,v2,v3,sbuff_coord, sbuff_src_v, val);
            for (int m = 0; m < bc_size;m++)  src_coord[bc_size*s + m] = sbuff_coord[m];
            for (int m = 0; m < bc_size;m++)  src_value[bc_size*s + m] = - sbuff_src_v[m]; // U matr comes with minus sign
            s ++;
        }
    }

//    int n_proc; // Number of MPI threads
//    int i_proc; // index of this MPI thread
//    MPI_Comm_size(comm, &n_proc);
//    MPI_Comm_rank(comm, &i_proc);

//    for (int i = 0; i<src_coord.size();i++)
//        std::cout<<"Process #"<<i_proc<<" ,"<<"src_coord["<<i<<"]="<<src_coord[i]<<std::endl;

    calculate_displacements_(src_coord, src_value, surf_coord, surf_value,
                             trg_coord, trg_value, mult_order, comm);

    for (int i = 0; i < DIM_ * trg_cnt; i++) Av_out[i] = trg_value[i];

    //=========================================================
    // 2) LOCAL pass
    //=========================================================

    // a) add (1/2)*delta_ij * u_j = 1/2 * u_i

    for (int i = 0; i < trg_cnt; i++) {
        if (bc_t[i]==1) {
            for (int k = 0; k<DIM_; k++) Av_out[DIM_ * i + k] += 0.5 * v_inp[DIM_ * i + k];
        }
    }

    // b) Subtract-add cycle
    dvec add(DIM_);
    dvec subtr(DIM_);
    for (int i = 0; i < trg_cnt; i++)
    {

        for (int k = 0; k<DIM_; k++)
        {
            v1[k] = vert_1[i*DIM_+k];
            v2[k] = vert_2[i*DIM_+k];
            v3[k] = vert_3[i*DIM_+k];
            val[k] = v_inp[i*DIM_+k];
        }
        local_pass_(subtr, add, v1, v2, v3, 3-bc_t[i], val);

        for (int k = 0; k < DIM_; k++)
        {
            if (bc_t[i]==2)
            { subtr[k] = - subtr[k]; add[k] = - add[k]; } // U matr comes with minus sign
            Av_out[DIM_*i+k] -= subtr[k];
            Av_out[DIM_*i+k] += add[k];
        }
    }
}

void SolverFmmGmres::local_pass_(dvec & sub, dvec & add, const dvec & v_1, const dvec & v_2,
                                 const dvec & v_3, const int bc, const dvec & val) {

    const double G  = 1.;
    const double nu = 0.3;

    dvec csi(DIM_);
    for (int i = 0; i < DIM_; i++) {
        csi[i] = (1./3.) * (v_1[i] + v_2[i] + v_3[i]);
    }
    //============================================
    //             Add value
    //============================================

    // (invert bc when using for RHS correction)
    for (int i = 0; i<DIM_; i++) add[i] = 0;
    if (bc==1)
    {
        for (int i = 0; i<DIM_; i++)
        {
            for (int j = 0; j<DIM_; j++)
            {
                add[i] += sint_u(i+1, j+1, v_1, v_2, v_3, csi, G, nu)* val[j];
            }
        }
    }
    if (bc==2)
    {
        for (int i = 0; i<DIM_; i++)
        {
            for (int j = 0; j<DIM_; j++)
            {
                add[i] += sint_p(i+1, j+1, v_1, v_2, v_3, csi, nu)* val[j];
            }
        }
    }

    //for (int i = 0; i<3; i++) {add[i] = 0;}

    //=========================================
    //             Sub value
    //=========================================

    for (int i = 0; i<DIM_; i++) sub[i] = 0;

    int bc_size = DIM_ * SRC_PER_TRG_; // 3 * (sources_per_triangle)
    int bv_size = SURF_DIM_ * SRC_PER_TRG_; // 6 * (sources_per_triangle)

    dvec src_coord(bc_size);
    dvec src_value(bc_size);

    dvec surf_coord(bc_size);
    dvec surf_value(bv_size);

    dvec x(DIM_), d(DIM_), n(DIM_);
    if (bc==1)
    {
        triangle2src_(1, v_1, v_2, v_3, src_coord, src_value, val);

        // Direct summation over src and surf
        for (int s=0; s< src_coord.size()/3; s++)
        {
            x[0] = src_coord[s*DIM_];
            x[1] = src_coord[s*DIM_+1];
            x[2] = src_coord[s*DIM_+2];

            d[0] = src_value[s*DIM_];
            d[1] = src_value[s*DIM_+1];
            d[2] = src_value[s*DIM_+2];

            for (int i = 0; i<DIM_; i++){
                for (int j = 0; j<DIM_; j++){
                    sub[i] += U_ij( i+1, j+1, csi, x, G, nu ) * d[j];
                }
            }
        }
    }

    if (bc==2)
    {
        triangle2src_(2, v_1, v_2, v_3, surf_coord, surf_value, val);

        // Direct summation over src and surf
        for (int s=0; s< src_coord.size()/3; s++)
        {
            x[0] = surf_coord[s*DIM_];
            x[1] = surf_coord[s*DIM_+1];
            x[2] = surf_coord[s*DIM_+2];

            d[0] = surf_value[s*SURF_DIM_];
            d[1] = surf_value[s*SURF_DIM_+1];
            d[2] = surf_value[s*SURF_DIM_+2];

            n[0] = surf_value[s*SURF_DIM_+3];
            n[1] = surf_value[s*SURF_DIM_+4];
            n[2] = surf_value[s*SURF_DIM_+5];

            for (int i = 0; i<DIM_; i++){
                for (int j = 0; j<DIM_; j++){
                    sub[i] += P_ij( i+1, j+1, csi, x, n, nu ) * d[j];
                }
            }
        }
    }
    //for (int i = 0; i<3; i++) {sub[i] = 0;}
}

void SolverFmmGmres::triangle2src_(int mode, const dvec & v_1, const dvec & v_2, const dvec & v_3,
                                   dvec & src_coord, dvec & src_value, const dvec & val) {

    // Normal components by
    // Cubic Gauss quadrature points and weights
    dvec eta_1(4);
    eta_1 = { 1./3., 3./5., 1./5., 1./5. };
    dvec eta_2(4);
    eta_2 = { 1./3., 1./5., 3./5., 1./5. };
    dvec eta_3(4);
    for (int i = 0; i < 4; i++) {
        eta_3[i] = 1. - eta_1[i] - eta_2[i];
    }
    dvec ww_i(4);
    ww_i =  {-9.0/32.0, 25.0/96.0, 25.0/96.0, 25.0/96.0};

    // Split triangle into 4 pieces
    dvec v_12(DIM_);
    for (int i = 0; i < DIM_; i++) {
        v_12[i] = (1./2.) * (v_1[i] + v_2[i]);
    }
    dvec v_13(DIM_);
    for (int i = 0; i < DIM_; i++) {
        v_13[i] = (1./2.) * (v_1[i] + v_3[i]);
    }
    dvec v_23(DIM_);
    for (int i = 0; i < DIM_; i++) {
        v_23[i] = (1./2.) * (v_2[i] + v_3[i]);
    }

    int i = 0; // Point index
    dvec x_1(DIM_);
    dvec x_2(DIM_);
    dvec x_3(DIM_);
    for (int t = 0; t < 4; ++t) // cycle over triangles
    {
        if (t==0) for (int v = 0; v < DIM_; v++) { x_1[v] = v_1[v];  x_2[v] = v_12[v]; x_3[v] = v_13[v];}
        if (t==1) for (int v = 0; v < DIM_; v++) { x_1[v] = v_2[v];  x_2[v] = v_23[v]; x_3[v] = v_12[v];}
        if (t==2) for (int v = 0; v < DIM_; v++) { x_1[v] = v_3[v];  x_2[v] = v_13[v]; x_3[v] = v_23[v];}
        if (t==3) for (int v = 0; v < DIM_; v++) { x_1[v] = v_12[v]; x_2[v] = v_23[v]; x_3[v] = v_13[v];}

        for (int c = 0; c<4; c++ ) // cycle over gauss points
        {
            for (int m = 0; m<DIM_; m++)
            {
                if (mode == 1){
                    src_coord[DIM_*i + m] = x_1[m] * eta_1[c] + x_2[m] * eta_2[c] + x_3[m] * eta_3[c];
                    src_value[DIM_*i + m] = ww_i[c] * 2.0 * area(x_1, x_2, x_3) * val[m];
                }

                if (mode == 2){
                    src_coord[DIM_*i + m] = x_1[m] * eta_1[c] + x_2[m] * eta_2[c] + x_3[m] * eta_3[c];
                    src_value[SURF_DIM_*i + m] = ww_i[c] * 2.0 * area(x_1, x_2, x_3) * val[m];
                    src_value[SURF_DIM_*i + DIM_ + m] = norm(v_1, v_2, v_3)[m];
                }
            }
            i++;
        }
    }
}

void SolverFmmGmres::calculate_displacements_(dvec & src_coord,  dvec & src_value,
                                              dvec & surf_coord, dvec & surf_value,
                                              dvec & trg_coord,  dvec & trg_value_out,
                                              int mult_order, MPI_Comm comm) {

    // Set kernel.
      const pvfmm::Kernel<double>& kernel_fn=ElasticKernel<double>::Disp();
   // const pvfmm::Kernel<double>& kernel_fn=StokesKernel<double>::Velocity();

    // Create memory-manager (optional)
    pvfmm::mem::MemoryManager mem_mgr(10000000);

    // Construct tree.
    pvfmm_tree_ = PtFMM_CreateTree(src_coord, src_value, surf_coord, surf_value,
                                               trg_coord, comm, max_pts_, pvfmm::FreeSpace);

    // Load matrices.
    pvfmm::PtFMM matrices(&mem_mgr);
    matrices.Initialize(mult_order, comm, &kernel_fn);

    // FMM Setup
    pvfmm_tree_->SetupFMM(&matrices);

    // Run FMM
    int n_trg = trg_coord.size()/3;
    PtFMM_Evaluate(pvfmm_tree_, trg_value_out, n_trg);

    pvfmm_tree_->ClearFMMData();

    // Re-run FMM
    PtFMM_Evaluate(pvfmm_tree_, trg_value_out, n_trg, &src_value, &surf_value);


    // Free memory
    //delete tree;
    return;
}


void SolverFmmGmres::re_calculate_displacements_(dvec & src_coord,  dvec & src_value,
                                              dvec & surf_coord, dvec & surf_value,
                                              dvec & trg_coord,  dvec & trg_value_out,
                                              int mult_order, MPI_Comm comm) {




    int n_trg = trg_coord.size()/3;

    // Clean FMM sources and targets
     pvfmm_tree_->ClearFMMData();

    // Re-run FMM
    PtFMM_Evaluate(pvfmm_tree_, trg_value_out, n_trg, &src_value, &surf_value);

    return;
}



///////////////////////////////////////////////////////////////////////////////

// End of the file

