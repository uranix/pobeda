#include <iostream>
#include <iomanip>
#include <petscksp.h>
#include <petsctao.h>

using std::cout;
using std::endl;
using std::setw;


typedef struct {

    int info;

} MatrInfo2;


PetscErrorCode apply_Op_2(Mat opA, Vec vx_in, Vec Ax_out) {

    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
    MPI_Comm_rank(MPI_COMM_WORLD, &i_proc);

    cout << "mat mult goes..." << i_proc << "  of " << n_proc << endl;

    const int nsz = 12;
    double A[nsz * nsz];
    for (int i = 0; i < nsz; ++i) {
        for (int j = 0; j < nsz; ++j) {
            A[i + j * nsz] = i + j + 1;
            A[i + i * nsz] = -2 - i;
//            cout << setw(8) << A[i + j * nsz];
        }
        A[i + i * nsz] = -2 - i;
//        cout << endl;
    }

//    const int n_loc = 6;

    void * ctx = 0;
    MatShellGetContext(opA, &ctx);
    MatrInfo2 * matr_ctx = (MatrInfo2 *) ctx;

    const PetscScalar * v_arr_xread;
    VecGetArrayRead(vx_in, &v_arr_xread);

    PetscInt loc_sz;
    VecGetLocalSize(vx_in, &loc_sz);

    cout << "loc size = " << loc_sz << endl;
    double v_buff[nsz];
    for (int i = 0; i < loc_sz; ++i) {
        double sum = 0;
        for (int k = 0; k < loc_sz; ++k) {
            sum += A[(i + 6*i_proc) + (k + 6 * i_proc) * nsz] * v_arr_xread[k];
        }
        v_buff[i] = sum;
        cout << v_buff[i] << "  ";
    }
    cout << endl;

    int glob_ind[6];
    if (0 == i_proc) {
        int tmp[6] =  {0,1,2,3,4,5};
        for (int k = 0; k < 6; ++k) {glob_ind[k] = tmp[k];}
    }
    if (1 == i_proc) {
        int tmp[6] =  {6,7,8,9,10, 11};
        for (int k = 0; k < 6; ++k) {glob_ind[k] = tmp[k];}
    }

    // Backward setup of the original matvec into the Av_out
    VecSetValues(Ax_out, 6, glob_ind, v_buff, INSERT_VALUES);
    VecAssemblyBegin(Ax_out);
    VecAssemblyEnd(Ax_out);

    return 0;
}

int main(int argc, char * argv[]) {

    cout << "ksp gmres starts" << endl;

    MPI_Init(&argc, &argv);
    MPI_Comm comm_mpi = MPI_COMM_WORLD;

    // 1.2 Initialize vectors
    Vec            x, b;      /* approx solution, RHS, exact solution */
    const int loc_size = 6;

    PetscInitialize(0, 0, (char*)0, 0);

    PetscErrorCode ierr;
    VecCreate(comm_mpi, &x);
    ierr = VecSetSizes(x, loc_size, PETSC_DECIDE);      CHKERRQ(ierr);
    VecSetFromOptions(x);

    VecSet(x, 1.0);
    VecAssemblyBegin(x);
    VecAssemblyEnd(x);

    VecView(x, PETSC_VIEWER_STDOUT_WORLD);

    // PETSc RHS
    ierr = VecDuplicate(x, &b);     CHKERRQ(ierr);
    VecCopy(x,b);
    VecView(b, PETSC_VIEWER_STDOUT_WORLD);

    // 2. Setup matrix (shell).
    Mat        A;
    MatrInfo2  matr_ctx;

    MatCreateShell(comm_mpi, loc_size, loc_size, PETSC_DETERMINE, PETSC_DETERMINE, &matr_ctx, &A);
    MatShellSetOperation(A, MATOP_MULT, (void(*)(void)) apply_Op_2);
//    MatMult(A, x, b);

    cout << "x after Matmult: " << endl;
    VecView(x, PETSC_VIEWER_STDOUT_WORLD);
    cout << "b after Matmult: " << endl;
    VecView(b, PETSC_VIEWER_STDOUT_WORLD);


    cout << "start gmres..." << endl;
    // 2.     call GMRes
    KSP    ksp_solver;    /* linear solver context */
//    PC     prec;     /* preconditioner context */

    KSPCreate(comm_mpi, &ksp_solver);
    KSPSetOperators(ksp_solver, A, A);
    KSPSetType(ksp_solver, KSPGMRES);
//    KSPSetInitialGuessNonzero(ksp_solver,PETSC_TRUE);

    KSPSetFromOptions(ksp_solver);
    KSPSetUp(ksp_solver);


    // View solver info;
//    //KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);

    // b = right-hand side, x - solution
    KSPSolve(ksp_solver, b, x);

//    PetscInt its;
//    KSPConvergedReason reason;
//    KSPGetConvergedReason(ksp_solver,&reason);
//    if (reason==KSP_DIVERGED_INDEFINITE_PC) {
//        PetscPrintf(comm_mpi,"\nDivergence because of indefinite preconditioner;\n");
//        PetscPrintf(comm_mpi,"Run the executable again but with '-pc_factor_shift_type POSITIVE_DEFINITE' option.\n");
//   } else if (reason < 0) {
//        PetscPrintf(comm_mpi,"\nOther kind of divergence: this should not happen.\n");
//   } else {
//        KSPGetIterationNumber(ksp_solver,&its);
//   }

    KSPView(ksp_solver,PETSC_VIEWER_STDOUT_WORLD);

    cout << "x after GMRES: " << endl;
    VecView(x, PETSC_VIEWER_STDOUT_WORLD);
    cout << "b after GMRES: " << endl;
    VecView(b, PETSC_VIEWER_STDOUT_WORLD);

    KSPDestroy(&ksp_solver);
    VecDestroy(&x);
    VecDestroy(&b);
    MatDestroy(&A);
    PetscFinalize();

    MPI_Finalize();

    return 0;
}


// End of the file

