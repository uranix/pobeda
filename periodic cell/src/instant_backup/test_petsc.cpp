#include <iostream>
#include "petscksp.h"

using std::cout;
using std::endl;

static char help[] = "Block Jacobi preconditioner for solving a linear system in parallel with KSP.\n \
        The code indicates the\n \
        procedures for setting the particular block sizes and for using different\n \
        linear solvers on the individual blocks.\n \n";


int main(int argc, char * argv[]) {

    cout << "petsc gmres example starts..." << endl;

    // Petsc vector creation
    Vec    x,b,u;
    Mat            A;            /* linear system matrix */
    KSP            ksp;         /* KSP context */
    KSP            *subksp;     /* array of local KSP contexts on this processor */
    PC             pc;           /* PC context */
    PC             subpc;        /* PC context for subdomain */
    PetscReal      norm;         /* norm of solution error */
    PetscInt       i,j,Ii,J,*blks, m = 4, n;
    PetscMPIInt    rank,size;
    PetscInt       its,nlocal,first,Istart,Iend;
    PetscScalar    v,one = 1.0,none = -1.0;
    PetscBool      isbjacobi, setFlag;

    PetscInitialize(&argc, &argv, (char*)0, help);
    //PetscOptionsGetInt(NULL, "-m", &m, );
    PetscOptionsGetInt(NULL, NULL, "-m", &m, &setFlag);
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    MPI_Comm_size(PETSC_COMM_WORLD,&size);

    n = m + 2;

    /* -------------------------------------------------------------------
        Compute the matrix and right-hand-side vector that define
        the linear system, Ax = b.
    ------------------------------------------------------------------- */

    /* Create and assemble parallel matrix */
    MatCreate(PETSC_COMM_WORLD,&A);
    MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, m*n, m*n);
    MatSetFromOptions(A);
    MatMPIAIJSetPreallocation(A,5,NULL,5,NULL);
    MatSeqAIJSetPreallocation(A,5,NULL);
    MatGetOwnershipRange(A,&Istart,&Iend);
    for (Ii=Istart; Ii<Iend; Ii++) {
        v = -1.0; i = Ii/n; j = Ii - i*n;
        if (i>0)   {J = Ii - n; MatSetValues(A,1,&Ii,1,&J,&v,ADD_VALUES);}
        if (i<m-1) {J = Ii + n; MatSetValues(A,1,&Ii,1,&J,&v,ADD_VALUES);}
        if (j>0)   {J = Ii - 1; MatSetValues(A,1,&Ii,1,&J,&v,ADD_VALUES);}
        if (j<n-1) {J = Ii + 1; MatSetValues(A,1,&Ii,1,&J,&v,ADD_VALUES);}
        v = 4.0; MatSetValues(A,1,&Ii,1,&Ii,&v,ADD_VALUES);
    }
    MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);


    /* Create parallel vectors */
    VecCreate(PETSC_COMM_WORLD,&u);
    VecSetSizes(u,PETSC_DECIDE,m*n);
    VecSetFromOptions(u);
    VecDuplicate(u,&b);
    VecDuplicate(b,&x);

    /* Set exact solution; then compute right-hand-side vector. */
    VecSet(u,one);
    MatMult(A,u,b);

    /* Create linear solver context */
    KSPCreate(PETSC_COMM_WORLD,&ksp);

    /* Set operators. Here the matrix that defines the linear system
       also serves as the preconditioning matrix.  */
    KSPSetOperators(ksp,A,A);

    /* Set default preconditioner for this program to be block Jacobi.
       This choice can be overridden at runtime with the option
       -pc_type <type> */
    KSPGetPC(ksp,&pc);
    PCSetType(pc,PCBJACOBI);


    /* -------------------------------------------------------------------
                        Define the problem decomposition
       ------------------------------------------------------------------- */

    /* Call PCBJacobiSetTotalBlocks() to set individually the size of
       each block in the preconditioner.  This could also be done with
       the runtime option -pc_bjacobi_blocks <blocks>
       Also, see the command PCBJacobiSetLocalBlocks() to set the
       local blocks.
       Note: The default decomposition is 1 block per processor.
    */
    PetscMalloc1(m,&blks);
    for (i=0; i<m; i++) blks[i] = n;
    PCBJacobiSetTotalBlocks(pc,m,blks);
    PetscFree(blks);

    /* -------------------------------------------------------------------
               Set the linear solvers for the subblocks
       ------------------------------------------------------------------- */

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
           Basic method, should be sufficient for the needs of most users.
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      By default, the block Jacobi method uses the same solver on each
      block of the problem.  To set the same solver options on all blocks,
      use the prefix -sub before the usual PC and KSP options, e.g.,
           -sub_pc_type <pc> -sub_ksp_type <ksp> -sub_ksp_rtol 1.e-4
   */

   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Advanced method, setting different solvers for various blocks.
      - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      Note that each block's KSP context is completely independent of
      the others, and the full range of uniprocessor KSP options is
      available for each block. The following section of code is intended
      to be a simple illustration of setting different linear solvers for
      the individual blocks.  These choices are obviously not recommended
      for solving this particular problem.
   */
    PetscObjectTypeCompare((PetscObject)pc,PCBJACOBI,&isbjacobi);
    if (isbjacobi) {
    /* Call KSPSetUp() to set the block Jacobi data structures (including
       creation of an internal KSP context for each block).

       Note: KSPSetUp() MUST be called before PCBJacobiGetSubKSP(). */
    KSPSetUp(ksp);

    /* Extract the array of KSP contexts for the local blocks */
    PCBJacobiGetSubKSP(pc,&nlocal,&first,&subksp);

    /* Loop over the local blocks, setting various KSP options
       for each block.  */
    for (i=0; i<nlocal; i++) {
        KSPGetPC(subksp[i],&subpc);
            if (!rank) {
                if (i%2) {
                    PCSetType(subpc,PCILU);
                } else {
                    PCSetType(subpc,PCNONE);
                    KSPSetType(subksp[i],KSPBCGS);
                    KSPSetTolerances(subksp[i],1.e-6,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
                }
            } else {
                PCSetType(subpc,PCJACOBI);
                KSPSetType(subksp[i],KSPGMRES);
                KSPSetTolerances(subksp[i],1.e-6,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
            }
        }
    }

    /* -------------------------------------------------------------------
                           Solve the linear system
       ------------------------------------------------------------------- */

    /* Set runtime options */
    KSPSetFromOptions(ksp);

    /* Solve the linear system */
    KSPSolve(ksp,b,x);

    /* -------------------------------------------------------------------
                           Check solution and clean up
       ------------------------------------------------------------------- */

    /*  Check the error */
    VecAXPY(x,none,u);
    VecNorm(x,NORM_2,&norm);
    KSPGetIterationNumber(ksp,&its);
    PetscPrintf(PETSC_COMM_WORLD,"Norm of error %g iterations %D\n",(double)norm,its);

    /*   Free work space.  All PETSc objects should be destroyed when they
         are no longer needed. */
    KSPDestroy(&ksp);
    VecDestroy(&u);  VecDestroy(&x);
    VecDestroy(&b);  MatDestroy(&A);
    PetscFinalize();

    return 0;
}

// End of the file

