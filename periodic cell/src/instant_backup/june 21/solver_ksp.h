// POBEDA -  Parallel   Optimization   with
// Boundary Elements and Domain  Adaptivity

//-----------------------------------------------
// Surface  solver - given  the  distributed mesh
// and boundary conditions, solves elasticity BVP
// on the  surface using PVFMM and parallel GMRES
// This class serves as a wrapper for PETSC GMRES
// with customized matrix-vector product operator
// developed my Mikhail Litsarev and Igor Ostanin
//===============================================


#ifndef _SOLVER_GMRES_FOR_FMM_H_
#define _SOLVER_GMRES_FOR_FMM_H_

#include <vector>
#include "mpi.h"
#include <pvfmm.hpp>
#include <petscksp.h>
#include <petsctao.h>

using dvec = std::vector<double>;
using ivec = std::vector<int>;

// Matrix shell context - data required for matvec computation
typedef struct {
    const dvec * vert_1_;
    const dvec * vert_2_;
    const dvec * vert_3_;
    const ivec * bc_type_;
    dvec * tr_di_;
    int mult_order;
    MPI_Comm comm;
    int N_vox;
} MatrInfo;


// TO DO - remaster this using MPI scater and MPI gather

// Distribute local indices for PETSC array initialization
void setup_local_indxs(PetscInt * loc_ind_out, const int Nvox, MPI_Comm comm_mpi);

// Local size of an array
int  local_span(const int N_trg, MPI_Comm comm_mpi);

// GMRES solver class
class SolverFmmGmres {
public:
    SolverFmmGmres();
    ~SolverFmmGmres();

    bool  init(const int N, const dvec & vert_1, const dvec & vert_2,
               const dvec & vert_3, const ivec & bc_type, dvec & tr_di);

    int   solve(const int mult_order, MPI_Comm comm, const int max_Iter = 100, const double tol = 1e-6);

private:

    MatrInfo  matr_ctx_;

    const int SRC_PER_TRG_;
    const int DIM_;
    const int SURF_DIM_;

    int   N_vox_;
    const dvec * vert_1_;
    const dvec * vert_2_;
    const dvec * vert_3_;
    const ivec * bc_type_;
    dvec * tr_di_;

    /*! Maximum number of points in the box (FMM parameter). */
    const size_t max_pts_;

};

#endif  //  _SOLVER_GMRES_FOR_FMM_H_

// End of the file

