#include "solver_ksp.h"

#include <iostream>
#include <petscksp.h>
#include <petsctao.h>
#include "matvec.h"
#include "mpi.h"


using ivec = std::vector<int>;
using dvec = std::vector<double>;

using  std::cout;
using  std::endl;


// Custom matvec function
PetscErrorCode apply_Op(Mat opA, Vec vx_in, Vec Ax_out) {

    // Set up matvec context and input
    void * ctx = 0;
    MatShellGetContext(opA, &ctx);
    MatrInfo * matr_ctx = (MatrInfo *) ctx;
    PetscScalar * Av_arr;
    VecGetArray(Ax_out, &Av_arr);

    const PetscScalar * v_arr_xread;
    VecGetArrayRead(vx_in, &v_arr_xread);

    PetscInt x_sz;
    VecGetLocalSize(vx_in, &x_sz);

    // Matvec operation (see matvec.h)
    matvec(v_arr_xread, Av_arr, x_sz,
           *matr_ctx->vert_1_, *matr_ctx->vert_2_, *matr_ctx->vert_3_,
           *matr_ctx->bc_type_, matr_ctx->mult_order, matr_ctx->comm);

    // Backward setup of the original matvec into the Av_out
    const int N_trg = matr_ctx->N_vox;
    const int span = local_span(N_trg, matr_ctx->comm);
    int loc_ind[3 * span];
    setup_local_indxs(loc_ind,matr_ctx->N_vox, matr_ctx->comm);
    VecSetValues(Ax_out, x_sz, loc_ind, Av_arr, INSERT_VALUES);
    VecAssemblyBegin(Ax_out);
    VecAssemblyEnd(Ax_out);

    return 0;
}

///////////////////////////////////////////////////////////////////////////////

void setup_local_indxs(PetscInt * loc_ind_out, const int Nvox, MPI_Comm comm_mpi) {


    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm_mpi, &n_proc);
    MPI_Comm_rank(comm_mpi, &i_proc);

    int N_trg = Nvox;
    int step = (int) std::floor(N_trg / (double) (n_proc - 1.));
    const int start = step *  i_proc;
    int end         = step * (i_proc + 1);
    if (i_proc == n_proc-1) end = N_trg;
    const int span = end - start;

    for (int i = 0; i < span; i++) {
        for (int k = 0; k < 3; k++) {
            loc_ind_out[3 * i + k] = 3 * (start + i) + k;
        }
    }
}

int  local_span(const int N_trg, MPI_Comm comm_mpi) {

    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm_mpi, &n_proc);
    MPI_Comm_rank(comm_mpi, &i_proc);
    int step;
    if (n_proc==1) step = N_trg;
    if (n_proc>1) step = (int) std::floor(N_trg / (double) (n_proc - 1.));
    const int start = step *  i_proc;
    int end         = step * (i_proc + 1);
    if (i_proc == n_proc-1) end = N_trg;
    return (end - start);
}

///////////////////////////////////////////////////////////////////////////////

SolverFmmGmres::SolverFmmGmres():
matr_ctx_(),
SRC_PER_TRG_(16),
DIM_(3),
SURF_DIM_(6),
N_vox_(-1),
vert_1_(nullptr),
vert_2_(nullptr),
vert_3_(nullptr),
bc_type_(nullptr),
tr_di_(nullptr),
max_pts_(100)
{
}

SolverFmmGmres::~SolverFmmGmres() {

    vert_1_ = nullptr;
    vert_2_ = nullptr;
    vert_3_ = nullptr;
    bc_type_ = nullptr;
    tr_di_ = nullptr;
}


bool SolverFmmGmres::init(const int Nvox_ext, const dvec & vert_1_ext, const dvec & vert_2_ext,
                          const dvec & vert_3_ext, const ivec & bc_type_ext,
                          dvec & tr_di_ext) {
    N_vox_  = Nvox_ext;
    vert_1_ = &vert_1_ext;
    vert_2_ = &vert_2_ext;
    vert_3_ = &vert_3_ext;
    bc_type_ = &bc_type_ext;
    tr_di_ = &tr_di_ext;

    return true;
}

int SolverFmmGmres::solve(const int m_MultOrder, MPI_Comm comm_mpi, const int max_Iter, const double tol) {

    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm_mpi, &n_proc);
    MPI_Comm_rank(comm_mpi, &i_proc);

    if (!i_proc) cout << "Surface solution Solver starts... " << endl;

    // 1. Build rhs. (vector b)
    dvec b_rhs(tr_di_->size());
    rhs( *tr_di_, b_rhs, *vert_1_, *vert_2_, *vert_3_, *bc_type_, m_MultOrder, comm_mpi );

    // Test output
//    std::cout<<" RHS: "<<std::endl;
//    for (int i = 0; i<b_rhs.size(); i++)
//    {std::cout<<b_rhs[i]<<std::endl;}

    // 1.1 Initial pass of the matvec (create tree etc)
    init_matvec(*vert_1_, *vert_2_, *vert_3_, *bc_type_, m_MultOrder, comm_mpi);

    // 1.2 Initialize vectors
    Vec            x, b;      /* approx solution, RHS, exact solution */
    PetscInitialize(0, 0, (char*)0, 0);
    PetscErrorCode ierr;
    VecCreate(comm_mpi, &x);
    ierr = VecSetSizes(x, b_rhs.size(), PETSC_DECIDE);   CHKERRQ(ierr);
    VecSetFromOptions(x);

    //--- Init petsc vector ---
    const int N_trg = N_vox_;
    const int span = local_span(N_trg, comm_mpi);
    int loc_ind[3 * span];
    setup_local_indxs(loc_ind, N_vox_, comm_mpi);
    //VecSet(x, 1.0);
    VecSetValues(x, b_rhs.size(), loc_ind, b_rhs.data(), INSERT_VALUES);
    VecAssemblyBegin(x);
    VecAssemblyEnd(x);
    ierr = VecDuplicate(x, &b);  CHKERRQ(ierr);
    VecSetValues(b, b_rhs.size(), loc_ind, b_rhs.data(), INSERT_VALUES);
    VecAssemblyBegin(b);
    VecAssemblyEnd(b);

//    cout << "Vector b: " << endl;
//    VecView(b, PETSC_VIEWER_STDOUT_WORLD);

    // 2. Setup matrix (shell).
    Mat   A;            /* linear system matrix */

    // Pass the context to the matvec implementation
    matr_ctx_.vert_1_ = vert_1_;
    matr_ctx_.vert_2_ = vert_2_;
    matr_ctx_.vert_3_ = vert_3_;
    matr_ctx_.bc_type_ = bc_type_;
    matr_ctx_.tr_di_ = tr_di_;
    matr_ctx_.comm = comm_mpi;
    matr_ctx_.mult_order = m_MultOrder;
    matr_ctx_.N_vox = N_vox_;

    MatCreateShell(comm_mpi, b_rhs.size(), b_rhs.size(), PETSC_DETERMINE, PETSC_DETERMINE, &matr_ctx_, &A);
    MatShellSetOperation(A, MATOP_MULT, (void(*)(void)) apply_Op);

    if (!i_proc) cout << "Start gmres..." << endl;

    KSP    ksp_solver;    /* linear solver context */

    KSPCreate(comm_mpi, &ksp_solver);
    KSPSetOperators(ksp_solver, A, A);
    KSPSetType(ksp_solver, KSPGMRES);
    KSPSetTolerances(ksp_solver,tol,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
    //KSPGMRESSetHapTol(ksp_solver, 0.01);
    KSPGMRESSetRestart(ksp_solver, 10);

    KSPSetFromOptions(ksp_solver);
    //KSPMonitorTrueResidualMaxNorm(ksp_solver);
    KSPSetUp(ksp_solver);
    KSPSolve(ksp_solver, b, x);

    PetscInt its;
    KSPConvergedReason reason;
    KSPGetConvergedReason(ksp_solver,&reason);
    if (reason==KSP_DIVERGED_INDEFINITE_PC) {
        PetscPrintf(comm_mpi,"\nDivergence because of indefinite preconditioner;\n");
        PetscPrintf(comm_mpi,"Run the executable again but with '-pc_factor_shift_type POSITIVE_DEFINITE' option.\n");
   } else if (reason < 0) {
        PetscPrintf(comm_mpi,"\nOther kind of divergence: this should not happen.\n");
   } else {
        KSPGetIterationNumber(ksp_solver,&its);
   }
    if (!i_proc) cout << "x after GMRES: " << endl;
//    VecView(x, PETSC_VIEWER_STDOUT_WORLD);
//    cout << "b after GMRES: " << endl;
//    VecView(b, PETSC_VIEWER_STDOUT_WORLD);


    // Save solution to tr_di vector
    PetscScalar *avec;
    VecGetArray(x, &avec);
    for (int i = 0; i < tr_di_->size(); i++)
        tr_di_->data()[i] = avec[i];


    KSPDestroy(&ksp_solver);
    VecDestroy(&x);
    VecDestroy(&b);
    MatDestroy(&A);
    PetscFinalize();

    return 0;
}


///////////////////////////////////////////////////////////////////////////////

// End of the file



