#include "mesher.h"
#include "auxillary.h"


typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

extern double YOUNG;     // Young's modulus of the material
extern double POISS;     // Poisson's ratio of the material


dvec mv33(dvec & matrix, dvec & vector)
{
    dvec res(3);

    for (int i = 0; i<3; i++) {
        res[i] = 0;
        for (int j = 0; j<3; j++) {
            res[i] = res[i] + matrix[j+i*3] * vector[j];
        }
    }
    //std::cout<<res[0]<<" , "<<res[1]<<" , "<<res[2]<<std::endl;
    return res;
}


void generate_BVP(

        // Input

        int M,
        int N,
        int K,
        double c,
        ivec & side_BC,
        dvec & force_on_voxel,
        ivec & status,
        ivec & is_surf,
        bool isPeriodic,
        bool isSymmetric,

        // Output

        dvec & vert_1,    // first vertice of a triangular element
        dvec & vert_2,    // second vertice of a triangular element
        dvec & vert_3,    // third vertice of a triangular element
        //dvec & nor,      // element's outward normal
        dvec & tr_di,      // vector of tractions (known if type 1(Neumann) BC are specified)
        //dvec & colloc,    // array of collocation points

        ivec & bc_t,      // BC type: 1 - Neumann 2 - Dirichlet
        //dvec & are,      // element's area
        dvec & sig
        )
{
    // Sequential mesher

    // Check that input parameters are OK
    if ((isPeriodic)&&( (N!=M)||(N!=K)) ) std::cout<<"Error! Periodic cell mode ON, but M,N,K are not equal"<<std::endl;
    if ((isSymmetric)&&(N/2!=N/2.0)) std::cout<<"Error! Symmetric mode is ON, but N is not even"<<std::endl;



    std::cout<<"Generate new boundary..."<<std::endl;

    int N_ele;  // total number of elements

    // Counting the number of elements

    dvec trac(0), disp(0);

    N_ele = 0;
    for (int m = 0; m < M; m++){
        for (int n = 0; n < N; n++){
            for (int k = 0; k < K; k++){
                if (status[iii(m,n,k,M,N,K)] == 1){

                    if ( (m ==   0) && (status[iii(M-1,n,k,M,N,K)]==0) )   N_ele += 4;
                    if ( (m >    0) && (status[iii(m-1,n,k,M,N,K)]==0) )   N_ele += 4;
                    if ( (m == M-1) && (status[iii(0  ,n,k,M,N,K)]==0) )   N_ele += 4;
                    if ( (m <  M-1) && (status[iii(m+1,n,k,M,N,K)]==0) )   N_ele += 4;

                    if ( (n ==   0) && (status[iii(m, N-1,k,M,N,K)]==0) )   N_ele += 4;
                    if ( (n >    0) && (status[iii(m, n-1,k,M,N,K)]==0) )   N_ele += 4;
                    if ( (n == N-1) && (status[iii(m, 0  ,k,M,N,K)]==0) )   N_ele += 4;
                    if ( (n <  N-1) && (status[iii(m, n+1,k,M,N,K)]==0) )   N_ele += 4;

                    if ( (k ==   0) && (status[iii(m, n, K-1,M,N,K)]==0) )   N_ele += 4;
                    if ( (k >    0) && (status[iii(m, n, k-1,M,N,K)]==0) )   N_ele += 4;
                    if ( (k == K-1) && (status[iii(m, n, 0  ,M,N,K)]==0) )   N_ele += 4;
                    if ( (k <  K-1) && (status[iii(m, n, k+1,M,N,K)]==0) )   N_ele += 4;



                    //std::cout<<N_ele<<std::endl;
                }
            }
        }
    }

    std::cout<<"I'm going to generate"<<std::endl;
    std::cout<<N_ele<<" elements. Wait a bit ..."<<std::endl;

    vert_1.resize(3*N_ele);
    vert_2.resize(3*N_ele);
    vert_3.resize(3*N_ele);
    trac.resize(3*N_ele);
    disp.resize(3*N_ele);
    tr_di.resize(3*N_ele);
    bc_t.resize(N_ele);

    for (int i = 0; i<3*N_ele; i++)
    {
       vert_1[i] = 0;
       vert_2[i] = 0;
       vert_3[i] = 0;
       trac[i] = 0;
       disp[i] = 0;
    }

    for (int i = 0; i<N_ele; i++)
    {
        bc_t[i] = 1;
       // are[i] = 0.;
    }

    std::cout<<"initialized elements"<<std::endl;
    double X,Y,Z;

    // Vertices, normals, BCs, tractions
    int i = 0; // Element index
    for (int m = 0; m < M; m++){
        for (int n = 0; n < N; n++){
            for (int k = 0; k < K; k++){
                if (status[iii(m,n,k,M,N,K)] == 1){

                    X = (m + 0.5)*c;
                    Y = (n + 0.5)*c;
                    Z = (k + 0.5)*c;

                    // 1 Y+
                    if (( (n  < N-1) && ( status[iii(m,n+1,k,M,N,K)]==0 ) )||
                        ( (n == N-1) && ( status[iii(m,0  ,k,M,N,K)]==0 ) )  ) {


                        // Mark this vox as a surf vox
                        is_surf[iii(m,n,k,M,N,K)] = 1;



                        // Element 1
                        vert_1[3 * i+0] = X + c / 2.0;
                        vert_1[3 * i+1] = Y + c / 2.0;
                        vert_1[3 * i+2] = Z - c / 2.0;

                        vert_2[3 * i+0] = X;
                        vert_2[3 * i+1] = Y + c / 2.0;
                        vert_2[3 * i+2] = Z;

                        vert_3[3 * i+0] = X + c / 2.0;
                        vert_3[3 * i+1] = Y + c / 2.0;
                        vert_3[3 * i+2] = Z + c / 2.0;


                        trac[3 * i+0] = force_on_voxel[iiiii(m,n,k,0,0,M,N,K,6,3)];
                        trac[3 * i+1] = force_on_voxel[iiiii(m,n,k,0,1,M,N,K,6,3)];
                        trac[3 * i+2] = force_on_voxel[iiiii(m,n,k,0,2,M,N,K,6,3)];

                        if ((n==N-1) && (side_BC[0] == 2)) bc_t[i] = 2;

                        // Element 2
                        vert_1[3 * (i+1)+0] = X + c / 2.0;
                        vert_1[3 * (i+1)+1] = Y + c / 2.0;
                        vert_1[3 * (i+1)+2] = Z + c / 2.0;

                        vert_2[3 * (i+1)+0] = X;
                        vert_2[3 * (i+1)+1] = Y + c / 2.0;
                        vert_2[3 * (i+1)+2] = Z;

                        vert_3[3 * (i+1)+0] = X - c / 2.0;
                        vert_3[3 * (i+1)+1] = Y + c / 2.0;
                        vert_3[3 * (i+1)+2] = Z + c / 2.0;

                        trac[3 * (i+1)+0] = force_on_voxel[iiiii(m,n,k,0,0,M,N,K,6,3)];
                        trac[3 * (i+1)+1] = force_on_voxel[iiiii(m,n,k,0,1,M,N,K,6,3)];
                        trac[3 * (i+1)+2] = force_on_voxel[iiiii(m,n,k,0,2,M,N,K,6,3)];

                        if ((n==N-1) && (side_BC[0] == 2)) bc_t[i+1] = 2;


                        // Element 3
                        vert_1[3 * (i+2)+0] = X - c / 2.0;
                        vert_1[3 * (i+2)+1] = Y + c / 2.0;
                        vert_1[3 * (i+2)+2] = Z + c / 2.0;

                        vert_2[3 * (i+2)+0] = X;
                        vert_2[3 * (i+2)+1] = Y + c / 2.0;
                        vert_2[3 * (i+2)+2] = Z;

                        vert_3[3 * (i+2)+0] = X - c / 2.0;
                        vert_3[3 * (i+2)+1] = Y + c / 2.0;
                        vert_3[3 * (i+2)+2] = Z - c / 2.0;



                        trac[3 * (i+2)+0] = force_on_voxel[iiiii(m,n,k,0,0,M,N,K,6,3)];
                        trac[3 * (i+2)+1] = force_on_voxel[iiiii(m,n,k,0,1,M,N,K,6,3)];
                        trac[3 * (i+2)+2] = force_on_voxel[iiiii(m,n,k,0,2,M,N,K,6,3)];

                        if ((n==N-1) && (side_BC[0] == 2)) bc_t[i+2] = 2;


                        // Element 4
                        vert_1[3 * (i+3)+0] = X - c / 2.0;
                        vert_1[3 * (i+3)+1] = Y + c / 2.0;
                        vert_1[3 * (i+3)+2] = Z - c / 2.0;

                        vert_2[3 * (i+3)+0] = X;
                        vert_2[3 * (i+3)+1] = Y + c / 2.0;
                        vert_2[3 * (i+3)+2] = Z;

                        vert_3[3 * (i+3)+0] = X + c / 2.0;
                        vert_3[3 * (i+3)+1] = Y + c / 2.0;
                        vert_3[3 * (i+3)+2] = Z - c / 2.0;



                        trac[3 * (i+3)+0] = force_on_voxel[iiiii(m,n,k,0,0,M,N,K,6,3)];
                        trac[3 * (i+3)+1] = force_on_voxel[iiiii(m,n,k,0,1,M,N,K,6,3)];
                        trac[3 * (i+3)+2] = force_on_voxel[iiiii(m,n,k,0,2,M,N,K,6,3)];

                        if ((n==N-1) && (side_BC[0] == 2)) bc_t[i+3] = 2;


                        i += 4;

                    } // Y+


                    // 2 X+
                    if (( (m  < M-1) && ( status[iii(m+1,n,k,M,N,K)]==0 ) )||
                        ( (m == M-1) && ( status[iii(0  ,n,k,M,N,K)]==0 ) )  ) {


                        // Mark this vox as a surf vox
                        is_surf[iii(m,n,k,M,N,K)] = 1;


                        // Element 5

                        vert_1[3 * i+0] = X + c / 2.0;
                        vert_1[3 * i+1] = Y + c / 2.0;
                        vert_1[3 * i+2] = Z - c / 2.0;

                        vert_2[3 * i+0] = X + c / 2.0;
                        vert_2[3 * i+1] = Y + c / 2.0;
                        vert_2[3 * i+2] = Z + c / 2.0;

                        vert_3[3 * i+0] = X + c / 2.0;
                        vert_3[3 * i+1] = Y;
                        vert_3[3 * i+2] = Z;


                        trac[3 * i+0] = force_on_voxel[iiiii(m,n,k,1,0,M,N,K,6,3)];
                        trac[3 * i+1] = force_on_voxel[iiiii(m,n,k,1,1,M,N,K,6,3)];
                        trac[3 * i+2] = force_on_voxel[iiiii(m,n,k,1,2,M,N,K,6,3)];

                        if ((m==M-1) && (side_BC[0] == 2)) bc_t[i] = 2;

                        // Element 6
                        vert_1[3 * (i+1)+0] = X + c / 2.0;
                        vert_1[3 * (i+1)+1] = Y - c / 2.0;
                        vert_1[3 * (i+1)+2] = Z - c / 2.0;

                        vert_2[3 * (i+1)+0] = X + c / 2.0;
                        vert_2[3 * (i+1)+1] = Y + c / 2.0;
                        vert_2[3 * (i+1)+2] = Z - c / 2.0;

                        vert_3[3 * (i+1)+0] = X + c / 2.0;
                        vert_3[3 * (i+1)+1] = Y;
                        vert_3[3 * (i+1)+2] = Z;



                        trac[3 * (i+1)+0] = force_on_voxel[iiiii(m,n,k,1,0,M,N,K,6,3)];
                        trac[3 * (i+1)+1] = force_on_voxel[iiiii(m,n,k,1,1,M,N,K,6,3)];
                        trac[3 * (i+1)+2] = force_on_voxel[iiiii(m,n,k,1,2,M,N,K,6,3)];

                        if ((m==M-1) && (side_BC[0] == 2)) bc_t[i+1] = 2;

                        // Element 7
                        vert_1[3 * (i+2)+0] = X + c / 2.0;
                        vert_1[3 * (i+2)+1] = Y - c / 2.0;
                        vert_1[3 * (i+2)+2] = Z + c / 2.0;

                        vert_2[3 * (i+2)+0] = X + c / 2.0;
                        vert_2[3 * (i+2)+1] = Y - c / 2.0;
                        vert_2[3 * (i+2)+2] = Z - c / 2.0;

                        vert_3[3 * (i+2)+0] = X + c / 2.0;
                        vert_3[3 * (i+2)+1] = Y;
                        vert_3[3 * (i+2)+2] = Z;



                        trac[3 * (i+2)+0] = force_on_voxel[iiiii(m,n,k,1,0,M,N,K,6,3)];
                        trac[3 * (i+2)+1] = force_on_voxel[iiiii(m,n,k,1,1,M,N,K,6,3)];
                        trac[3 * (i+2)+2] = force_on_voxel[iiiii(m,n,k,1,2,M,N,K,6,3)];

                        if ((m==M-1) && (side_BC[0] == 2)) bc_t[i+2] = 2;



                        // Element 8
                        vert_1[3 * (i+3)+0] = X + c / 2.0;
                        vert_1[3 * (i+3)+1] = Y + c / 2.0;
                        vert_1[3 * (i+3)+2] = Z + c / 2.0;

                        vert_2[3 * (i+3)+0] = X + c / 2.0;
                        vert_2[3 * (i+3)+1] = Y - c / 2.0;
                        vert_2[3 * (i+3)+2] = Z + c / 2.0;

                        vert_3[3 * (i+3)+0] = X + c / 2.0;
                        vert_3[3 * (i+3)+1] = Y;
                        vert_3[3 * (i+3)+2] = Z;



                        trac[3 * (i+3)+0] = force_on_voxel[iiiii(m,n,k,1,0,M,N,K,6,3)];
                        trac[3 * (i+3)+1] = force_on_voxel[iiiii(m,n,k,1,1,M,N,K,6,3)];
                        trac[3 * (i+3)+2] = force_on_voxel[iiiii(m,n,k,1,2,M,N,K,6,3)];

                        if ((m==M-1) && (side_BC[0] == 2)) bc_t[i+3] = 2;


                        i += 4;
                    } // X+

                    // 3 Y-
                    if (( (n  > 0) && ( status[iii(m,n-1,k,M,N,K)]==0 ) )||
                        ( (n == 0) && ( status[iii(m,N-1  ,k,M,N,K)]==0 ) )  ) {


                        // Mark this vox as a surf vox
                        is_surf[iii(m,n,k,M,N,K)] = 1;


                        // Element 9
                        vert_1[3 * i+0] = X + c / 2.0;
                        vert_1[3 * i+1] = Y - c / 2.0;
                        vert_1[3 * i+2] = Z - c / 2.0;

                        vert_2[3 * i+0] = X + c / 2.0;
                        vert_2[3 * i+1] = Y - c / 2.0;
                        vert_2[3 * i+2] = Z + c / 2.0;

                        vert_3[3 * i+0] = X;
                        vert_3[3 * i+1] = Y - c / 2.0;
                        vert_3[3 * i+2] = Z;



                        trac[3 * i+0] = force_on_voxel[iiiii(m,n,k,2,0,M,N,K,6,3)];
                        trac[3 * i+1] = force_on_voxel[iiiii(m,n,k,2,1,M,N,K,6,3)];
                        trac[3 * i+2] = force_on_voxel[iiiii(m,n,k,2,2,M,N,K,6,3)];

                        if ((n==0) && (side_BC[0] == 2)) bc_t[i] = 2;

                        // Element 10
                        vert_1[3 * (i+1)+0] = X + c / 2.0;
                        vert_1[3 * (i+1)+1] = Y - c / 2.0;
                        vert_1[3 * (i+1)+2] = Z + c / 2.0;

                        vert_2[3 * (i+1)+0] = X - c / 2.0;
                        vert_2[3 * (i+1)+1] = Y - c / 2.0;
                        vert_2[3 * (i+1)+2] = Z + c / 2.0;

                        vert_3[3 * (i+1)+0] = X;
                        vert_3[3 * (i+1)+1] = Y - c / 2.0;
                        vert_3[3 * (i+1)+2] = Z;



                        trac[3 * (i+1)+0] = force_on_voxel[iiiii(m,n,k,2,0,M,N,K,6,3)];
                        trac[3 * (i+1)+1] = force_on_voxel[iiiii(m,n,k,2,1,M,N,K,6,3)];
                        trac[3 * (i+1)+2] = force_on_voxel[iiiii(m,n,k,2,2,M,N,K,6,3)];

                        if ((n==0) && (side_BC[0] == 2)) bc_t[i+1] = 2;

                        // Element 11
                        vert_1[3 * (i+2)+0] = X - c / 2.0;
                        vert_1[3 * (i+2)+1] = Y - c / 2.0;
                        vert_1[3 * (i+2)+2] = Z + c / 2.0;

                        vert_2[3 * (i+2)+0] = X - c / 2.0;
                        vert_2[3 * (i+2)+1] = Y - c / 2.0;
                        vert_2[3 * (i+2)+2] = Z - c / 2.0;

                        vert_3[3 * (i+2)+0] = X;
                        vert_3[3 * (i+2)+1] = Y - c / 2.0;
                        vert_3[3 * (i+2)+2] = Z;



                        trac[3 * (i+2)+0] = force_on_voxel[iiiii(m,n,k,2,0,M,N,K,6,3)];
                        trac[3 * (i+2)+1] = force_on_voxel[iiiii(m,n,k,2,1,M,N,K,6,3)];
                        trac[3 * (i+2)+2] = force_on_voxel[iiiii(m,n,k,2,2,M,N,K,6,3)];

                        if ((n==0) && (side_BC[0] == 2)) bc_t[i+2] = 2;


                        // Element 12
                        vert_1[3 * (i+3)+0] = X - c / 2.0;
                        vert_1[3 * (i+3)+1] = Y - c / 2.0;
                        vert_1[3 * (i+3)+2] = Z - c / 2.0;

                        vert_2[3 * (i+3)+0] = X + c / 2.0;
                        vert_2[3 * (i+3)+1] = Y - c / 2.0;
                        vert_2[3 * (i+3)+2] = Z - c / 2.0;

                        vert_3[3 * (i+3)+0] = X;
                        vert_3[3 * (i+3)+1] = Y - c / 2.0;
                        vert_3[3 * (i+3)+2] = Z;



                        trac[3 * (i+3)+0] = force_on_voxel[iiiii(m,n,k,2,0,M,N,K,6,3)];
                        trac[3 * (i+3)+1] = force_on_voxel[iiiii(m,n,k,2,1,M,N,K,6,3)];
                        trac[3 * (i+3)+2] = force_on_voxel[iiiii(m,n,k,2,2,M,N,K,6,3)];

                        if ((n==0) && (side_BC[0] == 2)) bc_t[i+3] = 2;

                        i += 4;
                    } // Y-

                    // 4 X-
                    if (( (m  > 0) && ( status[iii(m-1,n,k,M,N,K)]==0 ) )||
                        ( (m == 0) && ( status[iii(M-1,n,k,M,N,K)]==0 ) )  ) {

                        // Mark this vox as a surf vox
                        is_surf[iii(m,n,k,M,N,K)] = 1;


                        // Element 13
                        vert_1[3 * i+0] = X - c / 2.0;
                        vert_1[3 * i+1] = Y - c / 2.0;
                        vert_1[3 * i+2] = Z + c / 2.0;

                        vert_2[3 * i+0] = X - c / 2.0;
                        vert_2[3 * i+1] = Y + c / 2.0;
                        vert_2[3 * i+2] = Z + c / 2.0;

                        vert_3[3 * i+0] = X - c / 2.0;
                        vert_3[3 * i+1] = Y;
                        vert_3[3 * i+2] = Z;



                        trac[3 * i+0] = force_on_voxel[iiiii(m,n,k,3,0,M,N,K,6,3)];
                        trac[3 * i+1] = force_on_voxel[iiiii(m,n,k,3,1,M,N,K,6,3)];
                        trac[3 * i+2] = force_on_voxel[iiiii(m,n,k,3,2,M,N,K,6,3)];

                        if ((m==0) && (side_BC[3] == 2)) bc_t[i] = 2;



                        // Element 14
                        vert_1[3 * (i+1)+0] = X - c / 2.0;
                        vert_1[3 * (i+1)+1] = Y + c / 2.0;
                        vert_1[3 * (i+1)+2] = Z + c / 2.0;

                        vert_2[3 * (i+1)+0] = X - c / 2.0;
                        vert_2[3 * (i+1)+1] = Y + c / 2.0;
                        vert_2[3 * (i+1)+2] = Z - c / 2.0;

                        vert_3[3 * (i+1)+0] = X - c / 2.0;
                        vert_3[3 * (i+1)+1] = Y;
                        vert_3[3 * (i+1)+2] = Z;



                        trac[3 * (i+1)+0] = force_on_voxel[iiiii(m,n,k,3,0,M,N,K,6,3)];
                        trac[3 * (i+1)+1] = force_on_voxel[iiiii(m,n,k,3,1,M,N,K,6,3)];
                        trac[3 * (i+1)+2] = force_on_voxel[iiiii(m,n,k,3,2,M,N,K,6,3)];

                        if ((m==0) && (side_BC[3] == 2)) bc_t[i+1] = 2;


                        // Element 15
                        vert_1[3 * (i+2)+0] = X - c / 2.0;
                        vert_1[3 * (i+2)+1] = Y + c / 2.0;
                        vert_1[3 * (i+2)+2] = Z - c / 2.0;

                        vert_2[3 * (i+2)+0] = X - c / 2.0;
                        vert_2[3 * (i+2)+1] = Y - c / 2.0;
                        vert_2[3 * (i+2)+2] = Z - c / 2.0;

                        vert_3[3 * (i+2)+0] = X - c / 2.0;
                        vert_3[3 * (i+2)+1] = Y;
                        vert_3[3 * (i+2)+2] = Z;



                        trac[3 * (i+2)+0] = force_on_voxel[iiiii(m,n,k,3,0,M,N,K,6,3)];
                        trac[3 * (i+2)+1] = force_on_voxel[iiiii(m,n,k,3,1,M,N,K,6,3)];
                        trac[3 * (i+2)+2] = force_on_voxel[iiiii(m,n,k,3,2,M,N,K,6,3)];

                        if ((m==0) && (side_BC[3] == 2)) bc_t[i+2] = 2;



                        // Element 16
                        vert_1[3 * (i+3)+0] = X - c / 2.0;
                        vert_1[3 * (i+3)+1] = Y - c / 2.0;
                        vert_1[3 * (i+3)+2] = Z - c / 2.0;

                        vert_2[3 * (i+3)+0] = X - c / 2.0;
                        vert_2[3 * (i+3)+1] = Y - c / 2.0;
                        vert_2[3 * (i+3)+2] = Z + c / 2.0;

                        vert_3[3 * (i+3)+0] = X - c / 2.0;
                        vert_3[3 * (i+3)+1] = Y;
                        vert_3[3 * (i+3)+2] = Z;



                        trac[3 * (i+3)+0] = force_on_voxel[iiiii(m,n,k,3,0,M,N,K,6,3)];
                        trac[3 * (i+3)+1] = force_on_voxel[iiiii(m,n,k,3,1,M,N,K,6,3)];
                        trac[3 * (i+3)+2] = force_on_voxel[iiiii(m,n,k,3,2,M,N,K,6,3)];

                        if ((m==0) && (side_BC[3] == 2)) bc_t[i+3] = 2;




                        i += 4;
                    } // X-

                    // 5 Z+
                    if (( (k  < K-1) && ( status[iii(m,n,k+1,M,N,K)]==0 ) )||
                        ( (k == K-1) && ( status[iii(m,n,0,M,N,K)]==0 ) )  ) {



                        // Mark this vox as a surf vox
                        is_surf[iii(m,n,k,M,N,K)] = 1;


                        // Element 17
                        vert_1[3 * i+0] = X + c / 2.0;
                        vert_1[3 * i+1] = Y + c / 2.0;
                        vert_1[3 * i+2] = Z + c / 2.0;

                        vert_2[3 * i+0] = X - c / 2.0;
                        vert_2[3 * i+1] = Y + c / 2.0;
                        vert_2[3 * i+2] = Z + c / 2.0;

                        vert_3[3 * i+0] = X;
                        vert_3[3 * i+1] = Y;
                        vert_3[3 * i+2] = Z + c / 2.0;



                        trac[3 * i+0] = force_on_voxel[iiiii(m,n,k,4,0,M,N,K,6,3)];
                        trac[3 * i+1] = force_on_voxel[iiiii(m,n,k,4,1,M,N,K,6,3)];
                        trac[3 * i+2] = force_on_voxel[iiiii(m,n,k,4,2,M,N,K,6,3)];

                        if ((k==K-1) && (side_BC[4] == 2)) bc_t[i] = 2;



                        // Element 18
                        vert_1[3 * (i+1)+0] = X - c / 2.0;
                        vert_1[3 * (i+1)+1] = Y + c / 2.0;
                        vert_1[3 * (i+1)+2] = Z + c / 2.0;

                        vert_2[3 * (i+1)+0] = X - c / 2.0;
                        vert_2[3 * (i+1)+1] = Y - c / 2.0;
                        vert_2[3 * (i+1)+2] = Z + c / 2.0;

                        vert_3[3 * (i+1)+0] = X;
                        vert_3[3 * (i+1)+1] = Y;
                        vert_3[3 * (i+1)+2] = Z + c / 2.0;



                        trac[3 * (i+1)+0] = force_on_voxel[iiiii(m,n,k,4,0,M,N,K,6,3)];
                        trac[3 * (i+1)+1] = force_on_voxel[iiiii(m,n,k,4,1,M,N,K,6,3)];
                        trac[3 * (i+1)+2] = force_on_voxel[iiiii(m,n,k,4,2,M,N,K,6,3)];

                        if ((k==K-1) && (side_BC[4] == 2)) bc_t[i+1] = 2;

                        // Element 19
                        vert_1[3 * (i+2)+0] = X - c / 2.0;
                        vert_1[3 * (i+2)+1] = Y - c / 2.0;
                        vert_1[3 * (i+2)+2] = Z + c / 2.0;

                        vert_2[3 * (i+2)+0] = X + c / 2.0;
                        vert_2[3 * (i+2)+1] = Y - c / 2.0;
                        vert_2[3 * (i+2)+2] = Z + c / 2.0;

                        vert_3[3 * (i+2)+0] = X;
                        vert_3[3 * (i+2)+1] = Y;
                        vert_3[3 * (i+2)+2] = Z + c / 2.0;



                        trac[3 * (i+2)+0] = force_on_voxel[iiiii(m,n,k,4,0,M,N,K,6,3)];
                        trac[3 * (i+2)+1] = force_on_voxel[iiiii(m,n,k,4,1,M,N,K,6,3)];
                        trac[3 * (i+2)+2] = force_on_voxel[iiiii(m,n,k,4,2,M,N,K,6,3)];

                        if ((k==K-1) && (side_BC[4] == 2)) bc_t[i+2] = 2;


                        // Element 20
                        vert_1[3 * (i+3)+0] = X + c / 2.0;
                        vert_1[3 * (i+3)+1] = Y - c / 2.0;
                        vert_1[3 * (i+3)+2] = Z + c / 2.0;

                        vert_2[3 * (i+3)+0] = X + c / 2.0;
                        vert_2[3 * (i+3)+1] = Y + c / 2.0;
                        vert_2[3 * (i+3)+2] = Z + c / 2.0;

                        vert_3[3 * (i+3)+0] = X;
                        vert_3[3 * (i+3)+1] = Y;
                        vert_3[3 * (i+3)+2] = Z + c / 2.0;



                        trac[3 * (i+3)+0] = force_on_voxel[iiiii(m,n,k,4,0,M,N,K,6,3)];
                        trac[3 * (i+3)+1] = force_on_voxel[iiiii(m,n,k,4,1,M,N,K,6,3)];
                        trac[3 * (i+3)+2] = force_on_voxel[iiiii(m,n,k,4,2,M,N,K,6,3)];

                        if ((k==K-1) && (side_BC[4] == 2)) bc_t[i+3] = 2;




                        i += 4;

                    } // Z+

                    // 6 Z-
                    if (( (k  > 0) && ( status[iii(m,n,k-1,M,N,K)]==0 ) )||
                        ( (k == 0) && ( status[iii(m,n,K-1,M,N,K)]==0 ) )  ) {



                        // Mark this vox as a surf vox
                        is_surf[iii(m,n,k,M,N,K)] = 1;


                        // Element 21
                        vert_1[3 * i+0] = X + c / 2.0;
                        vert_1[3 * i+1] = Y + c / 2.0;
                        vert_1[3 * i+2] = Z - c / 2.0;

                        vert_2[3 * i+0] = X + c / 2.0;
                        vert_2[3 * i+1] = Y - c / 2.0;
                        vert_2[3 * i+2] = Z - c / 2.0;

                        vert_3[3 * i+0] = X;
                        vert_3[3 * i+1] = Y;
                        vert_3[3 * i+2] = Z - c / 2.0;



                        trac[3 * i+0] = force_on_voxel[iiiii(m,n,k,5,0,M,N,K,6,3)];
                        trac[3 * i+1] = force_on_voxel[iiiii(m,n,k,5,1,M,N,K,6,3)];
                        trac[3 * i+2] = force_on_voxel[iiiii(m,n,k,5,2,M,N,K,6,3)];

                        if ((k==0) && (side_BC[5] == 2)) bc_t[i] = 2;

                        // Element 22
                        vert_1[3 * (i+1)+0] = X + c / 2.0;
                        vert_1[3 * (i+1)+1] = Y - c / 2.0;
                        vert_1[3 * (i+1)+2] = Z - c / 2.0;

                        vert_2[3 * (i+1)+0] = X - c / 2.0;
                        vert_2[3 * (i+1)+1] = Y - c / 2.0;
                        vert_2[3 * (i+1)+2] = Z - c / 2.0;

                        vert_3[3 * (i+1)+0] = X;
                        vert_3[3 * (i+1)+1] = Y;
                        vert_3[3 * (i+1)+2] = Z - c / 2.0;



                        trac[3 * (i+1)+0] = force_on_voxel[iiiii(m,n,k,5,0,M,N,K,6,3)];
                        trac[3 * (i+1)+1] = force_on_voxel[iiiii(m,n,k,5,1,M,N,K,6,3)];
                        trac[3 * (i+1)+2] = force_on_voxel[iiiii(m,n,k,5,2,M,N,K,6,3)];


                        if ((k==0) && (side_BC[5] == 2)) bc_t[i+1] = 2;

                        // Element 23
                        vert_1[3 * (i+2)+0] = X - c / 2.0;
                        vert_1[3 * (i+2)+1] = Y - c / 2.0;
                        vert_1[3 * (i+2)+2] = Z - c / 2.0;

                        vert_2[3 * (i+2)+0] = X - c / 2.0;
                        vert_2[3 * (i+2)+1] = Y + c / 2.0;
                        vert_2[3 * (i+2)+2] = Z - c / 2.0;

                        vert_3[3 * (i+2)+0] = X;
                        vert_3[3 * (i+2)+1] = Y;
                        vert_3[3 * (i+2)+2] = Z - c / 2.0;



                        trac[3 * (i+2)+0] = force_on_voxel[iiiii(m,n,k,5,0,M,N,K,6,3)];
                        trac[3 * (i+2)+1] = force_on_voxel[iiiii(m,n,k,5,1,M,N,K,6,3)];
                        trac[3 * (i+2)+2] = force_on_voxel[iiiii(m,n,k,5,2,M,N,K,6,3)];


                        if ((k==0) && (side_BC[5] == 2)) bc_t[i+2] = 2;


                        // Element 24
                        vert_1[3 * (i+3)+0] = X - c / 2.0;
                        vert_1[3 * (i+3)+1] = Y + c / 2.0;
                        vert_1[3 * (i+3)+2] = Z - c / 2.0;

                        vert_2[3 * (i+3)+0] = X + c / 2.0;
                        vert_2[3 * (i+3)+1] = Y + c / 2.0;
                        vert_2[3 * (i+3)+2] = Z - c / 2.0;

                        vert_3[3 * (i+3)+0] = X;
                        vert_3[3 * (i+3)+1] = Y;
                        vert_3[3 * (i+3)+2] = Z - c / 2.0;



                        trac[3 * (i+3)+0] = force_on_voxel[iiiii(m,n,k,5,0,M,N,K,6,3)];
                        trac[3 * (i+3)+1] = force_on_voxel[iiiii(m,n,k,5,1,M,N,K,6,3)];
                        trac[3 * (i+3)+2] = force_on_voxel[iiiii(m,n,k,5,2,M,N,K,6,3)];

                        if ((k==0) && (side_BC[5] == 2)) bc_t[i+3] = 2;


                        i += 4;
                    } // Z-

                }
            }
        }
    }

    // Mark the second layer of the neighbours
    for (int i = 0; i<is_surf.size(); i++)
    {
        if (is_surf[i]==1){
            int mm = mnk(i,M,N,K)[0];
            int nn = mnk(i,M,N,K)[1];
            int kk = mnk(i,M,N,K)[2];

            // 6-neibourhood
            if ((mm<M) && (status[iii(mm+1,nn,kk,M,N,K)]==1)&&(is_surf[iii(mm+1,nn,kk,M,N,K)]==0) ) is_surf[iii(mm+1,nn,kk,M,N,K)]=2;
            if ((mm>1) && (status[iii(mm-1,nn,kk,M,N,K)]==1)&&(is_surf[iii(mm-1,nn,kk,M,N,K)]==0) ) is_surf[iii(mm-1,nn,kk,M,N,K)]=2;
            if ((nn<N) && (status[iii(mm,nn+1,kk,M,N,K)]==1)&&(is_surf[iii(mm,nn+1,kk,M,N,K)]==0) ) is_surf[iii(mm,nn+1,kk,M,N,K)]=2;
            if ((nn>1) && (status[iii(mm,nn-1,kk,M,N,K)]==1)&&(is_surf[iii(mm,nn-1,kk,M,N,K)]==0) ) is_surf[iii(mm,nn-1,kk,M,N,K)]=2;
            if ((kk<K) && (status[iii(mm,nn,kk+1,M,N,K)]==1)&&(is_surf[iii(mm,nn,kk+1,M,N,K)]==0) ) is_surf[iii(mm,nn,kk+1,M,N,K)]=2;
            if ((kk>1) && (status[iii(mm,nn,kk-1,M,N,K)]==1)&&(is_surf[iii(mm,nn,kk-1,M,N,K)]==0) ) is_surf[iii(mm,nn,kk-1,M,N,K)]=2;

            // 12-neighbourhood
            if ((mm>1) && (nn>1) && (status[iii(mm-1,nn-1,kk,M,N,K)]==1)&&(is_surf[iii(mm-1,nn-1,kk,M,N,K)]==0) ) is_surf[iii(mm-1,nn-1,kk,M,N,K)]=2;
            if ((mm>1) && (nn<N) && (status[iii(mm-1,nn+1,kk,M,N,K)]==1)&&(is_surf[iii(mm-1,nn+1,kk,M,N,K)]==0) ) is_surf[iii(mm-1,nn+1,kk,M,N,K)]=2;
            if ((mm<M) && (nn>1) && (status[iii(mm+1,nn-1,kk,M,N,K)]==1)&&(is_surf[iii(mm+1,nn-1,kk,M,N,K)]==0) ) is_surf[iii(mm+1,nn-1,kk,M,N,K)]=2;
            if ((mm<M) && (nn<N) && (status[iii(mm+1,nn+1,kk,M,N,K)]==1)&&(is_surf[iii(mm+1,nn+1,kk,M,N,K)]==0) ) is_surf[iii(mm+1,nn+1,kk,M,N,K)]=2;
            if ((mm>1) && (kk>1) && (status[iii(mm-1,nn,kk-1,M,N,K)]==1)&&(is_surf[iii(mm-1,nn,kk-1,M,N,K)]==0) ) is_surf[iii(mm-1,nn,kk-1,M,N,K)]=2;
            if ((mm>1) && (kk<K) && (status[iii(mm-1,nn,kk+1,M,N,K)]==1)&&(is_surf[iii(mm-1,nn,kk+1,M,N,K)]==0) ) is_surf[iii(mm-1,nn,kk+1,M,N,K)]=2;
            if ((mm<M) && (kk>1) && (status[iii(mm+1,nn,kk-1,M,N,K)]==1)&&(is_surf[iii(mm+1,nn,kk-1,M,N,K)]==0) ) is_surf[iii(mm+1,nn,kk-1,M,N,K)]=2;
            if ((mm<M) && (kk<K) && (status[iii(mm+1,nn,kk+1,M,N,K)]==1)&&(is_surf[iii(mm+1,nn,kk+1,M,N,K)]==0) ) is_surf[iii(mm+1,nn,kk+1,M,N,K)]=2;
            if ((nn>1) && (kk>1) && (status[iii(mm,nn-1,kk-1,M,N,K)]==1)&&(is_surf[iii(mm,nn-1,kk-1,M,N,K)]==0) ) is_surf[iii(mm,nn-1,kk-1,M,N,K)]=2;
            if ((nn>1) && (kk<K) && (status[iii(mm,nn-1,kk+1,M,N,K)]==1)&&(is_surf[iii(mm,nn-1,kk+1,M,N,K)]==0) ) is_surf[iii(mm,nn-1,kk+1,M,N,K)]=2;
            if ((nn<N) && (kk>1) && (status[iii(mm,nn+1,kk-1,M,N,K)]==1)&&(is_surf[iii(mm,nn+1,kk-1,M,N,K)]==0) ) is_surf[iii(mm,nn+1,kk-1,M,N,K)]=2;
            if ((nn<N) && (kk<K) && (status[iii(mm,nn+1,kk+1,M,N,K)]==1)&&(is_surf[iii(mm,nn+1,kk+1,M,N,K)]==0) ) is_surf[iii(mm,nn+1,kk+1,M,N,K)]=2;
            // 8-neighbourhood
            if ((mm>1) && (nn>1) && (kk>1) && (status[iii(mm-1,nn-1,kk-1,M,N,K)]==1)&&(is_surf[iii(mm-1,nn-1,kk-1,M,N,K)]==0) ) is_surf[iii(mm-1,nn-1,kk-1,M,N,K)]=2;
            if ((mm<M) && (nn>1) && (kk>1) && (status[iii(mm+1,nn-1,kk-1,M,N,K)]==1)&&(is_surf[iii(mm+1,nn-1,kk-1,M,N,K)]==0) ) is_surf[iii(mm+1,nn-1,kk-1,M,N,K)]=2;
            if ((mm>1) && (nn<N) && (kk>1) && (status[iii(mm-1,nn+1,kk-1,M,N,K)]==1)&&(is_surf[iii(mm-1,nn+1,kk-1,M,N,K)]==0) ) is_surf[iii(mm-1,nn+1,kk-1,M,N,K)]=2;
            if ((mm>1) && (nn>1) && (kk<K) && (status[iii(mm-1,nn-1,kk+1,M,N,K)]==1)&&(is_surf[iii(mm-1,nn-1,kk+1,M,N,K)]==0) ) is_surf[iii(mm-1,nn-1,kk+1,M,N,K)]=2;
            if ((mm<M) && (nn<N) && (kk>1) && (status[iii(mm+1,nn+1,kk-1,M,N,K)]==1)&&(is_surf[iii(mm+1,nn+1,kk-1,M,N,K)]==0) ) is_surf[iii(mm+1,nn+1,kk-1,M,N,K)]=2;
            if ((mm<M) && (nn>1) && (kk<K) && (status[iii(mm+1,nn-1,kk+1,M,N,K)]==1)&&(is_surf[iii(mm+1,nn-1,kk+1,M,N,K)]==0) ) is_surf[iii(mm+1,nn-1,kk+1,M,N,K)]=2;
            if ((mm>1) && (nn<N) && (kk<K) && (status[iii(mm-1,nn+1,kk+1,M,N,K)]==1)&&(is_surf[iii(mm-1,nn+1,kk+1,M,N,K)]==0) ) is_surf[iii(mm-1,nn+1,kk+1,M,N,K)]=2;
            if ((mm<M) && (nn<N) && (kk<K) && (status[iii(mm+1,nn+1,kk+1,M,N,K)]==1)&&(is_surf[iii(mm+1,nn+1,kk+1,M,N,K)]==0) ) is_surf[iii(mm+1,nn+1,kk+1,M,N,K)]=2;


        }
    }

    std::cout<<"mesh is done"<<std::endl;

    // 2) ASSIGN ADDITIONAL TRACTIONS DUE TO HOMOGENIZED STRESS TENSOR
    dvec v1(3), v2(3), v3(3), n(3), tr(3);

    for (int i = 0; i<N_ele; i++)
    {
        for (int k = 0; k < 3; k++ )
        {  v1[k] = vert_1[3*i+k];
           v2[k] = vert_2[3*i+k];
           v3[k] = vert_3[3*i+k]; }
        n = norm(v1, v2, v3);
        tr = mv33(sig, n);

        for (int k = 0; k < 3; k++ )
        {
            trac[3 * i + k] = trac[3 * i + k] + tr[k];
        }
    }

    // Union of tractions and displacements
    for (int i = 0; i<bc_t.size(); i++)
    {
        if (bc_t[i]==1)
        { for (int k = 0; k<3; k++) tr_di[3*i+k] = trac[3*i+k];}
        if (bc_t[i]==2)
        { for (int k = 0; k<3; k++) tr_di[3*i+k] = disp[3*i+k];}

    }

    double margin = 10e-10;
    for (int i = 0; i<vert_1.size(); i++)
    {
        vert_1[i] = margin/2. + vert_1[i]*(1 - margin);
        vert_2[i] = margin/2. + vert_2[i]*(1 - margin);
        vert_3[i] = margin/2. + vert_3[i]*(1 - margin);

    }

    //std::cout<<"areas and normals are done"<<std::endl;
    std::cout<<"OK"<<std::endl;

}

