#include "topo_deriv.h"

typedef std::vector<double> vec;

void topo_deriv( vec & trg_value, vec & deriv, double G, double nu)
{
    //Calculation of the topological derivatives

    double s_dot, s_tr;
    double E; E= 2*G*(1+nu);

    for (int t = 0; t<trg_value.size()/9; t++)
    {
        s_dot = 0; for (int i = 0; i<9; i++) {s_dot += trg_value[9*t+i] * trg_value[9*t+i];}
        s_tr = trg_value[9*t+0] + trg_value[9*t+4] + trg_value[9*t+8];

        deriv[t] =  ( (3.0 * (1.0 - nu))/(4.0 * E * (7.0 - 5.0 * nu)) ) *
                ( 10.0 * (1.0 + nu) * s_dot - (1.0 + 5.0 * nu) * s_tr*s_tr ) ;
    }
}

void ener_dens( vec & trg_value, vec & dens, double c, double E, double nu)
{
    vec stress(9);
    vec strain(9);


    for (int t = 0; t<trg_value.size()/9; t++)
    {
        dens[t] = 0;

        for (int i = 0; i<9; i++) stress[i] = trg_value[9*t+i];
        strain = stress2strain( stress, E, nu);

        for (int i = 0; i<9; i++)
        {
            dens[t] += 0.5 * stress[i] * strain[i];
        }
        dens[t] *= c*c*c;
    }
}


vec stress2strain(vec & stress, double E, double nu)
{
    double G = E / (2.*(1.+nu));

    vec strain(9);

    strain[0] = (1./E) * ( stress[0] - nu * (stress[4] + stress[8]) );
    strain[4] = (1./E) * ( stress[4] - nu * (stress[0] + stress[8]) );
    strain[8] = (1./E) * ( stress[8] - nu * (stress[0] + stress[4]) );

    strain[1] = (1./(2.*G)) * stress[1];
    strain[2] = (1./(2.*G)) * stress[2];
    strain[3] = (1./(2.*G)) * stress[3];

    strain[5] = (1./(2.*G)) * stress[5];
    strain[6] = (1./(2.*G)) * stress[6];
    strain[7] = (1./(2.*G)) * stress[7];
/*
    std::cout.precision(4);
    std::cout<<"TEST OUTPUT STRESS AND STRAIN:"<<std::endl;
    std::cout<<"=============================="<<std::endl;

    std::cout<<"stress:"<<std::endl;
    std::cout<<stress[0]<<"   "<<stress[1]<<"   "<<stress[2]<<std::endl;
    std::cout<<stress[3]<<"   "<<stress[4]<<"   "<<stress[5]<<std::endl;
    std::cout<<stress[6]<<"   "<<stress[7]<<"   "<<stress[8]<<std::endl;


    std::cout<<"strain:"<<std::endl;
    std::cout<<strain[0]<<"   "<<strain[1]<<"   "<<strain[2]<<std::endl;
    std::cout<<strain[3]<<"   "<<strain[4]<<"   "<<strain[5]<<std::endl;
    std::cout<<strain[6]<<"   "<<strain[7]<<"   "<<strain[8]<<std::endl;
*/




    return strain;
}
