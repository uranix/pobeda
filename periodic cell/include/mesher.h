// Modified mesher that supports
// periodic structures and
// symmetry


#ifndef MESHER_H
#define MESHER_H

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>
#include <omp.h>

typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

void generate_BVP(

        // Input

        int M,
        int N,
        int K,
        double c,
        ivec & side_BC,
        dvec & force_on_voxel,
        ivec & status,
        ivec & is_surf,
        bool isPeriodic,
        bool isSymmetric,

        // Output

        dvec & vert_1,    // first vertice of a triangular element
        dvec & vert_2,    // second vertice of a triangular element
        dvec & vert_3,    // third vertice of a triangular element
        //dvec & norm,      // element's outward normal
        dvec & tr_di,      // vector of tractions (known if type 1(Neumann) BC are specified)
        //dvec & colloc,    // array of collocation points

        ivec & bc_t,      // BC type: 1 - Neumann 2 - Dirichlet
        //dvec & area,      // element's area
        dvec & sig        // Homogenized stress
        );


#endif // MESHER_H

